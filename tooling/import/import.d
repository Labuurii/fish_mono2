import std.stdio;
import std.path;
import std.file;


immutable string CLASSIC_PATH = "C://Program Files (x86)/World of Warcraft/_classic_";
immutable string ADDONS_PATH = "Interface/Addons";
immutable string FONTS_PATH = "Fonts";

void run(string base_path) {
	auto addons_path = base_path.buildPath(ADDONS_PATH);
	auto fonts_path = base_path.buildPath(FONTS_PATH);
	
	if(exists(ADDONS_PATH)) rmdirRecurse(ADDONS_PATH);
	if(exists(FONTS_PATH)) rmdirRecurse(FONTS_PATH);
	
	copyRecurse(addons_path, ADDONS_PATH);
	copyRecurse(fonts_path, FONTS_PATH);
}

void main(string[] args) {
	if(args.length != 2) {
		writeln("Usage: import classic/retail");
		return;
	}
	
	auto mode = args[1];
	if(mode == "classic") {
		run(CLASSIC_PATH);
	} else if(mode == "retail") {
		writeln("Retail mode currently not implemented");
		//run(RETAIL_PATH);
	} else {
		writeln("Invalid mode " ~ mode);
	}
}





void copyRecurse(string from, string to)
{
    import std.file: copy, dirEntries, isDir, isFile, mkdirRecurse, SpanMode;
    import std.path: buildNormalizedPath, buildPath;

    from = buildNormalizedPath(from);
    to = buildNormalizedPath(to);

    if (isDir(from))
    {
        mkdirRecurse(to);

        auto entries = dirEntries(from, SpanMode.breadth);
        foreach (entry; entries)
        {
            auto dst = buildPath(to, entry.name[from.length + 1 .. $]);
                // + 1 for the directory separator
            if (isFile(entry.name)) copy(entry.name, dst);
            else mkdirRecurse(dst);
        }
    }
    else copy(from, to);
}