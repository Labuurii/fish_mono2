local TRANSMIT_FNS_STATE = {
	LOADED_TRANSMIT_FNS = nil
}

function load_transmit_fns()
	if TRANSMIT_FNS_STATE.LOADED_TRANSMIT_FNS then
		return TRANSMIT_FNS_STATE.LOADED_TRANSMIT_FNS
	end

	local mutable_state = {
		fishing_skill_line_index = -1, -- -1 == Fishing level unknown, nil == no fishing level
		is_casting = false,
		current_error_id = -1,
		last_error_type = -1,
		can_retrieve_corpse = false,
		mailbox_open = false,
		ah_open = false,
		merchant_open = false,
	}
	
	local event_frame = CreateFrame("Frame")
	event_frame:RegisterEvent("UNIT_SPELLCAST_START")
	event_frame:RegisterEvent("UNIT_SPELLCAST_STOP")
	event_frame:RegisterEvent("UNIT_SPELLCAST_FAILED")
	event_frame:RegisterEvent("UI_ERROR_MESSAGE")
	event_frame:RegisterEvent("CORPSE_IN_RANGE")
	event_frame:RegisterEvent("CORPSE_OUT_OF_RANGE")
	event_frame:RegisterEvent("MAIL_SHOW")
	event_frame:RegisterEvent("MAIL_CLOSED")
	event_frame:RegisterEvent("AUCTION_HOUSE_SHOW")
	event_frame:RegisterEvent("AUCTION_HOUSE_CLOSED")
	event_frame:RegisterEvent("MERCHANT_SHOW")
	event_frame:RegisterEvent("MERCHANT_CLOSED")
	event_frame:SetScript("OnEvent", function(self, event, p1, p2)
		if event == "UNIT_SPELLCAST_START" then
			mutable_state.is_casting = true
		elseif event == "UNIT_SPELLCAST_STOP" then
			mutable_state.is_casting = false
		elseif event == "UI_ERROR_MESSAGE" then
			mutable_state.current_error_id = mutable_state.current_error_id + 1
			local id = mutable_state.current_error_id
			mutable_state.last_error_type = p1
			print(p1);
		elseif event == "CORPSE_IN_RANGE" then
			mutable_state.can_retrieve_corpse = true
		elseif event == "CORPSE_OUT_OF_RANGE" then
			mutable_state.can_retrieve_corpse = false
		elseif event == "MAIL_SHOW" then
			mutable_state.mailbox_open = true
		elseif event == "MAIL_CLOSED" then
			mutable_state.mailbox_open = false
		elseif event == "AUCTION_HOUSE_CLOSED" then
			mutable_state.ah_open = false
		elseif event == "AUCTION_HOUSE_SHOW" then
			mutable_state.ah_open = true
		elseif event == "MERCHANT_SHOW" then
			mutable_state.merchant_open = true
		elseif event == "MERCHANT_CLOSED" then
			mutable_state.merchant_open = false
		end
	end)
	
	event_frame:SetScript("OnUpdate", function()
		if mutable_state.fishing_skill_line_index and mutable_state.fishing_skill_line_index == -1 then
			for index = 1, 64 do
				local name = GetSkillLineInfo(index) -- On first run without /reload the skilllineinfo is still not available, therefore try to initialize it here
				if index == 1 and not name then 
					return -- No Skill Line Info available just yet
				end
				
				if name == "Fishing" then
					print("Fishing Skill Line Inited")
					mutable_state.fishing_skill_line_index = index
				end
			end
			
			if mutable_state.fishing_skill_line_index == -1 then
				print("Got no fishing")
				mutable_state.fishing_skill_line_index = nil -- We have not learned fishing
			end
		end
	end)
	
	function health(unit)
		local hp = UnitHealth(unit)
		if hp <= 0 then
			return 0
		end
		return string.format("%.2f", hp / UnitHealthMax(unit))
	end
	
	function power(unit)
		local power = UnitPower(unit)
		if power <= 0 then
			return 0
		end
		return string.format("%.2f", power / UnitPowerMax(unit))
	end
	
	function bool_to_num(b)
		if b then
			return 1
		else
			return 0
		end
	end

	TRANSMIT_FNS_STATE.LOADED_TRANSMIT_FNS = {
		
		region = function()
			return string.upper(GetCVar("portal"))
		end,
		
		realm = function()
			return string.upper(GetRealmName())
		end,
		
		player_name = function()
			return string.upper(UnitName("player"))
		end,
		
		player_level = function()
			return UnitLevel("player")
		end,
		
		player_race = function()
			local _, r = UnitRace("player")
			return string.upper(r)
		end,
		
		player_class = function()
			local _, r = UnitClass("player")
			return string.upper(r)
		end,
		
		player_hp = function()
			if UnitIsDeadOrGhost("player") then
				return 0
			end
		
			return health("player")
		end,
		
		player_mana = function()
			return power("player")
		end,
		
		target_hp = function()
			return health("target")
		end,
		
		has_target = function()
			if UnitName("target") and not UnitIsDead("target") then
				if UnitIsPlayer("target") then
					return 0
				end
			
				if UnitClassification("target") ~= "normal" then
					return 0
				end
				
				if UnitReaction("player", "target") > 4 then
					return 0
				end
			
				local _, class = UnitClass("player")
				if class == "MAGE" then
					if IsSpellInRange("Fireball") == 0 then
						return 0
					end
				end
			
				return 1
			else
				return 0
			end
		end,
		
		player_position = function()
			local x, y = UnitPosition("player")
			x = string.format("%.1f", x)
			y = string.format("%.1f", y)
			return tostring(x) .. "=" .. y
		end,
		
		player_angle = function()
			return string.format("%.2f", GetPlayerFacing())
		end,
		
		is_casting = function()
			return bool_to_num(mutable_state.is_casting)
		end,
		
		current_fishing_level = function()
			if not mutable_state.fishing_skill_line_index then -- Fishing not learned
				return 0
			end
			if mutable_state.fishing_skill_line_index == -1 then
				return nil
			end
			local _, _, _, current = GetSkillLineInfo(mutable_state.fishing_skill_line_index)
			return current
		end,
		
		max_fishing_level = function()
			if not mutable_state.fishing_skill_line_index then -- Fishing not learned
				return 0
			end
			if mutable_state.fishing_skill_line_index == -1 then
				return nil -- By returning nil this will cause the text reading to fail, I want this until 
			end
			local _, _, _, _, _, _, max_skill = GetSkillLineInfo(mutable_state.fishing_skill_line_index)
			return max_skill
		end,
		
		error = function()
			return tostring(mutable_state.last_error_type) .. "=" .. mutable_state.current_error_id
		end,
		
		player_in_combat = function()
			return bool_to_num(UnitAffectingCombat("player"))
		end,
		
		target_in_combat = function()
			return bool_to_num(UnitAffectingCombat("target"))
		end,
		
		level_xp = function()
			return UnitXP("player")
		end,
		
		level_xp_max = function()
			return UnitXPMax("player")
		end,
		
		mage_water_count = function()
			return fish_get_item_count(FISH_MAGE_WATER) -- FISH_MAGE_WATER is from rest.lua
		end,
		
		mage_food_count = function()
			return fish_get_item_count(FISH_MAGE_FOOD) -- FISH_MAGE_FOOD is from rest.lua
		end,
		
		on_taxi = function()
			return bool_to_num(UnitOnTaxi("player"))
		end,
		
		player_speed = function()
			local current_speed = GetUnitSpeed("player")
			return string.format("%.1f", current_speed)
		end,
		
		can_retrieve_corpse = function()
			return bool_to_num(mutable_state.can_retrieve_corpse)
		end,
		
		is_hearthstone_on_cd = function()
			local start_time = GetItemCooldown(6948)
			local result = start_time ~= 0
			return bool_to_num(result)
		end,
		
		report_bag_fish = function(name)
			return function()
				return count_in_bag(name)
			end
		end,
		
		report_mail_fish = function(name)
			return function()
				return count_in_mail(name)
			end
		end,
		
		mailbox_open = function()
			return bool_to_num(mutable_state.mailbox_open)
		end,
		
		ah_window_open = function()
			return bool_to_num(mutable_state.ah_open)
		end,
		
		merchant_open = function()
			return bool_to_num(mutable_state.merchant_open)
		end,
		
		current_ah_search_listing = function(page)
			return function()
				local listing = get_paged_ah_listing(page)
				local count = table.getn(listing)
				local result = ""
				result = page .. "=" .. count
				if count > 0 then
					for index = 1,count do
						local prizing = listing[index]
						result = result .. "=" .. prizing.prize .. "=" .. prizing.count
					end
				end
				return result
			end
		end,
		
		player_gold = function()
			return math.floor(GetMoney() / 100 / 100)
		end,
		
		empty_bag_slots = function()
			local count = 0
			for index = 0,4 do
				count = count + GetContainerNumFreeSlots(index)
			end
			return count
		end,
		
		player_swimming = function()
			return bool_to_num(IsSwimming())
		end,
		
		corpse_pos = function()
			local x, y = corpse_position()
			x = string.format("%.1f", x)
			y = string.format("%.1f", y)
			return tostring(x) .. "=" .. y
		end
	}
	
	return TRANSMIT_FNS_STATE.LOADED_TRANSMIT_FNS
end