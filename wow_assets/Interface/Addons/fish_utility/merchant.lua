
function get_merchant_item_index(item_name)
	for index=1,GetMerchantNumItems(name) do
		local name = GetMerchantItemInfo(index)
		if name == item_name then
			return index
		end
	end
end

function ensure_item_count(item_name, count)
	local current_item_count = GetItemCount(item_name)
	local merchant_index = get_merchant_item_index(item_name)
	print(merchant_index)
	if not merchant_index then
		return;
	end
	
	while current_item_count < count do
		local total_left_to_buy = count - current_item_count
		local item_count_to_buy = 20
		if total_left_to_buy < item_count_to_buy then
			item_count_to_buy = total_left_to_buy
		end
	
		BuyMerchantItem(merchant_index, item_count_to_buy)
		current_item_count = current_item_count + item_count_to_buy
	end
end