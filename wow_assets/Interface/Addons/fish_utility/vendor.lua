
function vendor_everything()
	if GetMerchantNumItems() == 0 then
		return -- No merchant window is open
	end

	for bag = 0,4,1 do 
		for slot = 1, 32, 1 do 
			local name = GetContainerItemLink(bag,slot);
			if name and name ~= "Fishing Pole" then
				UseContainerItem(bag,slot); 
			end
		end
	end
	CloseAllBags()
end

function vendor_close()
	CloseMerchant()
end