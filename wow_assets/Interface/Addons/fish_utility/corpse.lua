local hbd = LibStub("HereBeDragons-2.0")

function GetCorpseLocation()
    -- If the player isn't dead or a ghost, drop out
    if not UnitIsDeadOrGhost("player") then
        if m or x or y then
            ClearCorpseArrow()
        end
    end

    -- Cache the result so we don't scan the maps multiple times
    if m and x and y then
        return m, x, y
    end

    --  See if the player corpse is on the current map
    local om = C_Map.GetBestMapForUnit("player")
    if not om then return nil; end
    local corpse = C_DeathInfo.GetCorpseMapPosition(om)
    if not corpse then return nil; end
    local cx, cy = corpse:GetXY()
    if cx ~= 0 and cy ~= 0 then
        m = om
        x = cx
        y = cy
    end
    return m, x, y
end

function corpse_position()
	local map, x, y = GetCorpseLocation()
	if not map then
		return nil, nil
	end
	
	local y, x = hbd:GetWorldCoordinatesFromZone(x, y, map) -- This lib is trolling
	return x, y
end