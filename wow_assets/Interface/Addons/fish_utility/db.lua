-- GLOBAL VARIABLE: DB_CHAR_DATA

local FNS = load_transmit_fns()
local FRAME = CreateFrame("Frame")

local function update_db()
	-- Updates DB_CHAR_DATA
	local _, toon_id = BNGetInfo()
	DB_CHAR_DATA = {
		["bn_toon_id"] = toon_id,
		["char_name"] = FNS.player_name(),
		["region"] = FNS.region(),
		["realm"] = FNS.realm(),
		["race"] = FNS.player_race(),
		["class"] = FNS.player_class(),
		["level"] = FNS.player_level(),
		["current_fishing"] = FNS.current_fishing_level(),
		["max_fishing"] = FNS.max_fishing_level(),
		["gold"] = FNS.player_gold(),
		["last_update_time"] = date("%y/%m/%d %H:%M:%S")
	}
end

local function update_every_once_in_a_while()
	fish_wait(60, function()
		update_db()
		update_every_once_in_a_while()
	end)
end

update_every_once_in_a_while()

FRAME:RegisterEvent("ADDON_LOADED")
FRAME:RegisterEvent("PLAYER_LOGOUT")
FRAME:SetScript("OnEvent", function(self, ev)
	fish_wait(5, update_db)
end)