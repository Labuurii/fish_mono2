function take_taxi(place)
	SelectGossipOption(1)
	
	for i = 1, NumTaxiNodes() do
		if TaxiNodeName(i) == place then
			TakeTaxiNode(i)
		end
	end
end

function print_taxi_nodes()
	for i = 1, NumTaxiNodes() do
		print(TaxiNodeName(i))
	end
end