
local DEBUG_MODE = false

local SILVERPINE_WHITELIST = {
	"Hearthstone",
	"Oily Blackmouth"
}

local FISH_WHITELIST = {
	"Fishing Pole",
	"Hearthstone",
	"Oily Blackmouth",
	"Firefin Snapper",
	"Winter Squid",
	"Summer Bass",
	"Nightfin Snapper",
	"mouth Clam",
	"Stonescale Eel",
	"Bright Baubles",
	"Golden Pearl",
	"Black Pearl",
	"Iridescent Pearl",
	"Small Lustrous Pearl",
	"Superior Healing Potion"
}

local function string_contains_any_of(str, list_of_options)
	for i,option in ipairs(list_of_options) do
		if string.find(str, option) then
			return true
		end
	end
	
	return false
end

local function delete_items_with_whitelist(whitelist) 
	for bag = 0,4 do 
		for slot = 1, 32 do 
			local name = GetContainerItemLink(bag,slot); 
			if name and not string_contains_any_of(name, whitelist) then 
				if DEBUG_MODE then
					print(name)
				else
					PickupContainerItem(bag,slot); 
					DeleteCursorItem();
				end
			end
		end
	end
end

local function delete_items_with_blacklist(blacklist) 
	for bag = 0,4 do 
		for slot = 1, 32 do 
			local name = GetContainerItemLink(bag,slot); 
			if name and string_contains_any_of(name, blacklist) then 
				if DEBUG_MODE then
					print(name)
				else
					PickupContainerItem(bag,slot); 
					DeleteCursorItem();
				end
			end
		end
	end
end

function whitelist_items_silverpine()
	delete_items_with_whitelist(SILVERPINE_WHITELIST)
end

function fish_cleanup()
	delete_items_with_whitelist(FISH_WHITELIST)
end

function delete_fishing_pole()
	local blacklist = { "Fishing Pole" }
	delete_items_with_blacklist(blacklist)
end

function fish_apply_buff()
	for bag = 0,4,1 do 
		for slot = 1, 32, 1 do 
			local name = GetContainerItemLink(bag,slot);
			if name and string.find(name, "Bright Baubles") then
				print("Found bright baubles")
				UseContainerItem(bag,slot); 
				break
			end
		end
	end
end

CameraZoomIn(50)