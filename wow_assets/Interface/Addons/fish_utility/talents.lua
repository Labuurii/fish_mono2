
local MAGE_TALENT_POINTS = {
	["Improved Frostbolt"] = 5,
	["Ice Shards"] = 5
}

local TALENT_NAME_INDEX_MAPPING = {}

local function learn_talents_impl(talent_points)
	for name, count in pairs(talent_points) do
		local index = TALENT_NAME_INDEX_MAPPING[name]
		if not index then
			error("There are no talent named " .. name)
		end
		
		for i = 1, count do
			LearnTalent(index.tab, index.talent)
		end
	end
end

local function learn_talents()
	local _,class = UnitClass("player")
	if class == "MAGE" then
		learn_talents_impl(MAGE_TALENT_POINTS)
	end
end

fish_wait(2, function()
	for tab_index = 1, 3 do
		for talent_index = 1, GetNumTalents(tab_index) do
			local name = GetTalentInfo(tab_index, talent_index)
			TALENT_NAME_INDEX_MAPPING[name] = {
				tab = tab_index,
				talent = talent_index
			}
		end
	end

	learn_talents()
end)

local FRAME = CreateFrame("Frame")
FRAME:RegisterEvent("PLAYER_LEVEL_UP")
FRAME:SetScript("OnEvent", function()
	learn_talents()
end)