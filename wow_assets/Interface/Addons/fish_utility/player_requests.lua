

local function handle_duel_requested()
	fish_wait(math.random(1, 3), CancelDuel)
end

local function handle_trade_requested()
	fish_wait(math.random(0.5, 2), CloseTrade)
end

local function handle_group_invite()
	DeclineGroup()
end

local function handle_player_death()
	RepopMe()
end


local FRAME = CreateFrame("Frame")
FRAME:RegisterEvent("DUEL_REQUESTED")
FRAME:RegisterEvent("TRADE_SHOW")
FRAME:RegisterEvent("PARTY_INVITE_REQUEST")
FRAME:RegisterEvent("PLAYER_DEAD")
FRAME:SetScript("OnEvent", function(self, event)
	print(event)
	if event == "DUEL_REQUESTED" then
		handle_duel_requested()
	elseif event == "TRADE_SHOW" then
		handle_trade_requested()
	elseif event == "PARTY_INVITE_REQUEST" then
		handle_group_invite()
	elseif event == "PLAYER_DEAD" then
		handle_player_death()
	end
end)