
local DEBUG = false
local PAGE_SIZE = 8

local PRIZINGS_PAGED = {}

local FRAME = CreateFrame("FRAME")
FRAME:RegisterEvent("AUCTION_ITEM_LIST_UPDATE")
FRAME:SetScript("OnEvent", function(self, event)
	PRIZINGS_PAGED = {}
	
	local player_name = UnitName("player")
	
	-- Hashtable of <prize in silver, count>
	local prizings = {}
	
	-- Get all the listings
	for index = 1,100 do
		local item_name,_, quantity ,_,_,_,_,_,_, buyout_prize_copper, _,_,_, lister_name = GetAuctionItemInfo("list", index)
		if not item_name then
			break
		end
		
		if lister_name ~= player_name then
			buyout_prize_copper = buyout_prize_copper / quantity
			local buyout_prize_silver = math.floor(buyout_prize_copper / 100)
		
			local prizing_count = prizings[buyout_prize_silver]
			if not prizing_count then
				prizing_count = 0
				prizings[buyout_prize_silver] = 0
			end
			prizings[buyout_prize_silver] = prizing_count + 1
		end
	end
	
	-- Fill the paged prizings table
	local current_page = {}
	table.insert(PRIZINGS_PAGED, current_page)
	for prize, count in pairs(prizings) do
		table.insert(current_page, { prize = prize, count = count })
		if table.getn(current_page) >= PAGE_SIZE then
			current_page = {}
			table.insert(PRIZINGS_PAGED, current_page)
		end
	end
	
	if DEBUG then
		for index, prizing_page in ipairs(PRIZINGS_PAGED) do
			print(table.getn(prizing_page))
			for prize_index, prizing in ipairs(prizing_page) do
				print(prizing.prize, prizing.count)
			end
			print("====")
		end
	end
end)

-- Returns an array of: { prize (in silver), count }
function get_paged_ah_listing(page)
	local page = PRIZINGS_PAGED[page]
	if not page then
		return {}
	end
	return page
end

function auction_all_items(name, prize_silver)
	local prize_copper = prize_silver * 100
	
	CancelSell()
	ClearCursor()
	
	while true do
		local count_items_picked_up = fish_pickup_item(name)
		if count_items_picked_up == 0 then
			break
		end
		
		ClickAuctionSellItemButton()
		local buyout_prize = prize_copper * count_items_picked_up
		local bid_prize = buyout_prize - ( 10 * 100 )
		if bid_prize <= 0 then
			bid_prize = buyout_prize
		end
		
		StartAuction(bid_prize, buyout_prize, 2) -- This call has to be done from a click
		break
	end
	

end