
local DEBUG = false

local FISH_QUOTAS = {
	["Oily Blackmouth"] = 20,
	["Firefin Snapper"] = 25,
	["Winter Squid"] = 18,
	["Raw Summer Bass"] = 10,
	["Raw Nightfin Snapper"] = 25,
	["Stonescale Eel"] = 25
}

local function delete_item_stacks_with_count(item_name, count, max_deletes)
	local deletes = 0
	for bag = 0,4 do 
		for slot = 1, 32 do 
			local _, slot_count, _,_,_,_, item_link = GetContainerItemInfo(bag, slot)
			if item_link and string.find(item_link, item_name) and slot_count == count then
				if DEBUG then
					print(item_link, bag, slot)
				else
					PickupContainerItem(bag,slot)
					DeleteCursorItem()
				end
				
				deletes = deletes + 1
				if deletes >= max_deletes then
					return
				end
			end
		end
	end
end

local function delete_count_items(item_name, count) -- Stack size is assumed 20
	if count > 19 then
		delete_item_stacks_with_count(item_name, 20, math.floor(count / 20))
	end
	
	local small_stack_count = count % 20
	if small_stack_count ~= 0 then
		delete_item_stacks_with_count(item_name, small_stack_count, 1)
	end
end

function destroy_overquoted_fish()
	for name, stack_quota in pairs(FISH_QUOTAS) do
		local item_quota = stack_quota * 20
		local item_count = GetItemCount(name)
		if item_count > item_quota then
			if DEBUG then
				print(name, item_count / 20)
			end
			
			local item_count_to_delete = item_count - item_quota
			delete_count_items(name, item_count_to_delete)
		end
	end
end