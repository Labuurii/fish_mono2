local ACTIONBAR_OFFSETS = {
	[1] = 1,
	[2] = 13,
	[3] = 25, -- Right 1
	[4] = 37, -- Right 2
	[5] = 49, -- Bottom Right
	[6] = 61, -- Bottom Left
}

local KEYBINDINGS = {
	["NUMLOCK"] = "NONE",
	["F1"] = "MULTIACTIONBAR3BUTTON1",
	["SHIFT-F1"] = "MULTIACTIONBAR4BUTTON1",
	["F2"] = "MULTIACTIONBAR3BUTTON2",
	["F3"] = "MULTIACTIONBAR3BUTTON3",
	["F4"] = "MULTIACTIONBAR3BUTTON4",
	["F5"] = "MULTIACTIONBAR3BUTTON5",
	["SHIFT-F2"] = "MULTIACTIONBAR4BUTTON2",
	["SHIFT-F3"] = "MULTIACTIONBAR4BUTTON3",
	["SHIFT-F4"] = "MULTIACTIONBAR4BUTTON4",
	["SHIFT-F5"] = "MULTIACTIONBAR4BUTTON5",
	["F8"] = "STOPAUTORUN",
	["F9"] = "STARTAUTORUN",
	["F10"] = "MULTIACTIONBAR3BUTTON10",
	["F11"] = "MULTIACTIONBAR3BUTTON11",
	["F12"] = "MULTIACTIONBAR3BUTTON12",
	["F6"] = "MULTIACTIONBAR3BUTTON6",
	["F7"] = "MULTIACTIONBAR3BUTTON7",
	["SHIFT-F6"] = "MULTIACTIONBAR4BUTTON6",
	["SHIFT-F7"] = "MULTIACTIONBAR4BUTTON7",
	["SHIFT-F8"] = "MULTIACTIONBAR4BUTTON8",
	["SHIFT-F9"] = "MULTIACTIONBAR4BUTTON9",
	["SHIFT-F10"] = "MULTIACTIONBAR4BUTTON10",
	["SHIFT-F11"] = "MULTIACTIONBAR4BUTTON11",
	["SHIFT-F12"] = "MULTIACTIONBAR4BUTTON12",
	["CTRL-F1"] = "MULTIACTIONBAR2BUTTON1",
	["CTRL-F2"] = "MULTIACTIONBAR2BUTTON2",
	["CTRL-F3"] = "MULTIACTIONBAR2BUTTON3",
	["CTRL-F4"] = "MULTIACTIONBAR2BUTTON4",
	["CTRL-F5"] = "MULTIACTIONBAR2BUTTON5",
	["CTRL-F6"] = "MULTIACTIONBAR2BUTTON6",
	["CTRL-F7"] = "MULTIACTIONBAR2BUTTON7",
	["CTRL-F8"] = "MULTIACTIONBAR2BUTTON8",
	["CTRL-F9"] = "MULTIACTIONBAR2BUTTON9",
	["CTRL-F10"] = "MULTIACTIONBAR2BUTTON10",
	["CTRL-F11"] = "MULTIACTIONBAR2BUTTON11",
	["CTRL-F12"] = "TOGGLERUN",
	
	["R"] = "MULTIACTIONBAR1BUTTON1",
	["ALT-1"] = "MULTIACTIONBAR1BUTTON2",
	["ALT-2"] = "MULTIACTIONBAR1BUTTON3",
	["ALT-3"] = "MULTIACTIONBAR1BUTTON4",
	["ALT-4"] = "MULTIACTIONBAR1BUTTON5",
	["ALT-5"] = "MULTIACTIONBAR1BUTTON6",
	["ALT-6"] = "MULTIACTIONBAR1BUTTON7",
	["ALT-7"] = "MULTIACTIONBAR1BUTTON8",
	["ALT-8"] = "MULTIACTIONBAR1BUTTON9",
	["ALT-9"] = "MULTIACTIONBAR1BUTTON10",
	["ALT-0"] = "MULTIACTIONBAR1BUTTON11",
	["ALT-+"] = "MULTIACTIONBAR1BUTTON12",
}

local DRUID_SPELLS = {
	["Wrath"] = ACTIONBAR_OFFSETS[1] + 1,
	["Healing Touch"] = ACTIONBAR_OFFSETS[1] + 2
}

local MAGE_SPELLS = {
	["Frostbolt"] = ACTIONBAR_OFFSETS[1],
	["Fireball"] = ACTIONBAR_OFFSETS[1] + 1,
	["Fire Blast"] = ACTIONBAR_OFFSETS[1] + 2,
	["Berserking"] = ACTIONBAR_OFFSETS[1] + 3,
	["Frost Nova"] = ACTIONBAR_OFFSETS[1] + 4,
	["Frost Armor"] = ACTIONBAR_OFFSETS[1] + 5,
	["Arcane Intellect"] = ACTIONBAR_OFFSETS[1] + 6,
	["Conjure Water"] = ACTIONBAR_OFFSETS[1] + 7,
	["Conjure Food"] = ACTIONBAR_OFFSETS[1] + 8
}

local MACRO_BINDINGS = {
	["t_taxi"] = ACTIONBAR_OFFSETS[5],
	["t_get_home"] = ACTIONBAR_OFFSETS[5] + 1,
	["hearthstone"] = ACTIONBAR_OFFSETS[5] + 2,
	["t_bag_report"] = ACTIONBAR_OFFSETS[5] + 3,
	["t_ah_open"] = ACTIONBAR_OFFSETS[5] + 4,
	["destroy_overquoted"] = ACTIONBAR_OFFSETS[5] + 5,
	["t_mail_report"] = ACTIONBAR_OFFSETS[5] + 6,
	["t_merchant_open"] = ACTIONBAR_OFFSETS[5] + 7,
	["buy_bright_baubles"] = ACTIONBAR_OFFSETS[5] + 8,
	["hide_script_errors"] = ACTIONBAR_OFFSETS[5] + 9,
	["t_corpse_pos"] = ACTIONBAR_OFFSETS[5] + 10,

	["t_end"] = ACTIONBAR_OFFSETS[3],
	["t_reset_counter"] = ACTIONBAR_OFFSETS[3] + 1,
	["t_char_summary"] = ACTIONBAR_OFFSETS[3] + 2,
	["t_combat"] = ACTIONBAR_OFFSETS[3] + 3,
	["t_pathing"] = ACTIONBAR_OFFSETS[3] + 4,
	["t_level_chour"] = ACTIONBAR_OFFSETS[3] + 5,
	["t_mage_rest"] = ACTIONBAR_OFFSETS[3] + 6,
	["stopattack"] = ACTIONBAR_OFFSETS[3] + 9,
	["startattack"] = ACTIONBAR_OFFSETS[3] + 10,
	["cleartarget"] = ACTIONBAR_OFFSETS[3] + 11,
	
	["vendor_sell_all"] = ACTIONBAR_OFFSETS[4],
	["vendor_close"] = ACTIONBAR_OFFSETS[4] + 1,
	["trainer_close"] = ACTIONBAR_OFFSETS[4] + 2,
	["trainer_all"] = ACTIONBAR_OFFSETS[4] + 3,
	["place_spells"] = ACTIONBAR_OFFSETS[4] + 4,
	["hide_ui"] = ACTIONBAR_OFFSETS[4] + 5,
	["show_ui"] = ACTIONBAR_OFFSETS[4] + 6,
	["mage_drink"] = ACTIONBAR_OFFSETS[4] + 7,
	["mage_eat"] = ACTIONBAR_OFFSETS[4] + 8,
	["cleanup_mage_water"] = ACTIONBAR_OFFSETS[4] + 9,
	["cleanup_mage_food"] = ACTIONBAR_OFFSETS[4] + 10,
	["camera_zoom_in"] = ACTIONBAR_OFFSETS[4] + 11,
	
	["do_manual_stuff"] = ACTIONBAR_OFFSETS[6],
	["camera_down"] = ACTIONBAR_OFFSETS[6] + 1,
	["camera_up"] = ACTIONBAR_OFFSETS[6] + 2,
}

local FISHING_MACRO_BINDINGS = {
	["fish"] = ACTIONBAR_OFFSETS[1],
	["cleanup"]= ACTIONBAR_OFFSETS[1] + 1,
	["stopcasting"]= ACTIONBAR_OFFSETS[1] + 2,
	["bright_baubles"]= ACTIONBAR_OFFSETS[1] + 3,
	["unequip_pole"]= ACTIONBAR_OFFSETS[1] + 4,
	["equip_pole"]= ACTIONBAR_OFFSETS[1] + 5,
	["open_clam"]= ACTIONBAR_OFFSETS[1] + 6,
}

local MACRO_TEXT = {
	["t_end"] = "/run transmit_end()",
	["t_reset_counter"] = "/run transmit_counter_reset()",
	["t_char_summary"] = "/run transmit_char_summary()",
	["t_combat"] = "/run transmit_combat_data()",
	["t_pathing"] = "/run transmit_pathing_data()",
	["t_level_chour"] = "/run transmit_level_chour_data()",
	["t_mage_rest"] = "/run transmit_mage_rest_data()",
	["t_taxi"] = "/run transmit_taxi()",
	["t_get_home"] = "/run transmit_get_home()",
	["t_bag_report"] = "/run transmit_bag_report()",
	["t_ah_open"] = "/run transmit_ah_open()",
	["t_mail_report"] = "/run transmit_mail_report()",
	["t_merchant_open"] = "/run transmit_merchant_window_open()",
	["t_corpse_pos"] = "/run transmit_corpse_pos()",
	["stopattack"] = "/stopattack",
	["startattack"] = "/stopattack",
	["cleartarget"] = "/cleartarget",
	["hide_script_errors"] = "/console scriptErrors 0",
	
	["vendor_sell_all"] = "/run vendor_everything()",
	["vendor_close"] = "/run vendor_close()",
	["trainer_close"] = "/run CloseTrainer()",
	["trainer_all"] = "/run train_everything()",
	["place_spells"] = "/run place_spells()",
	["hide_ui"] = "/run hide_ui()",
	["show_ui"] = "/run show_ui()",
	["mage_drink"] = "/use Conjured Fresh Water\n/use Conjured Water",
	["mage_eat"] = "/use Conjured Bread\n/use Conjured Muffin",
	["cleanup_mage_water"] = "/run cleanup_space_for_mage_water()",
	["cleanup_mage_food"] = "/run cleanup_space_for_mage_food()",
	
	["camera_zoom_in"] = "/run CameraZoomIn(50)",
	["do_manual_stuff"] = "/run do_manual_stuff()",
	["camera_down"] = "/run fish_run_and_then(3, MoveViewDownStart, MoveViewDownStop)",
	["camera_up"] = "/run fish_run_and_then(3, MoveViewUpStart, MoveViewUpStop)",
	["hearthstone"] = "/use Hearthstone",
	["destroy_overquoted"] = "/run destroy_overquoted_fish()",
	
	["fish"] = "/cleartarget\n/cast Fishing",
	["cleanup"] = "/run fish_cleanup()",
	["stopcasting"] = "/stopcasting",
	["bright_baubles"] = "/use Bright Baubles\n/use 16",
	["unequip_pole"] = "/run PickupInventoryItem(16);fish_put_cursor_item_in_bag()",
	["equip_pole"] = "/equip Fishing Pole",
	["open_clam"] = "/use Big-mouth Clam",
	["buy_bright_baubles"] = "/run delete_fishing_pole(); fish_wait(0.5, function() ensure_item_count(\"Bright Baubles\", 100); end); fish_wait(1, function() ensure_item_count(\"Fishing Pole\", 1) end)"
}

function place_keybindings()
	for key,action in pairs(KEYBINDINGS) do
		SetBinding(key, action, 1)
	end
end

function get_keybinding_for_macro(macro)
	return MACRO_BINDINGS[macro]
end

local function place_spells_impl(spells)
	for spell_name,binding_id in pairs(spells) do
		local _,_,_,_,_,_,spell_id = GetSpellInfo(spell_name)
		if spell_id then
			PickupSpell(spell_id)
			PlaceAction(binding_id)
		end
	end
	ClearCursor()
end

local function place_macros_impl(bindings)
	for macro_name, binding_id in pairs(bindings) do
		PickupMacro(macro_name)
		PlaceAction(binding_id)
	end
	ClearCursor()
end

function place_spells()
	local _, class = UnitClass("player")
	if class == "DRUID" then
		place_spells_impl(DRUID_SPELLS)
	elseif class == "MAGE" then
		place_spells_impl(MAGE_SPELLS)
	end
end

function place_macros()
	place_macros_impl(MACRO_BINDINGS)
end

function place_fishing_macros()
	place_macros_impl(FISHING_MACRO_BINDINGS)
end

function create_macros()
	for macro_name, macro_text in pairs(MACRO_TEXT) do
		local exists = GetMacroInfo(macro_name)
		if exists then
			EditMacro(macro_name, macro_name, "INV_MISC_QUESTIONMARK", macro_text)
		else
			CreateMacro(macro_name, "INV_MISC_QUESTIONMARK", macro_text)
		end
	end
end

function do_manual_stuff()
	vendor_everything()
	RetrieveCorpse()
	SelectGossipOption(1)
	AcceptXPLoss()
	
	local fishing_pole_index = get_merchant_item_index("Fishing Pole")
	if fishing_pole_index then
		BuyMerchantItem(fishing_pole_index, 1)
	end
	
	local num_services = GetNumTrainerServices()	
	for index = 1,num_services do
		local name = GetTrainerServiceInfo(index)
		if name and string.find(name, "Fishing") then
			BuyTrainerService(index)
		end
	end
	
	ConfirmBinder()
end

fish_wait(0.1, function()
	place_keybindings()
	create_macros()
	place_macros()
	if UnitLevel("player") < 20 then
		place_spells()
	else
		place_fishing_macros()
	end
end)