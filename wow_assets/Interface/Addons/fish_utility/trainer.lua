
function train_everything()
	SelectGossipOption(1)
	local num_services = GetNumTrainerServices()
	if num_services == 0 then
		return
	end
	
	local _, class = UnitClass("player")
	if class == "MAGE" then
		for index = 1,num_services do
			local name = GetTrainerServiceInfo(index)
			if name == "Frostbolt" or name == "Conjure Food" or name == "Conjure Water" or name == "Arcane Intellect" or name == "Frost Armor" or name == "Fire Blast" or name == "Frost Nova" then
				BuyTrainerService(index)
			end
		end
	end
end