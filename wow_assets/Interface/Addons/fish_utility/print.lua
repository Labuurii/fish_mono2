
function clear_chat()
	SELECTED_CHAT_FRAME:Clear()
end

function print_pos()
	local x, y = UnitPosition("player")
	x = string.format("%.1f", x) .. "f"
	y = string.format("%.1f", y) .. "f"

	print(x,y)
end

function print_pos_csharp()
	local x, y = UnitPosition("player")
	x = string.format("%.1f", x) .. "f,"
	y = string.format("%.1f", y) .. "f"

	print("new Vector2(",x,y,"),")
end