local WORK_LOAD = {
	current_work_index = 0,
	data = nil,
	FNS = load_transmit_fns()
}

local FRAME = CreateFrame("Frame","FRAMEFRAME")
FRAME:SetBackdrop({
    bgFile="Interface\\Buttons\\UI-Slot-Background",
})
FRAME:SetWidth(8000)
FRAME:SetHeight(5000)
FRAME:SetPoint("TOPLEFT", -500, 40)

local TEXT = FRAME:CreateFontString(nil, "OVERLAY", "GameFontNormal")
TEXT:SetFont("Fonts\\FRIZQT__.TTF", 200)
TEXT:SetSpacing(70)
TEXT:SetPoint("TOPLEFT", 505, -45)
TEXT:SetTextColor(1,1,1)
TEXT:SetJustifyH("LEFT")
TEXT:SetJustifyV("TOP")
TEXT:SetWidth(2800)
TEXT:SetNonSpaceWrap(true)

local REPORT_FISHES = {
	"Oily Blackmouth",
	"Firefin Snapper",
	"Winter Squid",
	"Raw Summer Bass",
	"Raw Nightfin Snapper",
	"Big-mouth Clam",
	"Stonescale Eel",
	"Golden Pearl",
	"Superior Healing Potion",
}

local function construct_fish_transmit_fields(field_gen)
	local result = {}
	for index, fish_name in ipairs(REPORT_FISHES) do
		table.insert(result, field_gen(fish_name))
	end
	return result
end

FRAME:SetScript("OnUpdate", function()
	if WORK_LOAD.data then
		local result = "TT"
		for i = 1,WORK_LOAD.current_work_index do
			result = result .. "T"
		end
		result = result .. "="
		for i,callback in ipairs(WORK_LOAD.data) do
			local cb_res = callback()
			cb_res = tostring(cb_res)
			cb_res = string.gsub(cb_res, "-", "N")
			result = result .. cb_res .. "="
		end
		
		result = result .. "TT"
		
		TEXT:SetText(result)
	end
end)

FRAME:Hide()

local function new_transmit(data_callbacks)
	WORK_LOAD.current_work_index = WORK_LOAD.current_work_index + 1
	WORK_LOAD.data = data_callbacks
	FRAME:Show()
end

function transmit_end()
	FRAME:Hide()
end

function transmit_counter_reset()
	transmit_end()
	WORK_LOAD.current_work_index = 0
end

function transmit_char_summary()
	new_transmit({
		WORK_LOAD.FNS.player_race,
		WORK_LOAD.FNS.player_class,
		WORK_LOAD.FNS.player_level,
		WORK_LOAD.FNS.current_fishing_level,
		WORK_LOAD.FNS.max_fishing_level,
		WORK_LOAD.FNS.player_gold
	})
end

function transmit_combat_data()
	new_transmit({
		WORK_LOAD.FNS.player_hp,
		WORK_LOAD.FNS.player_mana,
		WORK_LOAD.FNS.player_position,
		WORK_LOAD.FNS.player_angle,
		WORK_LOAD.FNS.is_casting,
		WORK_LOAD.FNS.player_in_combat,
		WORK_LOAD.FNS.target_in_combat,
		WORK_LOAD.FNS.has_target,
		WORK_LOAD.FNS.target_hp,
		WORK_LOAD.FNS.error
	})
end

function transmit_pathing_data()
	new_transmit({
		WORK_LOAD.FNS.player_position,
		WORK_LOAD.FNS.player_angle,
		WORK_LOAD.FNS.player_hp,
		WORK_LOAD.FNS.player_mana,
		WORK_LOAD.FNS.player_in_combat,
		WORK_LOAD.FNS.is_casting,
		WORK_LOAD.FNS.has_target,
		WORK_LOAD.FNS.player_level,
		WORK_LOAD.FNS.player_speed,
		WORK_LOAD.FNS.can_retrieve_corpse,
		WORK_LOAD.FNS.player_swimming
	})
end

function transmit_level_chour_data()
	new_transmit({
		WORK_LOAD.FNS.player_position,
		WORK_LOAD.FNS.player_level,
		WORK_LOAD.FNS.level_xp,
		WORK_LOAD.FNS.level_xp_max
	})
end

function transmit_mage_rest_data()
	new_transmit({
		WORK_LOAD.FNS.mage_water_count,
		WORK_LOAD.FNS.mage_food_count
	})
end

function transmit_taxi()
	new_transmit({
		WORK_LOAD.FNS.on_taxi
	})
end

function transmit_get_home()
	new_transmit({
		WORK_LOAD.FNS.is_hearthstone_on_cd,
		WORK_LOAD.FNS.player_position,
		WORK_LOAD.FNS.player_hp,
		WORK_LOAD.FNS.is_casting,
		WORK_LOAD.FNS.player_in_combat,
		WORK_LOAD.FNS.mailbox_open,
		WORK_LOAD.FNS.on_taxi,
	})
end

function transmit_bag_report()
	local fields = construct_fish_transmit_fields(WORK_LOAD.FNS.report_bag_fish)
	table.insert(fields, WORK_LOAD.FNS.empty_bag_slots)
	new_transmit(fields)
end

function transmit_mail_report()
	local fields = construct_fish_transmit_fields(WORK_LOAD.FNS.report_mail_fish)
	new_transmit(fields)
end

function transmit_ah_open()
	new_transmit({
		WORK_LOAD.FNS.ah_window_open
	})
end

function transmit_current_ah_listing(page)
	new_transmit({
		WORK_LOAD.FNS.current_ah_search_listing(page)
	})
end
t_cahl = transmit_current_ah_listing

function transmit_merchant_window_open()
	new_transmit({
		WORK_LOAD.FNS.merchant_open
	})
end

function transmit_corpse_pos()
	new_transmit({
		WORK_LOAD.FNS.corpse_pos
	})
end