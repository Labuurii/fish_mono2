
function take_out_all_money()
	local wait_time = 0
	local mail_count = GetInboxNumItems()
	for mail_index = mail_count, 1,-1 do
		local invoice_type = GetInboxInvoiceInfo(mail_index)
		if invoice_type == "seller" then
			local mail_index_to_use = mail_index
			wait_time = wait_time + 1
			fish_wait(wait_time, function()
				TakeInboxMoney(mail_index_to_use)
			end)
		end
	end
end

function take_out_count(target_name, max_count)
	local count_taken_out = 0
	
	local mail_count = GetInboxNumItems()
	for mail_index = 1, mail_count do
		local has_item = false
		for item_index = 1,69 do
			local name, _, _, count = GetInboxItem(mail_index, item_index)
			if name then
				has_item = true
				if name == target_name then
					print(target_name)
					fish_wait(mail_index, function()
						TakeInboxItem(mail_index, item_index)
					end)
					count_taken_out = count_taken_out + count
					
					if count_taken_out >= max_count then
						return
					end
				end
			end
		end
	end
end

function take_out_all(target_name)
	local wait_time = 0
	local mail_count = GetInboxNumItems()
	for mail_index = mail_count,1, -1 do
		for item_index = 69,1, -1 do
			local name, _, _, count = GetInboxItem(mail_index, item_index)
			if name then
				if name == target_name then
					local mail_index_to_use = mail_index
					wait_time = wait_time + 1
					fish_wait(wait_time, function()
						TakeInboxItem(mail_index, item_index)
					end)
				end
			end
		end
	end
end