local waitTable = {};
local waitFrame = nil;

function fish_wait(delay, func, ...)
  if(type(delay)~="number" or type(func)~="function") then
    return false;
  end
  if(waitFrame == nil) then
    waitFrame = CreateFrame("Frame","WaitFrame", UIParent);
    waitFrame:SetScript("onUpdate",function (self,elapse)
      local count = #waitTable;
      local i = 1;
      while(i<=count) do
        local waitRecord = tremove(waitTable,i);
        local d = tremove(waitRecord,1);
        local f = tremove(waitRecord,1);
        local p = tremove(waitRecord,1);
        if(d>elapse) then
          tinsert(waitTable,i,{d-elapse,f,p});
          i = i + 1;
        else
          count = count - 1;
          f(unpack(p));
        end
      end
    end);
  end
  tinsert(waitTable,{delay,func,{...}});
  return true;
end

function fish_get_item_count(items_lookup)
	local total_count = 0
	
	for bag_id = 0,4 do
		local bag_count = GetContainerNumSlots(bag_id)
		for slot_id = 1,bag_count do
			local id = GetContainerItemID(bag_id, slot_id)
			if id then
				local name = GetItemInfo(id)
				if items_lookup[name] then
					local _, count = GetContainerItemInfo(bag_id, slot_id)
					total_count = total_count + count
				end
			end
		end
	end
	
	return total_count
end

function fish_put_cursor_item_in_bag()
	for bag_id = 0,4 do
		local free_slots = GetContainerNumFreeSlots(bag_id)
		if free_slots and free_slots > 0 then
			if bag_id == 0 then
				PutItemInBackpack()
			else
				PutItemInBag(19 + bag_id) -- 20 is the first inventory slot id
			end
			break
		end
	end
end

function fish_pickup_item(name)
	for bag_id = 0,4 do
		local bag_count = GetContainerNumSlots(bag_id)
		for slot_id = 1,bag_count do
			local id = GetContainerItemID(bag_id, slot_id)
			if id then
				local item_name = GetItemInfo(id)
				if item_name == name then
					local _, count = GetContainerItemInfo(bag_id, slot_id)
					PickupContainerItem(bag_id, slot_id)
					return count
				end
			end
		end
	end
	
	return 0
end

function fish_run_and_then(delay, fn_1, fn_2)
	fn_1()
	fish_wait(delay, fn_2)
end