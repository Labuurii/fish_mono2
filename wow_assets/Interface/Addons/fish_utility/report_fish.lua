
function count_in_bag(name)
	return GetItemCount(name)
end

function count_in_mail(target_name)
	local result = 0

	local mail_count = GetInboxNumItems()
	for mail_index = 1, mail_count do
		for item_index = 1,69 do
			local name, _, _, count = GetInboxItem(mail_index, item_index)
			if not name then
				break
			end

			if name == target_name then
				result = result + count
			end
		end
	end
	
	return result
end