
FISH_MAGE_WATER = {
	["Conjured Water"] = 1,
	["Conjured Fresh Water"] = 1
}

FISH_MAGE_FOOD = {
	["Conjured Muffin"] = 1,
	["Conjured Bread"] = 1
}

local KEEP_WHITELIST = {
	["Hearthstone"] = 1,
	["Fishing Pole"] = 1,
	["Conjured Water"] = 1,
	["Conjured Fresh Water"] = 1,
	["Conjured Muffin"] = 1,
	["Conjured Bread"] = 1
}

local function count_total_bag_free_slots()
	local total_count = 0
	
	for bag_id = 0,4 do
		local count = GetContainerNumFreeSlots(bag_id)
		total_count = total_count + count
	end
	
	return total_count
end

local function make_sure_bag_is_not_full(whitelist)
	local left = count_total_bag_free_slots()
	if left == 0 then
	
		for bag_id = 0,4 do
			local bag_count = GetContainerNumSlots(bag_id)
			for slot_id = 1,bag_count do
				local id = GetContainerItemID(bag_id, slot_id)
				if id then
					local name = GetItemInfo(id)
					if name and not whitelist[name] then
						PickupContainerItem(bag_id, slot_id)
						DeleteCursorItem()
						return
					end
				end
			end
		end
	end
end

function cleanup_space_for_mage_water()
	local water_count = fish_get_item_count(FISH_MAGE_WATER)
	if water_count == 0 then
		make_sure_bag_is_not_full(KEEP_WHITELIST)
	end
end

function cleanup_space_for_mage_food()
	local food_count = fish_get_item_count(FISH_MAGE_FOOD)
	if food_count == 0 then
		make_sure_bag_is_not_full(KEEP_WHITELIST)
	end
end