# BasicBuffHide

## [v8.2.0](https://github.com/funkydude/BasicBuffHide/tree/v8.2.0) (2019-06-26)
[Full Changelog](https://github.com/funkydude/BasicBuffHide/compare/v8.0.1...v8.2.0)

- bump toc  
- fix curse link  
