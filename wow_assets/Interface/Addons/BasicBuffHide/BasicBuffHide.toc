## Interface: 80200
## Title: BasicBuffHide
## Notes: Hide the Blizzard Buff frame
## Author: Funkydude
## Version: v8.2.0
## X-Category: Buffs
## X-License: All Rights Reserved: You are free to fork and modify on GitHub, please ask us about anything else.
## X-Curse-Project-ID: 13502
## X-WoWI-ID: 21687

BasicBuffHide.lua

