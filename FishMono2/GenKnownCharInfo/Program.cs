﻿using CsvHelper;
using CsvHelper.Configuration;
using Neo.IronLua;
using Shared;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace GenCharacterCSV
{
    class Program
    {
        static List<string> get_accounts_paths(string base_path)
        {
            var r = new List<string>();
            var base_dir = Path.GetDirectoryName(base_path);
            var account_dir = Path.Combine(base_dir, "WTF\\Account");
            foreach (var dir in Directory.EnumerateDirectories(account_dir))
            {
                if (!dir.EndsWith("SavedVariables"))
                {
                    r.Add(dir);
                }
            }

            return r;
        }

        static List<string> get_servers(string base_path)
        {
            var r = new List<string>();
            foreach (var dir in Directory.EnumerateDirectories(base_path))
            {
                if (!dir.EndsWith("SavedVariables"))
                {
                    r.Add(dir);
                }
            }
            return r;
        }

        static List<string> get_characters(string base_path)
        {
            var r = new List<string>();
            r.AddRange(Directory.EnumerateDirectories(base_path));
            return r;
        }

        static FullWoWCharacterInfo try_read_wow_character_info(string char_path)
        {
            var file_path = Path.Combine(char_path, @"SavedVariables\fish_utility.lua");
            try
            {
                var text = File.ReadAllText(file_path, Encoding.UTF8);
                using (var lua = new Lua()) // Create the Lua script engine
                {
                    var env = lua.CreateEnvironment();
                    env.DoChunk(text, "fish_utility.lua");
                    var char_info = env.GetValue("DB_CHAR_DATA") as LuaTable;
                    if(char_info != null)
                    {
                        return new FullWoWCharacterInfo
                        {
                            bn_toon_id = (string)char_info["bn_toon_id"],
                            char_name = (string)char_info["char_name"],
                            region = (string)char_info["region"],
                            realm = (string)char_info["realm"],
                            race = (string)char_info["race"],
                            @class = (string)char_info["class"],
                            level = (int)char_info["level"],
                            current_fishing = (int)char_info["current_fishing"],
                            max_fishing = (int)char_info["max_fishing"],
                            gold = (int)char_info["gold"]
                        };
                    }
                }
            } catch
            {
                return null;
            }

            return null;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            var characters = new List<FullWoWCharacterInfo>();

            var account_paths = get_accounts_paths(WoWPaths.CLASSIC_WOW_PATH);
            foreach(var account_path in account_paths)
            {
                var servers = get_servers(account_path);
                foreach(var server in servers)
                {
                    var char_names = get_characters(server);
                    foreach(var char_path in char_names)
                    {
                        var info = try_read_wow_character_info(char_path);
                        if(info != null)
                        {
                            characters.Add(info);
                        }
                    }
                }
            }
            
            using(var writer = new CsvWriter(Console.Out, CultureInfo.InvariantCulture))
            {
                writer.WriteHeader<FullWoWCharacterInfo>();
                writer.NextRecord();
                writer.WriteRecords(characters);
            }
        }
    }
}
