﻿using CsvHelper;
using Shared;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenGoldReport
{
    class Program
    {
        // The key is the toon id
        static Dictionary<string, CSVWoWAccount> get_accounts_lookup()
        {
            var accounts = CSVWoWAccount.ReadAccounts();
            var r = new Dictionary<string, CSVWoWAccount>();
            foreach(var a in accounts)
            {
                r[a.bn_toon_id] = a;
            }
            return r;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            try
            {
                var accounts = get_accounts_lookup();

                var chars = FullWoWCharacterInfo.Load();
                chars.Sort(delegate (FullWoWCharacterInfo a, FullWoWCharacterInfo b) {
                    if (a.region != b.region)
                    {
                        a.region.CompareTo(b.region);
                    }
                    if (a.race != b.race)
                    {
                        a.race.CompareTo(b.race);
                    }

                    return -(a.gold.CompareTo(b.gold)); // Big gold values first
                });
                foreach (var account in chars)
                {
                    account.gold -= 50;
                }

                var gold_info = chars
                    .Select(delegate(FullWoWCharacterInfo @char)
                    {
                        CSVWoWAccount csv_account;
                        if (!accounts.TryGetValue(@char.bn_toon_id, out csv_account))
                        {
                            throw new Exception("Could not find any toon with the id \"" + @char.bn_toon_id + "\"");
                        }

                        return new WoWAccountGoldInfo(csv_account.ToWoWAccount(), @char);
                    })
                    .ToArray();

                using(var writer = new CsvWriter(Console.Out, CultureInfo.InvariantCulture))
                {
                    writer.Configuration.Delimiter = ",\t";
                    writer.WriteHeader<WoWAccountGoldInfo>();
                    writer.NextRecord();
                    writer.WriteRecords(gold_info);
                }
                
            } catch(Exception e)
            {
                Console.WriteLine("Could not generate report: " + e.Message + "\n" + e.StackTrace);
            }
        }
    }
}
