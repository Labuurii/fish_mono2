﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenGoldReport
{
    class WoWAccountGoldInfo
    {
        public string email { get; set; }
        public string char_name { get; set; }
        public string realm { get; set; }

        public string race { get; set; }
        public int gold { get; set; }

        public WoWAccountGoldInfo(WoWAccount account, FullWoWCharacterInfo character)
        {
            email = account.email;
            char_name = character.char_name;
            realm = character.realm;
            race = character.race;
            gold = character.gold;
        }
    }
}
