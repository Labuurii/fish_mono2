﻿using Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    public enum BotState
    {
        Running,
        Paused,
        Stopped
    }

    public interface ISlave
    {
        void SetProcessesToHighjack(List<Process> window_handles);
        void AddAccountToLaunch(WoWAccount account);
        Window GetTopLeftWindow();
        Window GetWindowToTheSideOf(Vector2I vector2I, Vector2I search_direction);
        WoWAccount GetAccountByWindow(Window selected_window);
        void RestartAccount(WoWAccount account);
        BotState TogglePause(Window selected_window);
        void Shutdown(Window selected_window);
        void RestartWindow(Window selected_window);
        BotState GetBotState(Window window);
        void ShutdownApp();
        void ShutdownLevelingAccounts();
        void TryRelaunchAccount(WoWAccount account);
        void ClickInMiddleOfWindow(Window selected_window);
        void ToggleFPS();
        void PrintEnumeratorStack(Window selected_window);
    }
}
