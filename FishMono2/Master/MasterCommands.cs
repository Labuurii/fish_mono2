﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Master
{
    public static class MasterCommands
    {
        public const Key ReLaunchNotRunning = Key.Q;
        public const Key ReLaunchLevelingAccounts = Key.W;

        public const Key FocusWindow = Key.A;
        public const Key TogglePaused = Key.S;
        public const Key ShutdownFocused = Key.D;
        public const Key RestartFocused = Key.F;
        public const Key PrintAccountInfo = Key.P;
        public const Key PrintEnumeratorStack = Key.O;
        public const Key ShutdownApp = Key.N;
        public const Key ShutdownLevelingAccounts = Key.M;
        public const Key ClickInMiddle = Key.C;
        public const Key HideSelectionWindow = Key.H;
        public const Key ToggleFPS = Key.G;
    }
}
