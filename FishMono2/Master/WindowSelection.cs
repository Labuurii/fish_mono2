﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Master
{
    public partial class WindowSelection : Form
    {
        public enum BorderColor
        {
            Running,
            Paused,
            Stopped
        }

        static readonly Color RUNNING_COLOR = Color.Green;
        static readonly Color STOPPED_COLOR = Color.Red;
        static readonly Color PAUSED_COLOR = Color.Yellow;

        static Color get_color(BorderColor color)
        {
            switch(color)
            {
                case BorderColor.Paused:
                    return PAUSED_COLOR;
                case BorderColor.Running:
                    return RUNNING_COLOR;
                case BorderColor.Stopped:
                    return STOPPED_COLOR;
                default:
                    throw new Exception("Unhandled enum value " + color);
            }
        }

        Color groupBoxColor = RUNNING_COLOR;
        public WindowSelection()
        {
            InitializeComponent();
            Hide();
        }

        public void SetArea(Rectangle rectangle)
        {
            Show();
            SetBounds(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                borderPen.Width = 3;
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void groupBox1_Paint(object sender, PaintEventArgs e)
        {
            DrawGroupBox(groupBox1, e.Graphics, groupBoxColor);
        }

        private void WindowSelection_Shown(object sender, EventArgs e)
        {

        }

        private void WindowSelection_Load(object sender, EventArgs e)
        {
            
        }

        public void SetColor(BorderColor color_id)
        {
            groupBoxColor = get_color(color_id);
            Refresh();
        }
    }
}
