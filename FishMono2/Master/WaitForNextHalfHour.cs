﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    public class WaitForNextHalfHour
    {
        public DateTime next;

        public WaitForNextHalfHour()
        {
            next = DateTime.Now.AddMinutes(-DateTime.Now.Minute);
            while(next <= DateTime.Now)
            {
                next = next.AddMinutes(30);
            }
            Reset();
        }

        public void Reset()
        {
            next = next.AddMinutes(30);
        }

        public bool HasEntered()
        {
            return DateTime.Now >= next;
        }
    }
}
