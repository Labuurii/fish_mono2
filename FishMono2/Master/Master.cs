﻿using CsvHelper;
using GlobalHotKey;
using Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Master
{
    [Flags]
    public enum MasterFlags
    {
        None = 0,
        Highjack = 1,
        LaunchTest = 2,
        DontLaunchNew = 3
    }

    public class Master
    {
        readonly MasterFlags flags;
        readonly ISlave slave;
        readonly WindowSelection window_sel;
        Window selected_window;
        readonly WaitForNextHalfHour restart_countdown = new WaitForNextHalfHour();
        CountDown delayed_launch_bots = null; // If the highjack flag is set the launch of new accounts have to wait, now it is one minute and then it launches new accounts

        static WindowSelection.BorderColor get_border_color(BotState state)
        {
            switch(state)
            {
                case BotState.Paused:
                    return WindowSelection.BorderColor.Paused;
                case BotState.Running:
                    return WindowSelection.BorderColor.Running;
                case BotState.Stopped:
                    return WindowSelection.BorderColor.Stopped;
                default:
                    throw new Exception("Unhandled enum value " + state);
            }
        }

        public Master(MasterFlags flags, ISlave slave)
        {
            this.flags = flags;
            this.slave = slave;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            window_sel = new WindowSelection();

            var hot_keys = new HotKeyManager();

            hot_keys.Register(Key.Up, ModifierKeys.None);
            hot_keys.Register(Key.Down, ModifierKeys.None);
            hot_keys.Register(Key.Right, ModifierKeys.None);
            hot_keys.Register(Key.Left, ModifierKeys.None);
            hot_keys.Register(MasterCommands.FocusWindow, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.TogglePaused, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ShutdownFocused, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.RestartFocused, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ReLaunchNotRunning, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ReLaunchLevelingAccounts, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.PrintAccountInfo, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.PrintEnumeratorStack, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ShutdownApp, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ShutdownLevelingAccounts, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ClickInMiddle, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.HideSelectionWindow, ModifierKeys.Alt);
            hot_keys.Register(MasterCommands.ToggleFPS, ModifierKeys.Alt);

            hot_keys.KeyPressed += on_key_pressed;
        }

        private void on_key_pressed(object sender, KeyPressedEventArgs e)
        {
            var key = e.HotKey.Key;
            switch (key)
            {
                case Key.Up:
                case Key.Down:
                case Key.Right:
                case Key.Left:
                    handle_move_selection(key);
                    break;
                case MasterCommands.FocusWindow:
                    handle_focus_window();
                    break;
                case MasterCommands.TogglePaused:
                    handle_toggle_paused();
                    break;
                case MasterCommands.ShutdownFocused:
                    handle_shutdown();
                    break;
                case MasterCommands.RestartFocused:
                    handle_restart();
                    break;
                case MasterCommands.ReLaunchNotRunning:
                    handle_rerun_launch_process();
                    break;
                case MasterCommands.ReLaunchLevelingAccounts:
                    handle_relaunch_leveling_accounts();
                    break;
                case MasterCommands.PrintAccountInfo:
                    handle_print_account_info();
                    break;
                case MasterCommands.PrintEnumeratorStack:
                    handle_print_enumerator_stack();
                    break;
                case MasterCommands.ShutdownApp:
                    handle_shutdown_app();
                    break;
                case MasterCommands.ShutdownLevelingAccounts:
                    handle_shutdown_leveling_accounts();
                    break;
                case MasterCommands.ClickInMiddle:
                    handle_click_in_middle();
                    break;
                case MasterCommands.HideSelectionWindow:
                    handle_hide_selection_window();
                    break;
                case MasterCommands.ToggleFPS:
                    handle_toggle_fps();
                    break;
                default:
                    throw new Exception("Unhandled hotkey " + key);
            }
        }

        private void handle_print_enumerator_stack()
        {
            if(selected_window != null && selected_window.Exists())
            {
                slave.PrintEnumeratorStack(selected_window);
            }
        }

        private void handle_relaunch_leveling_accounts()
        {
            relaunch_leveling_accounts();
        }

        private void handle_toggle_fps()
        {
            slave.ToggleFPS();
        }

        private void handle_hide_selection_window()
        {
            window_sel.Hide();
            selected_window = null;
        }

        private void handle_click_in_middle()
        {
            if(selected_window != null && selected_window.Exists())
            {
                slave.ClickInMiddleOfWindow(selected_window);
            }
        }

        private void handle_shutdown_leveling_accounts()
        {
            slave.ShutdownLevelingAccounts();
        }

        private void handle_shutdown_app()
        {
            slave.ShutdownApp();
        }

        private void handle_print_account_info()
        {
            if(selected_window != null && selected_window.Exists())
            {
                var account = slave.GetAccountByWindow(selected_window);
                if(account != null)
                {
                    Console.WriteLine(account.PrettyString());
                }
            }
        }

        private void handle_rerun_launch_process()
        {
            var accounts = GetAccounts();
            foreach(var account in accounts)
            {
                slave.TryRelaunchAccount(account);
            }
        }

        private void handle_restart()
        {
            if(selected_window != null && selected_window.Exists())
            {
                slave.RestartWindow(selected_window);
            }
        }

        private void handle_shutdown()
        {
            if(selected_window != null && selected_window.Exists())
            {
                slave.Shutdown(selected_window);
            }
        }

        private void handle_toggle_paused()
        {
            if(selected_window != null && selected_window.Exists())
            {
                var bot_state = slave.TogglePause(selected_window);
                var border_color = get_border_color(bot_state);
                window_sel.SetColor(border_color);
            }
        }

        private void handle_focus_window()
        {
            if(selected_window != null && selected_window.Exists())
            {
                selected_window.Focus();
            }
        }

        private void handle_move_selection(Key key)
        {
            if (selected_window == null || !selected_window.Exists())
            {
                var window = slave.GetTopLeftWindow();
                if (window != null)
                {
                    set_selected_window(window);
                }
            }
            else
            {
                var search_direction = new Vector2I();
                switch (key)
                {
                    case Key.Up:
                        search_direction.y = -1;
                        break;
                    case Key.Down:
                        search_direction.y = 1;
                        break;
                    case Key.Right:
                        search_direction.x = 1;
                        break;
                    case Key.Left:
                        search_direction.x = -1;
                        break;
                    default:
                        throw new Exception("Unhandled hotkey " + key);
                }

                var window = slave.GetWindowToTheSideOf(selected_window.GetArea().TopLeft(), search_direction);
                if (window != null)
                {
                    set_selected_window(window);
                }
            }
        }

        public void PreLoop()
        {
            if((flags & MasterFlags.Highjack) == MasterFlags.Highjack)
            {
                var processes = WoWProcessOps.GetWoWProcesses();
                slave.SetProcessesToHighjack(processes);
            } else if((flags & MasterFlags.LaunchTest) == MasterFlags.LaunchTest)
            {
                var account = new WoWAccount("foobarzebra@test.com");
                slave.AddAccountToLaunch(account);
            } 
            
            if((flags & MasterFlags.DontLaunchNew) != MasterFlags.DontLaunchNew && (flags & MasterFlags.LaunchTest) != MasterFlags.LaunchTest) {
                if((flags & MasterFlags.Highjack) == MasterFlags.Highjack)
                {
                    delayed_launch_bots = new CountDown(60);
                } else
                {
                    relaunch_farming_accounts();
                }
            }
        }

        public void Update()
        {
            Application.DoEvents();

            if(restart_countdown.HasEntered())
            {
                restart_countdown.Reset();
                relaunch_farming_accounts();
            }

            if(delayed_launch_bots != null)
            {
                if(delayed_launch_bots.HasCountedDown())
                {
                    delayed_launch_bots = null;
                    relaunch_farming_accounts();
                }
            }
        }

        private void relaunch_farming_accounts()
        {
            var accounts = GetAccounts();
            foreach (var account in accounts)
            {
                if (account.IsFishingReady())
                {
                    slave.TryRelaunchAccount(account);
                }
            }
        }

        private void relaunch_leveling_accounts()
        {
            var accounts = GetAccounts();
            foreach (var account in accounts)
            {
                if (!account.IsFishingReady())
                {
                    slave.TryRelaunchAccount(account);
                }
            }
        }

        private void set_selected_window(Window window)
        {
            this.selected_window = window;
            var area = window.GetArea();
            window_sel.SetColor(get_border_color(slave.GetBotState(window)));
            window_sel.SetArea(new System.Drawing.Rectangle
            {
                X = area.Left,
                Y = area.Top,
                Width = area.Right - area.Left,
                Height = area.Bottom - area.Top
            });
        }

        public List<WoWAccount> GetAccounts()
        {
            return CSVWoWAccount.ReadAccounts().Select(a => a.ToWoWAccount()).ToList();
        }
    }
}
