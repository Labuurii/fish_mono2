﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master
{
    public class FishAHListing
    {
        string _server;
        bool _is_horde;
        Dictionary<string, List<int>> _silver_prizes = new Dictionary<string, List<int>>();
        DateTime _time = DateTime.Now;

        public Dictionary<string, List<int>> silver_prizes { get => _silver_prizes; set => _silver_prizes = value; }
        public string server { get => _server; set => _server = value; }
        public DateTime time { get => _time; set => _time = value; }
        public bool is_horde { get => _is_horde; set => _is_horde = value; }

        public void AddRange(string fish_name, List<int> silver_prizes)
        {
            List<int> current_silver_prizes;
            if (!_silver_prizes.TryGetValue(fish_name, out current_silver_prizes))
            {
                current_silver_prizes = new List<int>();
                _silver_prizes[fish_name] = current_silver_prizes;
            }

            current_silver_prizes.AddRange(silver_prizes);
        }
    }
}
