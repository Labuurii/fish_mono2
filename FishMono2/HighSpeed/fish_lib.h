#pragma once

#include "HighSpeed.h"
#include "library.h"

extern "C" {
	enum class GlobalImage 
	{
		Screen,
		Changes
	};

	HIGHSPEED_API void window_minimize_size(void* window);

	HIGHSPEED_API void* create_screen_resource();
	HIGHSPEED_API void destroy_screen_resource(void* screen_resource);

	HIGHSPEED_API void screen_resource_get_size(void* screen_resource, Vec2I* sink);
	
	HIGHSPEED_API void screen_resource_blacken_part(void* screen_resource, Rect rect);
	HIGHSPEED_API void screen_resource_save_image_part(void* screen_resource, const char* path, Rect screen_part, GlobalImage image);
	HIGHSPEED_API void screen_resource_show_image_part(void* screen_resource, const char* name, Rect screen_part, GlobalImage image);
	
	HIGHSPEED_API void have_fun42(void* window);
	HIGHSPEED_API bool screen_resource_update(void* screen_resource);
	
	// Returns the amount of NMS boxes
	HIGHSPEED_API int screen_resource_compute_fishing_bobbers(void* screen_resource, Rect screen_part, Rect* nms_boxes_result, int max_nms_boxes);
	HIGHSPEED_API int screen_resource_count_white_pixels(void* screen_resource, Rect screen_part);

	// Returns the string length
	HIGHSPEED_API int screen_resource_read_text(void* screen_resource, Rect screen_part, char* sink, int sink_max_length);
	HIGHSPEED_API void screen_resource_read_text_array(void* screen_resource, int screen_part_count, Rect* screen_parts, int* text_lengths, char* sinks, int sink_element_length);
}