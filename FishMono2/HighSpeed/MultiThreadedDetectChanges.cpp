#include "MultiThreadedDetectChanges.h"

MultiThreadedDetectChangesWorker::MultiThreadedDetectChangesWorker() {
	std::thread t([this]() { run(); });
	t.detach();
}

void MultiThreadedDetectChangesWorker::run() {
	for (;;) {

		BGR* screen_data = nullptr;
		BGR* previous_screen_data = nullptr;
		BGR* changes_data = nullptr;

		Vec2I size;
		Rect work_load_rect;

		{
			std::unique_lock<std::mutex> l(mutex);
			cv.wait(l);
			
			auto j = this->job;
			screen_data = j.screen_data;
			previous_screen_data = j.previous_screen_data;
			changes_data = j.changes_data;
			size = j.size;
			work_load_rect = j.work_load_rect;

			if (screen_data == nullptr) {
				continue;
			}

			this->job = {};
		}

		{
			for (int y = work_load_rect.top; y < work_load_rect.bottom; ++y) {
				auto row_offset = y * size.x;
				for (int x = work_load_rect.left; x < work_load_rect.right; ++x) {
					auto screen_pixel = screen_data + row_offset + x;
					auto previous_screen_pixel = previous_screen_data + row_offset + x;
					auto changes_pixel = changes_data + row_offset + x;
					if (norm_avg_delta(*previous_screen_pixel , *screen_pixel) > 0.05f) {
						*changes_pixel = BGR(255, 255, 255);
					}
				}
			}

			is_done.store(true);
		}
	}
}

void MultiThreadedDetectChangesWorker::set_job(DetectChangesJob job) {
	{
		assert(is_done.load() && "set_job called while already doing work");
		std::lock_guard<std::mutex> l(mutex);
		this->job = job;
	}
	is_done.store(false);
	cv.notify_all();
}

void MultiThreadedDetectChangesWorker::wait_on_job_to_complete() {
	while(is_done.load() == false) {}
}

void MultiThreadedDetectChangesMaster::detect_changes(Image& screen_image, Image& previous_screen_image, Image& changes_image) {

	auto size = screen_image.get_size();
	assert(size == previous_screen_image.get_size());
	assert(size == changes_image.get_size());
	assert(size.y % workers.size() == 0 && "Worker count is not a factor of screen size");

	DetectChangesJob job;
	job.screen_data = screen_image.get_data();
	job.previous_screen_data = previous_screen_image.get_data();
	job.changes_data = changes_image.get_data();
	job.size = size;
	
	auto y_step = size.y / workers.size();
	Rect work_load_rect;
	work_load_rect.right = size.x;
	work_load_rect.bottom = y_step;

	for (auto& worker : workers) {
		job.work_load_rect = work_load_rect;
		worker.set_job(job);

		work_load_rect.top += y_step;
		work_load_rect.bottom += y_step;
	}

	for (auto& worker : workers) {
		worker.wait_on_job_to_complete();
	}
}