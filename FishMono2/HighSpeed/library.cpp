#include "library.h"

#include <iostream>
#include <cassert>

#define D3D_RELEASE(__p) {if(__p!=nullptr){__p->Release();__p=nullptr;}}
#define MAX_TITLE_LENGTH 1024

float clamp(float v, float min, float max) {
	if (v < min)
		v = min;
	if (v > max)
		v = max;
	return v;
}

float clamp01(float v) {
	return clamp(v, 0.0f, 1.0f);
}

Vec2I::Vec2I(int x, int y) : x(x), y(y) {}

bool Vec2I::operator==(Vec2I other) const noexcept {
	return x == other.x && y == other.y;
}

cv::Rect Rect::to_cv() const {
	cv::Rect r;
	r.x = left;
	r.y = top;
	r.width = right - left;
	r.height = bottom - top;
	return r;
}

Rect Rect::from_cv(cv::Rect s) {
	Rect r;
	r.left = s.x;
	r.top = s.y;
	r.right = s.x + s.width;
	r.bottom = s.y + s.height;
	return r;
}

Rect Rect::bound_rectangle(Rect sub) const {
	Rect r;
	r.top = clamp(sub.top, top, bottom);
	r.bottom = clamp(sub.bottom, top, bottom);
	r.left = clamp(sub.left, left, right);
	r.right = clamp(sub.right, left, right);

	if (r.bottom < r.top) {
		r.bottom = r.top;
	}
	if (r.right < r.left) {
		r.right = r.left;
	}

	return r;
}

bool Rect::has_size() const {
	return bottom > top && right > left;
}

Vec2I Rect::get_size() const {
	return Vec2I(
		right - left,
		bottom - top
	);
}

BGR::BGR(unsigned char b, unsigned char g, unsigned char r)
	: b(b), g(g), r(r) {}
	
bool BGR::operator==(BGR other) const {
	return b == other.b && g == other.g && b == other.b;
}

bool BGR::operator!=(BGR other) const {
	return !(*this == other);
}

Image::Image(const Image& other) {
	*this = other;
}

void Image::operator=(const Image& other) {
	if (other.data) {
		resize(other.size);
		memcpy(data.get(), other.data.get(), size.x * size.y * sizeof(BGR));
	}
	else {
		data = nullptr;
		size = other.size;
	}
}

void Image::set(Image& source, Rect rect) noexcept {
	resize(rect.get_size());
	auto source_size = source.get_size();
	auto destination_size = rect.get_size();
	auto source_data = source.get_data();
	auto destination_data = this->data.get();
	
	for(int y = rect.top; y < rect.bottom; ++y) {
		for(int x = rect.left; x < rect.right; ++x) {
			auto destination_x = x - rect.left;
			auto destination_y = y - rect.top;
			
			auto source_pixel = source_data + (y * source_size.x) + x;
			auto destination_pixel = destination_data + (destination_y * destination_size.x) + destination_x;
			
			*destination_pixel = *source_pixel;
		}
	}
}

void Image::operator=(cv::Mat& other) {
	if (!other.data) {
		data = nullptr;
		size = Vec2I();
		return;
	}

	assert(other.type() == CV_8UC3);
	resize(Vec2I(other.size().width, other.size().height));
	memcpy(data.get(), other.data, size.x * size.y * sizeof(BGR));
}

BGR* Image::get_data() const noexcept {
	return data.get();
}

Vec2I Image::get_size() const noexcept {
	return size;
}

void Image::resize(Vec2I size) noexcept {
	if(this->size == size) {
		return;
	}
	this->size = size;
	auto elements_required = size.y * size.x;
	data = std::make_unique<BGR[]>(elements_required);
}

cv::Mat Image::as_cv_mat() const noexcept {
	cv::Mat r(size.y, size.x, CV_8UC3, data.get());
	return r;
}

ScreenCapturer::ScreenCapturer() noexcept {
	auto adapter = D3DADAPTER_DEFAULT;
	HRESULT hr = S_OK;
	
	// init D3D and get screen size
	d3d = Direct3DCreate9(D3D_SDK_VERSION);
	hr = d3d->GetAdapterDisplayMode(adapter, &mode);
	if(FAILED(hr)) return;

	parameters.Windowed = TRUE;
	parameters.BackBufferCount = 1;
	parameters.BackBufferHeight = mode.Height;
	parameters.BackBufferWidth = mode.Width;
	parameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	parameters.hDeviceWindow = NULL;

	// create device & capture surface
	hr = d3d->CreateDevice(adapter, D3DDEVTYPE_HAL, NULL, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &parameters, &device);
	if(FAILED(hr)) return;
	hr = device->CreateOffscreenPlainSurface(mode.Width, mode.Height, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &surface, nullptr);
	if(FAILED(hr)) return;

	// compute the required buffer size
	hr = surface->LockRect(&rc, NULL, 0);
	if(FAILED(hr)) return;
	pitch = rc.Pitch;
	hr = surface->UnlockRect();
	if(FAILED(hr)) return;
}

ScreenCapturer::~ScreenCapturer() noexcept {
	D3D_RELEASE(surface);
	D3D_RELEASE(device);
	D3D_RELEASE(d3d);
}

Vec2I ScreenCapturer::get_size() const noexcept {
	return Vec2I((int)mode.Width, (int)mode.Height);
}

bool ScreenCapturer::is_valid() const noexcept {
	return pitch != 0;
}

bool ScreenCapturer::take_screenshot(Image* sink, Rect part) noexcept {
	HRESULT hr = S_OK;

	// get the data
	hr = device->GetFrontBufferData(0, surface);
	if(FAILED(hr)) return false;
	
	Vec2I size = part.get_size();
	sink->resize(size);

	Vec2I source_size;
	source_size.x = mode.Width;
	source_size.y = mode.Height;

	// copy it into our buffers
	hr = surface->LockRect(&rc, nullptr, D3DLOCK_READONLY);
	if(FAILED(hr)) return false;

	auto source_data = (unsigned char*)rc.pBits;
	auto destination_data = sink->get_data();
	for (int y = 0; y < size.y; ++y) {
		for (int x = 0; x < size.x; ++x) {
			auto source_x = part.left + x;
			auto source_y = part.top + y;

			auto source_pixel = source_data + ( source_y * ( source_size.x * 4 ) + source_x * 4);
			auto destination_pixel = destination_data + (y * size.x) + x;
			destination_pixel->b = source_pixel[0];
			destination_pixel->g = source_pixel[1];
			destination_pixel->r = source_pixel[2];
		}
	}
	
	hr = surface->UnlockRect();
	if(FAILED(hr)) return false;
	return true;
}

struct NormalButtonDesc {
	int vk;
	const char* name;
};

static const NormalButtonDesc NORMAL_VIRTUAL_KEYS[] = {
	{ VK_LBUTTON, "Left Mouse" },
	{ VK_RBUTTON, "Right Mouse" },
	{ VK_CANCEL, "Cancel" },
	{ VK_XBUTTON1, "XButton 1" },
	{ VK_XBUTTON2, "XButton 2" },
	{ VK_BACK, "Back" },
	{ VK_TAB, "Tab" },
	{ VK_CLEAR, "Clear" },
	{ VK_RETURN, "Return" },
	{ VK_PAUSE, "Pause" },
	{ VK_CAPITAL, "Capital" },
	{ VK_KANA, "Kana" },
	{ VK_HANGUL, "Hangul" },
	{ VK_JUNJA, "Junja" },
	{ VK_FINAL, "Final" },
	{ VK_HANJA, "Hanja" },
	{ VK_KANJI, "Kanji" },
	{ VK_ESCAPE, "Escape" },
	{ VK_CONVERT, "Convert" },
	{ VK_NONCONVERT, "Non Convert" },
	{ VK_ACCEPT, "Accept" },
	{ VK_MODECHANGE, "Mode Change" },
	{ VK_SPACE, "Space" },
	{ VK_PRIOR, "Prior" },
	{ VK_NEXT, "Next" },
	{ VK_END, "End" },
	{ VK_HOME, "Home" },
	{ VK_LEFT, "Left" },
	{ VK_UP, "Up" },
	{ VK_RIGHT, "Right" },
	{ VK_DOWN, "Down" },
	{ VK_SELECT, "Select" },
	{ VK_PRINT, "Print" },
	{ VK_EXECUTE, "Execute" },
	{ VK_SNAPSHOT, "Snapshot" },
	{ VK_INSERT, "Insert" },
	{ VK_DELETE, "Delete" },
	{ VK_HELP, "Help" },
	{ '0', "0" },
	{ '1', "1" },
	{ '2', "2" },
	{ '3', "3" },
	{ '4', "4" },
	{ '5', "5" },
	{ '6', "6" },
	{ '7', "7" },
	{ '8', "8" },
	{ '9', "9" },
	{ 'A', "A" },
	{ 'B', "B" },
	{ 'C', "C" },
	{ 'D', "D" },
	{ 'E', "E" },
	{ 'F', "F" },
	{ 'G', "G" },
	{ 'H', "H" },
	{ 'I', "I" },
	{ 'J', "J" },
	{ 'K', "K" },
	{ 'L', "L" },
	{ 'M', "M" },
	{ 'N', "N" },
	{ 'O', "O" },
	{ 'P', "P" },
	{ 'Q', "Q" },
	{ 'R', "R" },
	{ 'S', "S" },
	{ 'T', "T" },
	{ 'U', "U" },
	{ 'V', "V" },
	{ 'W', "W" },
	{ 'X', "X" },
	{ 'Y', "Y" },
	{ 'Z', "Z" },
	{ VK_LWIN, "LWIN" },
	{ VK_RWIN, "RWIN" },
	{ VK_APPS, "APPS" },
	{ VK_SLEEP, "SLEEP" },
	{ VK_NUMPAD0, "NUMPAD0" },
	{ VK_NUMPAD1, "NUMPAD1" },
	{ VK_NUMPAD2, "NUMPAD2" },
	{ VK_NUMPAD3, "NUMPAD3" },
	{ VK_NUMPAD4, "NUMPAD4" },
	{ VK_NUMPAD5, "NUMPAD5" },
	{ VK_NUMPAD6, "NUMPAD6" },
	{ VK_NUMPAD7, "NUMPAD7" },
	{ VK_NUMPAD8, "NUMPAD8" },
	{ VK_NUMPAD9, "NUMPAD9" },
	{ VK_MULTIPLY, "Multiply" },
	{ VK_ADD, "Add" },
	{ VK_SEPARATOR, "Separator" },
	{ VK_SUBTRACT, "Subtract" },
	{ VK_DECIMAL, "Decimal" },
	{ VK_DIVIDE, "Divide" },
	{ VK_F1, "F1" },
	{ VK_F2, "F2" },
	{ VK_F3, "F3" },
	{ VK_F4, "F4" },
	{ VK_F5, "F5" },
	{ VK_F6, "F6" },
	{ VK_F7, "F7" },
	{ VK_F8, "F8" },
	{ VK_F9, "F9" },
	{ VK_F10, "F10" },
	{ VK_F11, "F11" },
	{ VK_F12, "F12" },
	{ VK_F13, "F13" },
	{ VK_F14, "F14" },
	{ VK_F15, "F15" },
	{ VK_F16, "F16" },
	{ VK_F17, "F17" },
	{ VK_F18, "F18" },
	{ VK_F19, "F19" },
	{ VK_F20, "F20" },
	{ VK_F21, "F21" },
	{ VK_F22, "F22" },
	{ VK_F23, "F23" },
	{ VK_F24, "F24" },
	{ VK_NUMLOCK, "Num Lock" },
	{ VK_SCROLL, "Scroll" },
	{ VK_BROWSER_BACK, "Browser Back" },
	{ VK_BROWSER_FORWARD, "Browser Forward" },
	{ VK_BROWSER_REFRESH, "Browser Refresh" },
	{ VK_BROWSER_STOP, "Browser Stop" },
	{ VK_BROWSER_SEARCH, "Browser Search" },
	{ VK_BROWSER_FAVORITES, "Browser Favorites" },
	{ VK_BROWSER_HOME, "Browser Home" },
	{ VK_VOLUME_MUTE, "Volume Mute" },
	{ VK_VOLUME_DOWN, "Volume Down" },
	{ VK_VOLUME_UP, "Volume Up" },
	{ VK_MEDIA_NEXT_TRACK, "Media Next Track" },
	{ VK_MEDIA_PREV_TRACK, "Media Prev Track" },
	{ VK_MEDIA_STOP, "Media Stop" },
	{ VK_MEDIA_PLAY_PAUSE, "Media Pause" },
	{ VK_LAUNCH_MAIL, "Launch Mail" },
	{ VK_LAUNCH_MEDIA_SELECT, "Launch Media Select" },
	{ VK_LAUNCH_APP1, "Launch App 1" },
	{ VK_LAUNCH_APP2, "Launch App 2" },
	{ VK_OEM_1, "OEM 1" },
	{ VK_OEM_PLUS, "OEM Plus" },
	{ VK_OEM_COMMA, "OEM Comma" },
	{ VK_OEM_MINUS, "OEM Minus" },
	{ VK_OEM_PERIOD, "OEM Period" },
	{ VK_OEM_2, "OEM 2" },
	{ VK_OEM_3, "OEM 3" },
	{ VK_OEM_4, "OEM 4" },
	{ VK_OEM_5, "OEM 5" },
	{ VK_OEM_6, "OEM 6" },
	{ VK_OEM_7, "OEM 7" },
	{ VK_OEM_8, "OEM 8" },
	{ VK_OEM_102, "OEM 102" },
	{ VK_PROCESSKEY, "Process" },
	{ VK_PACKET, "Packet" },
	{ VK_ATTN, "ATTN" },
	{ VK_CRSEL, "CRSEL" },
	{ VK_EXSEL, "EXSEL" },
	{ VK_EREOF, "EROEF" },
	{ VK_PLAY, "Play" },
	{ VK_ZOOM, "Zoom" },
	{ VK_NONAME, "No Name" },
	{ VK_PA1, "PA1" },
	{ VK_OEM_CLEAR, "OEM Clear" },
	0
};

struct VirtualKeyMapping {
	KeyboardModifiers mod;
	int vk;
	const char* name;
};

static const VirtualKeyMapping MODIFIER_VIRTUAL_KEYS[] = {
	{KeyboardModifiers::LeftShift, VK_SHIFT, "Shift"},
	{KeyboardModifiers::LeftCtrl, VK_CONTROL, "Ctrl"},
	{KeyboardModifiers::LeftAlt, VK_MENU, "Alt"},
	{KeyboardModifiers::None, 0, nullptr}
};

bool get_all_pressed_buttons(std::vector<int>* normal_buttons_sink, KeyboardModifiers* keyboard_modifiers_sink) {
	BYTE keyboard[256];
	if(!GetKeyboardState(keyboard)) {
		return false;
	}
	
	for(int i = 0; NORMAL_VIRTUAL_KEYS[i].vk != 0; ++i) {
		auto vk = NORMAL_VIRTUAL_KEYS[i].vk;
		auto vk_state = keyboard[vk];
		bool high_bit = vk_state >> ( sizeof(vk_state) * 8 - 1 );
		if(high_bit) {
			normal_buttons_sink->push_back(vk);
		}
	}
	
	for(int i = 0; MODIFIER_VIRTUAL_KEYS[i].vk != 0; ++i) {
		auto mod = MODIFIER_VIRTUAL_KEYS[i].mod;
		auto vk = MODIFIER_VIRTUAL_KEYS[i].vk;
		auto vk_state = keyboard[vk];
		bool high_bit = vk_state >> ( sizeof(vk_state) * 8 - 1 );
		if(high_bit) {
			int sink = (int) ( *keyboard_modifiers_sink );
			sink |= (int)mod;
			*keyboard_modifiers_sink = (KeyboardModifiers)sink;
		}
	}
	
	return true;
}

bool get_normal_button_name(int search_vk, const char** sink) {
	for(int i = 0; NORMAL_VIRTUAL_KEYS[i].vk != 0; ++i) {
		auto vk = NORMAL_VIRTUAL_KEYS[i].vk;
		auto name = NORMAL_VIRTUAL_KEYS[i].name;
		if(vk == search_vk) {
			*sink = name;
			return true;
		}
	}
	return false;
}

bool get_keyboard_modifier_names(KeyboardModifiers mods, std::vector<const char*>* sink) {
	bool result = false;
	for(int i = 0; MODIFIER_VIRTUAL_KEYS[i].vk != 0; ++i) {
		auto mod = MODIFIER_VIRTUAL_KEYS[i].mod;
		auto name = MODIFIER_VIRTUAL_KEYS[i].name;
		
		if((int)mods & (int)mod) {
			sink->push_back(name);
			result = true;
		}
	}
	return true;
}

void get_complete_keybinding_string(KeybindingConfig keybinding, std::string* sink) {
	const char* key_name;
	thread_local std::vector<const char*> modifier_names;
	modifier_names.clear();
	
	if(keybinding.key == -1) {
		return;
	}
	
	if(!get_normal_button_name(keybinding.key, &key_name)) {
		key_name = "<UNKNOWN>";
	}
	*sink += key_name;
	
	get_keyboard_modifier_names(keybinding.modifiers, &modifier_names);
	for(auto mod_name : modifier_names) {
		*sink += '+';
		*sink += mod_name;
	}
}

WindowId::WindowId(HWND hwnd) : hwnd(hwnd) {}

bool WindowId::is_valid() const {
	return hwnd != NULL;
}

struct GetAllWindowsState {
	std::vector<WindowId>* sink;
};

BOOL CALLBACK EnumWindowsProc(
  _In_ HWND   hwnd,
  _In_ LPARAM lParam
)
{
	auto state = (GetAllWindowsState*)lParam;
	state->sink->push_back(WindowId{ hwnd });
	return TRUE;
}

void get_all_windows(std::vector<WindowId>* sink) noexcept {
	GetAllWindowsState state;
	state.sink = sink;
	(void)EnumWindows(EnumWindowsProc, (LPARAM)&state); // Discard error
}

bool get_window_name(WindowId id, std::string* sink) noexcept {
	char name_buffer[MAX_TITLE_LENGTH];
	auto r = GetWindowTextA((HWND)id.hwnd, name_buffer, MAX_TITLE_LENGTH);
	if(r == 0)
		return false;
	
	*sink = name_buffer; 
	return true;
}

bool get_window_class(WindowId id, std::string* sink) noexcept {
	char name_buffer[MAX_TITLE_LENGTH];
	auto r = GetClassNameA((HWND)id.hwnd, name_buffer, MAX_TITLE_LENGTH);
	if(r == 0)
		return false;
	*sink = name_buffer;
	return true;
}

bool get_single_window(std::string title, std::string class_, WindowId* sink) noexcept {
	std::vector<WindowId> all_windows;
	get_all_windows(&all_windows);
	std::string reused_string;
	for(auto w : all_windows) {
		if(!title.empty() && !get_window_name(w, &reused_string) || reused_string != title)
			continue;
		if(!class_.empty() && !get_window_class(w, &reused_string) || reused_string != class_)
			continue;
		
		*sink = w;
		return true;
	}
	
	return false;
}

bool get_window_rect(WindowId id, Rect* sink) noexcept {
	RECT rect;
	auto r = GetWindowRect((HWND)id.hwnd, &rect);
	if(!r)
		return false;
	
	sink->left = rect.left;
	sink->top = rect.top;
	sink->right = rect.right;
	sink->bottom = rect.bottom;
	return true;
}

WindowId get_focused_window() noexcept {
	auto hwnd = GetForegroundWindow();
	return WindowId(hwnd);
}

bool is_window_focused(WindowId id) noexcept {
	return GetForegroundWindow() == (HWND)id.hwnd;
}

bool is_window_alive(WindowId id) noexcept {
	return IsWindow((HWND)id.hwnd);
}

bool get_mouse_pos(Vec2I* sink) noexcept {
	POINT p;
	auto r = GetCursorPos(&p);
	if(!r)
		return false;
	
	sink->x = p.x;
	sink->y = p.y;
	return true;
}

bool move_mouse_to(Vec2I abs_p) noexcept {
	return SetCursorPos(abs_p.x, abs_p.y);
}

bool mouse_event(WindowId w, int wm, Vec2I pos) noexcept {
	return PostMessage(w.hwnd, wm, 0, MAKELPARAM(pos.x, pos.y));
}

bool press_right_mouse_button(WindowId w, Vec2I mouse_pos) noexcept {
	return mouse_event(w, WM_RBUTTONDOWN, mouse_pos);
}

bool release_right_mouse_button(WindowId w, Vec2I pos) noexcept {
	return mouse_event(w, WM_RBUTTONUP, pos);
}

bool press_left_mouse_button(WindowId w, Vec2I mouse_pos) noexcept {
	return mouse_event(w, WM_LBUTTONDOWN, mouse_pos);
}

bool release_left_mouse_button(WindowId w, Vec2I pos) noexcept {
	return mouse_event(w, WM_LBUTTONUP, pos);
}

bool press_button(WindowId window, int key) noexcept {
	if(!PostMessageA(window.hwnd, WM_KEYDOWN, key, 0)) {
		return false;
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	if(!PostMessageA(window.hwnd, WM_KEYUP, key, 0)) {
		return false;
	}
	
	return true;
	
}

bool write_char(WindowId window, char ch) noexcept {
	if(!PostMessageA(window.hwnd, WM_CHAR, ch, 0)) {
		return false;
	}
	
	return true;
}

bool write_string(WindowId window, const std::string& str) noexcept {
	for(auto ch : str) {
		if(!write_char(window, ch)) {
			return false;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return true;
}

std::vector<WindowId> get_wow_windows() {
	std::vector<WindowId> r;
	std::vector<WindowId> all_windows;
	
	get_all_windows(&all_windows);
	
	std::string window_name, window_class;
	for(auto window : all_windows) {
		window_name.clear();
		window_class.clear();
		if(get_window_name(window, &window_name) && get_window_class(window, &window_class)) {
			if(window_name == "World of Warcraft" && window_class == "GxWindowClass") {
				Rect rect;
				get_window_rect(window, &rect);
				r.push_back(window);
			}
		}
	}
	
	return r;
}