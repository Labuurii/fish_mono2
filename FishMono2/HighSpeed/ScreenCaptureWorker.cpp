#include "ScreenCaptureWorker.h"

void ScreenCaptureWorker::run_worker()
{
	Image thread_image;
	Vec2I screen_size;

	{
		std::lock_guard<std::mutex> l(mutex);
		screen_size = this->screen_size;
	}

	for (;;) {
		if (!should_run.load()) {
			break;
		}

		Rect rect;
		rect.right = screen_size.x;
		rect.bottom = screen_size.y;

		if (capturer->take_screenshot(&thread_image, rect)) {
			std::lock_guard<std::mutex> l(mutex);
			current_image = thread_image;
		}
	}
}

ScreenCaptureWorker::ScreenCaptureWorker(ScreenCapturer* capturer, Vec2I screen_size)
	: capturer(capturer), screen_size(screen_size)
{
	current_image.resize(screen_size);
	this->thread = std::thread([this]() { run_worker(); });
}

ScreenCaptureWorker::~ScreenCaptureWorker()
{
	should_run.store(false);
	thread.join();
}

void ScreenCaptureWorker::get_image(Image* sink)
{
	std::lock_guard<std::mutex> l(mutex);
	*sink = current_image;
}
