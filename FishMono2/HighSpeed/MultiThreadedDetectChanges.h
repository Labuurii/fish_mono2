#pragma once

#include <mutex>
#include <atomic>
#include <condition_variable>
#include <array>
#include "library.h"

class MultiThreadedDetectChangesMaster;

struct DetectChangesJob {
	BGR* screen_data = nullptr;
	BGR* previous_screen_data = nullptr;
	BGR* changes_data = nullptr;

	Vec2I size;
	Rect work_load_rect;
};

class MultiThreadedDetectChangesWorker {
	std::mutex mutex;
	std::condition_variable cv;
	DetectChangesJob job;

	void run();

public:
	std::atomic<bool> is_done = true;

	MultiThreadedDetectChangesWorker();

	void set_job(DetectChangesJob job);
	void wait_on_job_to_complete();
};

class MultiThreadedDetectChangesMaster {
	std::array<MultiThreadedDetectChangesWorker, 10> workers;
public:
	
	void detect_changes(Image& screen_image, Image& previous_screen_image, Image& changes_image);

};