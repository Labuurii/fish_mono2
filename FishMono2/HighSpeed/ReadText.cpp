#include "ReadText.h"

#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <opencv2/highgui.hpp>
#include <Windows.h>

void ReadTextManager::wait_for_work(ReadTextJob* sink) {
	for (;;) {
		std::unique_lock<std::mutex> l(mutex);
		if (!jobs.empty()) {
			auto job_index = jobs.size() - 1;
			*sink = jobs[job_index];
			jobs.erase(jobs.begin() + job_index);
			break;
		}
		else {
			cv.wait(l);
		}
	}
	

}
void ReadTextManager::signal_completed_job() {
	completed_jobs_counter.fetch_add(1);
}

void ReadTextManager::add_work_and_wait(const std::vector<ReadTextJob>& jobs) {
	auto count = jobs.size();
	{
		std::lock_guard<std::mutex> l(mutex);
		this->jobs = jobs;
		this->completed_jobs_counter.store(0);
		cv.notify_all();
	}

	while (this->completed_jobs_counter.load() != count) {
		std::this_thread::yield();
	}
}

ReadTextWorker::ReadTextWorker(DWORD_PTR logical_core_index, ReadTextManager* manager)
{
	this->manager = manager;
	std::thread t([this]() { run(); });
	DWORD_PTR mask = ( (DWORD_PTR)1 ) << logical_core_index;
	SetThreadAffinityMask(t.native_handle(), mask);

	t.detach();
}

void ReadTextWorker::run() {
	ReadTextJob job;
	Image ocr_image;

	tesseract::TessBaseAPI ocr;
	{
		auto init_r = ocr.Init(NULL, "eng", tesseract::OEM_LSTM_ONLY);
		if (init_r < 0) {
			return;
		}
		ocr.SetPageSegMode(tesseract::PSM_AUTO);
		ocr.SetVariable("tessedit_char_whitelist", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.=");
	}

	for (;;) {
		manager->wait_for_work(&job);

		ocr_image.set(*job.screen_image, job.screen_part);

		{
			auto data = ocr_image.get_data();
			auto size = ocr_image.get_size();

			for (int y = 0; y < size.y; ++y) {
				auto row_offset = y * size.x;
				for (int x = 0; x < size.x; ++x) {
					auto pixel = data + row_offset + x;
					if (pixel->r > 150 && pixel->g > 150 && pixel->b > 150) {
						pixel->r = pixel->g = pixel->b = 0;
					}
					else {
						pixel->r = pixel->g = pixel->b = 255;
					}
				}
			}
		}
		//cv::imshow("ocr", ocr_image.as_cv_mat());
		//cv::waitKey(8);

		auto mat = ocr_image.as_cv_mat();
		ocr.SetImage(mat.data, mat.cols, mat.rows, 3, mat.step);
		ocr.SetSourceResolution(70);

		auto result = ocr.GetUTF8Text();

		auto len = strlen(result);
		if (len > job.sink_max_length) {
			len = job.sink_max_length;
		}

		memcpy(job.sink, result, len);
		delete[] result;
		*job.sink_result_length = len;
		manager->signal_completed_job();
	}
}

ReadTextMaster::ReadTextMaster() {
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);

	workers.reserve(sys_info.dwNumberOfProcessors);
	for (int i = 0; i < sys_info.dwNumberOfProcessors; ++i) {
		workers.emplace_back(i, &manager);
	}
}

void ReadTextMaster::perform_jobs(std::vector<ReadTextJob>& jobs) {
	manager.add_work_and_wait(jobs);
}