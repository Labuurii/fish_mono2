#pragma once

#include "library.h"
#include <atomic>

struct ReadTextJob 
{
	Image* screen_image = nullptr;
	Rect screen_part;
	char* sink = nullptr;
	int* sink_result_length = nullptr;
	int sink_max_length = 0;
};

class ReadTextManager 
{
	std::mutex mutex;
	std::condition_variable cv;
	std::vector<ReadTextJob> jobs;
	std::atomic<int> completed_jobs_counter = 0;
public:
	void wait_for_work(ReadTextJob* job);
	void signal_completed_job();

	void add_work_and_wait(const std::vector<ReadTextJob>& jobs);
};

class ReadTextWorker {
	ReadTextManager* manager;

	void run();
public:
	ReadTextWorker(DWORD_PTR logical_core_index, ReadTextManager* manager);
};

class ReadTextMaster
{
	ReadTextManager manager;
	std::vector<ReadTextWorker> workers;
public:
	ReadTextMaster();

	void perform_jobs(std::vector<ReadTextJob>& jobs);
};

