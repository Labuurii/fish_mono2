#pragma once

#ifdef HIGHSPEED_EXPORTS
#    define HIGHSPEED_API __declspec(dllexport)
#else
#    define HIGHSPEED_API __declspec(dllimport)
#endif