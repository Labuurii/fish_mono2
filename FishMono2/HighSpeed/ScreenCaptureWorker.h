#pragma once

#include <mutex>
#include <thread>
#include "library.h"
#include <atomic>

class ScreenCaptureWorker
{
	std::thread thread;
	ScreenCapturer* capturer;
	Image current_image;
	Vec2I screen_size;
	std::mutex mutex;
	std::atomic<bool> should_run = true;

	void run_worker();
public:
	ScreenCaptureWorker(ScreenCapturer* capturer, Vec2I screen_size);
	~ScreenCaptureWorker();

	void get_image(Image* sink);
};

