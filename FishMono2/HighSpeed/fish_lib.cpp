#include "fish_lib.h"

#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

#include "MultiThreadedDetectChanges.h"
#include "ScreenCaptureWorker.h"
#include "ReadText.h"

#include <io.h>
#include <stdlib.h>

struct ScreenResource {
	ScreenCapturer capturer;
	ScreenCaptureWorker capture_worker;

	Rect screen_rect;
	Image previous_screen_image, screen_image, changes_image;
	MultiThreadedDetectChangesMaster detect_changes_master;
	
	std::vector<cv::Rect> reused_nms_boxes;
	std::vector<float> reused_nms_confidences;
	std::vector<int> reused_kept_nms_indicies;
	std::vector<cv::Rect> reused_final_nms_boxes;

	tesseract::TessBaseAPI ocr;
	Image ocr_image;

	std::vector<ReadTextJob> reused_read_text_array_jobs;
	ReadTextMaster read_text_array_master;
	
	ScreenResource() 
		: capture_worker(&capturer, capturer.get_size())
	{
		auto screen_size = capturer.get_size();
		screen_rect.right = screen_size.x;
		screen_rect.bottom = screen_size.y;

		previous_screen_image.resize(capturer.get_size());
		screen_image.resize(capturer.get_size());
		changes_image.resize(capturer.get_size());
	}

	bool init() {
		auto init_r = ocr.Init(NULL, "eng", tesseract::OEM_LSTM_ONLY);
		if (init_r < 0) {
			return false;
		}
		ocr.SetPageSegMode(tesseract::PSM_AUTO);
		ocr.SetVariable("tessedit_char_whitelist", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ=.-");
		return true;
	}

	Image* get_image(GlobalImage image) {
		switch (image) {
		case GlobalImage::Screen:
			return &screen_image;
		case GlobalImage::Changes:
			return &changes_image;
		default:
			assert(false && "Unhandled enum value");
		}
	}
};

bool has_white(BGR* data, Vec2I image_size, int start_x, int start_y, int area_size) {
	
	for(int y = start_y; y < start_y + area_size; ++y) {
		for(int x = start_x; x < start_x + area_size; ++x) {
			auto image_pixel = data + (y * image_size.x) + x;
			if(*image_pixel == BGR(255,255,255)) {
				return true;
			}
		}
	}
	
	return false;
}

bool top_first(cv::Rect a, cv::Rect b) {
	return a.y < b.y;
}

extern "C" {

	void* create_screen_resource() {
		auto resource = new ScreenResource();
		if (!resource->capturer.is_valid() || !resource->init()) {
			delete resource;
			return nullptr;
		}

		return resource;
	}

	void destroy_screen_resource(void* screen_resource) {
		delete screen_resource;
	}

	void screen_resource_get_size(void* screen_resource_void, Vec2I* sink) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		*sink = screen_resource->screen_rect.get_size();
	}

	void screen_resource_blacken_part(void* screen_resource_void, Rect rect) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		auto screen_rect = screen_resource->screen_rect;
		auto screen_size = screen_rect.get_size();
		rect = screen_rect.bound_rectangle(rect);
		
		auto data = screen_resource->changes_image.get_data();
		for(int y = rect.top; y < rect.bottom; ++y) {
			auto row_offset = y * screen_size.x;
			for(int x = rect.left; x < rect.right; ++x) {
				auto pixel = data + row_offset + x;
				*pixel = BGR(0,0,0);
			}
		}
	}

	void screen_resource_save_image_part(void* screen_resource_void, const char* path, Rect screen_part, GlobalImage image) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		Image img;
		img.set(*screen_resource->get_image(image), screen_part);
		cv::imwrite(path, img.as_cv_mat());
	}

	void screen_resource_show_image_part(void* screen_resource_void, const char* name, Rect screen_part, GlobalImage image) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		Image img;
		img.set(*screen_resource->get_image(image), screen_part);
		cv::imshow(name, img.as_cv_mat());
		cv::waitKey(8);
	}


	bool screen_resource_update(void* screen_resource_void) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		screen_resource->previous_screen_image = screen_resource->screen_image;
		screen_resource->capture_worker.get_image(&screen_resource->screen_image);
		
		screen_resource->detect_changes_master.detect_changes(
			screen_resource->screen_image,
			screen_resource->previous_screen_image,
			screen_resource->changes_image
		);

		return true;
	}

	int screen_resource_compute_fishing_bobbers(void* screen_resource_void, Rect screen_part, Rect* nms_boxes_result, int max_nms_boxes) {
		auto screen_resource = (ScreenResource*)screen_resource_void;
		int bobber_count = 0;
		
		const int STEP_SIZE = 3;
		const int AREA_SIZE = 10;
		const float NMS_MIN_CONFIDENCE = 0.5f;
		const float NMS_THRESHOLD = 0.1f;
		
		auto image_data = screen_resource->changes_image.get_data();
		auto size = screen_resource->screen_rect.get_size();
		auto& nms_boxes = screen_resource->reused_nms_boxes;
		auto& nms_confidences = screen_resource->reused_nms_confidences;
		
		nms_boxes.clear();
		nms_confidences.clear();
		
		// Compute all the boxes
		for(int y = screen_part.top; y < screen_part.bottom - AREA_SIZE; y += STEP_SIZE) {
			for(int x = screen_part.left; x < screen_part.right - AREA_SIZE; x += STEP_SIZE) {
				if(has_white(image_data, size, x, y, STEP_SIZE)) {
					cv::Rect rect;
					rect.y = y;
					rect.x = x;
					rect.width = AREA_SIZE;
					rect.height = AREA_SIZE;
					
					nms_boxes.push_back(rect);
					nms_confidences.push_back(1.0f);
				}
			}
		}
		
		
		// NMS the boxes
		auto& kept_nms_indicies = screen_resource->reused_kept_nms_indicies;
		auto& final_nms_boxes = screen_resource->reused_final_nms_boxes;
		
		kept_nms_indicies.clear();
		final_nms_boxes.clear();
		
		cv::dnn::NMSBoxes(nms_boxes, nms_confidences, NMS_MIN_CONFIDENCE, NMS_THRESHOLD, kept_nms_indicies);
		
		for(auto index  : kept_nms_indicies) {
			final_nms_boxes.push_back(nms_boxes[index]);
			//cv::rectangle(data->debug_image.as_cv_mat(), nms_boxes[index], cv::Scalar(255,255,255), 2);
		}
		
		std::sort(final_nms_boxes.begin(), final_nms_boxes.end(), top_first);
		
		for(auto nms_box : final_nms_boxes) {
			if(bobber_count >= max_nms_boxes) {
				break;
			}
			
			nms_boxes_result[bobber_count] = Rect::from_cv(nms_box);
			++bobber_count;
		}
		
		return bobber_count;
	}

	int screen_resource_count_white_pixels(void* screen_resource_void, Rect screen_part) 
	{
		int count = 0;

		ScreenResource* screen_resource = (ScreenResource*)screen_resource_void;
		auto image_data = screen_resource->changes_image.get_data();
		auto size = screen_resource->screen_rect.get_size();

		for (int y = screen_part.top; y < screen_part.bottom; ++y) {
			for (int x = screen_part.left; x < screen_part.right; ++x) {
				auto image_pixel = image_data + (y * size.x) + x;
				if (*image_pixel == BGR(255, 255, 255)) {
					++count;
				}
			}
		}

		return count;
	}

	int screen_resource_read_text(void* screen_resource_void, Rect screen_part, char* sink, int sink_max_length)
	{
		assert(false && "Deprecated and outdated");
		ScreenResource* screen_resource = (ScreenResource*)screen_resource_void;

		screen_resource->ocr_image.set(screen_resource->screen_image, screen_part);

		{
			auto data = screen_resource->ocr_image.get_data();
			auto size = screen_resource->ocr_image.get_size();

			for (int y = 0; y < size.y; ++y) {
				auto row_offset = y * size.x;
				for (int x = 0; x < size.x; ++x) {
					auto pixel = data + row_offset + x;
					if (pixel->r > 150 && pixel->g > 150 && pixel->b > 150) {
						pixel->r = pixel->g = pixel->b = 0;
					}
					else {
						pixel->r = pixel->g = pixel->b = 255;
					}
				}
			}
		}

		//cv::imwrite("output.jpg", screen_resource->ocr_image.as_cv_mat());
		//cv::imshow("ocr", screen_resource->ocr_image.as_cv_mat());
		//cv::waitKey(8);
		auto mat = screen_resource->ocr_image.as_cv_mat();
		screen_resource->ocr.SetImage(mat.data, mat.cols, mat.rows, 3, mat.step);

		auto result = screen_resource->ocr.GetUTF8Text();

		auto len = strlen(result);
		if (len > sink_max_length) {
			len = sink_max_length;
		}

		memcpy(sink, result, len);
		delete[] result;
		return len;
	}

	HIGHSPEED_API void screen_resource_read_text_array(void* screen_resource_void, int screen_part_count, Rect* screen_parts, int* text_lengths, char* sinks, int sink_element_length) {
		auto old_stdout = _dup(1);
		auto old_stderr = _dup(2);
		freopen("nul:", "a", stdout);
		freopen("nul:", "a", stderr);

		// Tesseract is bitch and outputs alot of crap I dont give a shit about. And there is no way to turn it off.
		
		ScreenResource* screen_resource = (ScreenResource*)screen_resource_void;

		auto& jobs = screen_resource->reused_read_text_array_jobs;
		jobs.clear();
		for (std::int64_t i = 0; i < screen_part_count; ++i) {
			auto rect = screen_parts[i];
			auto sink = sinks + i * sink_element_length;
			auto result_length = text_lengths + i;

			ReadTextJob job;
			job.screen_image = &screen_resource->screen_image;
			job.screen_part = rect;
			job.sink = sink;
			job.sink_max_length = sink_element_length;
			job.sink_result_length = result_length;

			jobs.push_back(job);
		}

		screen_resource->read_text_array_master.perform_jobs(jobs);

		_dup2(old_stdout, 1);
		_dup2(old_stderr, 2);
		_close(old_stdout);
		_close(old_stderr);
	}

	HIGHSPEED_API void have_fun42(void* window) {
		auto hwnd = (HWND)window;
		LONG lStyle = GetWindowLongPtr(hwnd, GWL_STYLE);
		lStyle &= ~(WS_CAPTION | WS_THICKFRAME);
		SetWindowLongPtr(hwnd, GWL_STYLE, lStyle);
		SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
	}

	void window_minimize_size(void* window) {
		auto hwnd = (HWND)window;
		LONG lStyle = GetWindowLongPtr(hwnd, GWL_STYLE);
		lStyle &= ~(WS_CAPTION | WS_THICKFRAME);
		SetWindowLongPtr(hwnd, GWL_STYLE, lStyle);
		SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);
	}
}