#pragma once

#include <string>
#include <d3d9.h>
#include <Windows.h>
#include <vector>

#include <opencv2/core.hpp>

float clamp(float v, float min, float max);
float clamp01(float v);

enum class MsgFormat {
	Msg,
	Warning,
	Error,
	Success
};

struct Vec2I {
	int x = 0;
	int y = 0;

	Vec2I() = default;
	Vec2I(int x, int y);
	
	bool operator==(Vec2I other) const noexcept;
};

struct Vec2 {
	float x = 0;
	float y = 0;
};

struct Rect {
	int left = 0;
	int top = 0;
	int right = 0;
	int bottom = 0;

	cv::Rect to_cv() const;
	static Rect from_cv(cv::Rect r);
	Rect bound_rectangle(Rect sub) const;
	bool has_size() const;
	Vec2I get_size() const;
};

struct BGR {
	unsigned char b = 0;
	unsigned char g = 0;
	unsigned char r = 0;
	
	BGR() = default;
	BGR(unsigned char b, unsigned char g, unsigned char r);
	
	bool operator==(BGR other) const;
	bool operator!=(BGR other) const;
};

struct RGB {
	unsigned char r, g, b;
};

__forceinline
float norm_avg_delta(BGR a, BGR b) {

	/*unsigned char db = a.b - b.b;
	unsigned char dg = a.g - b.g;
	unsigned char dr = a.r - b.r;

	int sum_i = db + dg + dr;
	float sum = sum_i;

	return (sum / 3.0f) / 255.0f;*/

	float a_arr[3];
	a_arr[0] = a.b;
	a_arr[1] = a.g;
	a_arr[2] = a.r;

	float b_arr[3];
	b_arr[0] = b.b;
	b_arr[1] = b.g;
	b_arr[2] = b.r;

	float deltas[3];

	for(int i = 0; i < 3; ++i) {
		deltas[i] = a_arr[i] - b_arr[i];
		if(deltas[i] < 0.0f) {
			deltas[i] = -deltas[i];
		}
	}

	float sum = 0.0f;
	for(int i = 0; i < 3; ++i) {
		sum += deltas[i];
	}

	return ( sum / 3.0f ) / 255.0f;
}



class Image {
	std::unique_ptr<BGR[]> data;
	Vec2I size;
public:
	Image() = default;
	Image(Image&&) = default;
	Image(const Image& other);

	void operator=(const Image& other);
	void operator=(cv::Mat& other);

	BGR* get_data() const noexcept;
	Vec2I get_size() const noexcept;
	
	void resize(Vec2I size) noexcept;
	void set(Image& other, Rect rect) noexcept;
	
	cv::Mat as_cv_mat() const noexcept;
};

class ScreenCapturer {
	IDirect3D9* d3d = nullptr;
    IDirect3DDevice9* device = nullptr;
    IDirect3DSurface9* surface = nullptr;
    D3DPRESENT_PARAMETERS parameters = { 0 };
    D3DDISPLAYMODE mode; // Inited in constructor
    D3DLOCKED_RECT rc = {};
    UINT pitch = 0;
public:
	ScreenCapturer() noexcept;
	~ScreenCapturer() noexcept;
	
	Vec2I get_size() const noexcept;
	
	bool is_valid() const noexcept;
	bool take_screenshot(Image* image, Rect part) noexcept;
};

enum class KeyboardModifiers : int {
	None = 0,
	LeftShift = 1,
	LeftCtrl = 1 << 1,
	LeftAlt = 1 << 2,
	RightAlt = 1 << 3,
	RightCtrl = 1 << 4,
	RightShift = 1 << 5 // TODO: Make sure this button is used in bot.cpp properly
};

struct KeybindingConfig {
	int key = -1;
	KeyboardModifiers modifiers = KeyboardModifiers::None;
};

bool get_all_pressed_buttons(std::vector<int>* normal_buttons_sink, KeyboardModifiers* keyboard_modifiers_sink);
bool get_normal_button_name(int search_vk, const char** sink);
bool get_keyboard_modifier_names(KeyboardModifiers mods, std::vector<const char*>* sink);
void get_complete_keybinding_string(KeybindingConfig keybinding, std::string* sink);

struct WindowId {
	HWND hwnd = NULL;

	WindowId() = default;
	WindowId(HWND hwnd);

	bool is_valid() const;
};

void get_all_windows(std::vector<WindowId>* sink) noexcept;
bool get_window_name(WindowId id, std::string* sink) noexcept;
bool get_window_class(WindowId id, std::string* sink) noexcept;
bool get_single_window(std::string title, std::string class_, WindowId* sink) noexcept;
bool get_window_rect(WindowId id, Rect* rect) noexcept;
WindowId get_focused_window() noexcept;
bool is_window_focused(WindowId w) noexcept;
bool is_window_alive(WindowId w) noexcept;

bool get_mouse_pos(Vec2I* sink) noexcept;
bool move_mouse_to(Vec2I abs_p) noexcept;
bool press_right_mouse_button(WindowId w, Vec2I mouse_pos) noexcept;
bool release_right_mouse_button(WindowId w, Vec2I mouse_pos) noexcept;
bool press_left_mouse_button(WindowId w, Vec2I mouse_pos) noexcept;
bool release_left_mouse_button(WindowId w, Vec2I mouse_pos) noexcept;

bool press_button(WindowId window, int key) noexcept;
bool write_char(WindowId window, char ch) noexcept;
bool write_string(WindowId window, const std::string& str) noexcept;

std::vector<WindowId> get_wow_windows();