﻿using Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class CleanupFirefox : RepeatComponent
    {
        public CleanupFirefox() : base(5)
        {
        }

        protected override void PerformJob()
        {
            Task.Run(delegate ()
            {
                var processes = Process.GetProcessesByName("firefox");
                foreach (var p in processes) p.Kill();
            });
        }
    }
}
