﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public interface ICommand
    {
        bool IsInRange(Vector2 pos);
        IEnumerator Start(WoWInstance instance, BotContext context);
    }
}
