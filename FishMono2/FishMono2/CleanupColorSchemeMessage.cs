﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class CleanupColorSchemeMessage : RepeatComponent
    {
        public CleanupColorSchemeMessage() : base(10)
        {
        }

        protected override void PerformJob()
        {
            Task.Run(delegate() {
                var handle = Win32.FindWindow(null, "Windows");
                if (handle != IntPtr.Zero)
                {
                    Win32.ShowWindow(handle, Win32.ShowWindowCommands.SW_MINIMIZE);
                }
            });   
        }
    }
}
