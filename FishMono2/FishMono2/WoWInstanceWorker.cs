﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public abstract class WoWInstanceWorker
    {
        readonly protected List<WoWInstance> instances = new List<WoWInstance>();

        public void Add(WoWInstance i)
        {
            if(!instances.Contains(i))
            {
                instances.Add(i);
            }
        }

        public void TryRemove(WoWInstance i)
        {
            instances.Remove(i);
        }

        public abstract void Update();
    }
}
