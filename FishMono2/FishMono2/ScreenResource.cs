﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ScreenResource : IDisposable
    {
        const int MAX_TEXT_SIZE = 2048;
        const int MAX_TEXT_ARRAY_COUNT = 128;
        const int MAX_NMS_BOXES = 2048;

        unsafe struct TextArrayData
        {
            public fixed int screen_parts[MAX_TEXT_ARRAY_COUNT * 4];
            public fixed int text_lengths[MAX_TEXT_ARRAY_COUNT];
            public fixed byte text[MAX_TEXT_SIZE * MAX_TEXT_ARRAY_COUNT];
        }

        unsafe struct NMSBoxesData
        {
            public fixed int nms_boxes[MAX_NMS_BOXES * 4];
        }

        

        IntPtr cpp_ptr;
        readonly IntPtr text_buffer_unmanaged = Marshal.AllocHGlobal(MAX_TEXT_SIZE);

        

        public ScreenResource()
        {
            cpp_ptr = HighSpeed.create_screen_resource();
            if(cpp_ptr == IntPtr.Zero)
            {
                throw new Exception("Could not initialize the screen resource");
            }
        }

        public Vector2I GetSize()
        {
            Vector2I result = new Vector2I();
            HighSpeed.screen_resource_get_size(cpp_ptr, out result);
            return result;
        }

        public void Update()
        {
            if(!HighSpeed.screen_resource_update(cpp_ptr))
            {
                throw new Exception("Could not update the screen");
            }
        }

        public string ReadText(Rect part)
        {
            var length = HighSpeed.screen_resource_read_text(cpp_ptr, part, text_buffer_unmanaged, MAX_TEXT_SIZE);
            if(length == 0)
            {
                return string.Empty;
            }

            return Marshal.PtrToStringAnsi(text_buffer_unmanaged, length);
        }

        public void ReadText(List<Rect> rects, List<string> sink)
        {
            if(sink.Count > MAX_TEXT_ARRAY_COUNT)
            {
                throw new Exception("Can not read more than " + MAX_TEXT_ARRAY_COUNT + " at one time");
            }

            unsafe
            {
                TextArrayData data;
                for (var i = 0; i < rects.Count; ++i)
                {
                    var rect = rects[i];
                    var screen_part_index = i * 4;
                    data.screen_parts[screen_part_index] = rect.Left;
                    data.screen_parts[screen_part_index + 1] = rect.Top;
                    data.screen_parts[screen_part_index + 2] = rect.Right;
                    data.screen_parts[screen_part_index + 3] = rect.Bottom;
                }

                for(var i = 0; i < MAX_TEXT_SIZE * MAX_TEXT_ARRAY_COUNT; ++i)
                {
                    data.text[i] = (byte)'A';
                }
                

                HighSpeed.screen_resource_read_text_array(cpp_ptr, rects.Count, data.screen_parts, data.text_lengths, data.text, MAX_TEXT_SIZE);

                sink.Clear();
                for (var i = 0; i < rects.Count; ++i)
                {
                    var len = data.text_lengths[i];
                    var text = data.text + i * MAX_TEXT_SIZE;
                    if(len > 0)
                    {
                        var str = Marshal.PtrToStringAnsi((IntPtr)text, len);
                        sink.Add(str);
                    } else
                    {
                        sink.Add(string.Empty);
                    }
                }
            }
        }

        public void ShowImage(string window_name, Rect part, HighSpeed.GlobalImage image)
        {
            HighSpeed.screen_resource_show_image_part(cpp_ptr, window_name, part, image);
        }

        public void SaveAsImage(string path, Rect part, HighSpeed.GlobalImage global_image)
        {
            HighSpeed.screen_resource_save_image_part(cpp_ptr, path, part, global_image);
        }

        // TODO: Blacken part, CountWhitePixels

        public void ComputeNMSBoxes(Rect screen_part, List<Rect> sink)
        {
            NMSBoxesData data;
            unsafe
            {
                var count = HighSpeed.screen_resource_compute_fishing_bobbers(cpp_ptr, screen_part, data.nms_boxes, MAX_NMS_BOXES);
                for(int i = 0; i < count; ++i)
                {
                    var nms_index = i * 4;
                    var rect = new Rect
                    {
                        Left = data.nms_boxes[nms_index],
                        Top = data.nms_boxes[nms_index + 1],
                        Right = data.nms_boxes[nms_index + 2],
                        Bottom = data.nms_boxes[nms_index + 3],
                    };
                    sink.Add(rect);
                }
            }
        }

        public int CountWhitePixels(Rect area)
        {
            return HighSpeed.screen_resource_count_white_pixels(cpp_ptr, area);
        }

        public void BlackenPart(Rect area)
        {
            HighSpeed.screen_resource_blacken_part(cpp_ptr, area);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Nothing to do
                }

                if(cpp_ptr != IntPtr.Zero)
                {
                    HighSpeed.destroy_screen_resource(cpp_ptr);
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~ScreenResource()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion


    }
}
