﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class AHOpenData : ITransmittable
    {
        public int is_open;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state.Parse(out is_open);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.AHOpenData);
        }
    }
}
