﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class FishingSpotData
    {
        public IFishingSpot fishing_spot;
        public HourRange daily_fish_range;
        public bool exit_on_death;
    }
}
