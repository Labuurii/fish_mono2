﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ClickButtonCommand : ICommand
    {
        ButtonClick click;

        public ClickButtonCommand(int button)
        {
            click = new ButtonClick(button, KeyMod.None);
        }

        public ClickButtonCommand(Win32.VK button)
        {
            this.click = new ButtonClick(button, KeyMod.None);
        }

        public ClickButtonCommand(ButtonClick click)
        {
            this.click = click;
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            yield return instance.window.ClickButton(click);
        }
    }
}
