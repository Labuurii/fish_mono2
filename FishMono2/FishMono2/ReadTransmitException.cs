﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ReadTransmitException : Exception
    {
        public ReadTransmitException(string msg) : base(msg) { }
    }
}
