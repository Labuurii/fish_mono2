﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class MerchantData : ITransmittable
    {
        public int merchant_window_open;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state.Parse(out merchant_window_open);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.MerchantData);
        }
    }
}
