﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class LearnAllSpells
    {
        public WorldPathData to_trainer_path, back_path;
        public Vector2 trainer_pos;
        public int level;

        public LearnAllSpellsCommand Construct()
        {
            return new LearnAllSpellsCommand(to_trainer_path.Construct(), trainer_pos, back_path.Construct(), level);
        }
    }

    public class LearnAllSpellsCommand : ICommand
    {
        readonly WorldPath start_path;
        readonly CommandList list;
        readonly int level;

        public LearnAllSpellsCommand(WorldPath to_trainer, Vector2 vendor_pos, WorldPath back_from_trainer, int level)
        {
            this.start_path = to_trainer;
            this.level = level;
            this.list = new CommandList(new ICommand[]
            {
                new RunAndCombatCommand(to_trainer, -1, -1, null, 0),
                new LookAtCommand(vendor_pos),
                new ClickButtonCommand(BuiltinWoWButtons.ShowUI),
                new ClickCenterCommand(MouseButton.Right),
                new WaitForCommand(2.0f),
                new ClickButtonCommand(BuiltinWoWButtons.TrainAll),
                new WaitForCommand(2.0f),
                new ClickButtonCommand(BuiltinWoWButtons.TrainAll),
                new WaitForCommand(2.0f),
                new ClickButtonCommand(BuiltinWoWButtons.TrainerClose),
                new WaitForCommand(0.5f),
                new ClickButtonCommand(BuiltinWoWButtons.PlaceSpells),
                new ClickButtonCommand(BuiltinWoWButtons.HideUI),
                new RunAndCombatCommand(back_from_trainer, -1, -1, null, 0)
            });
        }

        public bool IsInRange(Vector2 pos)
        {
            return start_path.IsInRange(pos);
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var chour_data = new LevelChourData();
            yield return instance.text_reader.Request(chour_data);

            if (chour_data.current_level != level || chour_data.current_level_xp > chour_data.current_level_max_xp / 10)
            {
                yield break;
            }

            var closest_point_index = start_path.GetClosestPointIndex(chour_data.player_pos);
            if (closest_point_index == -1 || Vector2.Distance(chour_data.player_pos, start_path.points[closest_point_index]) > 50.0f)
            {
                yield break;
            }

            yield return list.Run(instance, context);
        }
    }
}
