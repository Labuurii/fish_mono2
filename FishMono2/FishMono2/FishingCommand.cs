﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class Fishing
    {
        public Vector2 pos;
        public Vector2 look_at_pos;
        public bool trace_char_info;
        public float runtime = 1 * 60 * 60; // 1 hour
        public int min_pixel_change = 8;

        public ICommand Construct()
        {
            var list = new List<ICommand>();
            list.Add(new WalkPath {
                points = new Vector2[] { pos }
            }.Construct());
            list.Add(new LookAtCommand(look_at_pos));
            list.Add(new FishingCommand(trace_char_info, runtime, min_pixel_change));
            return new CommandList(list.ToArray());
        }
    }

    public class FishingCommand : ICommand
    {
        bool trace_char_info;
        float runtime;
        int min_pixel_change_inclusive;
        

        public static readonly Vector2I AcceptBuffOverride = new Vector2I(45, 30);

        public FishingCommand(bool trace_char_info, float runtime, int min_pixel_change = 8)
        {
            this.trace_char_info = trace_char_info;
            this.runtime = runtime;
            this.min_pixel_change_inclusive = min_pixel_change;
        }

        IEnumerator apply_fishing_buff(WoWInstance instance)
        {
            var topleft = instance.windowAlloc.area.TopLeft();

            yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
            yield return instance.window.ClickButton(FishingButtons.BrightBaubles);
            yield return FiberOps.WaitFor(2);
            yield return instance.MouseClickAtAbs(topleft + AcceptBuffOverride, MouseButton.Left);
            yield return instance.MouseClickAtAbs(topleft + AcceptBuffOverride, MouseButton.Left);
            yield return instance.MouseClickAtAbs(topleft + AcceptBuffOverride, MouseButton.Left);
            yield return instance.MouseClickAtAbs(topleft + AcceptBuffOverride, MouseButton.Left);
            yield return instance.MouseClickAtAbs(topleft + AcceptBuffOverride, MouseButton.Left);
            yield return FiberOps.WaitFor(5);
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var fishing_end_time = DateTime.Now.AddSeconds(runtime);

            if (instance.should_shut_down)
            {
                yield break;
            }

            // Make sure character is not swimming
            {
                var pathing_data = new PathingData();
                for(var i = 0; i < 10; ++i)
                {
                    yield return instance.text_reader.Request(pathing_data);
                    if(pathing_data.player_swimming != 0)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.WalkBackward, 0.5f);
                    } else
                    {
                        break;
                    }
                }

                if(pathing_data.player_swimming != 0)
                {
                    throw new Exception("Could not get out of the water while trying to do fishing");
                }
            }

            CountDown char_info_countdown = null;
            if(trace_char_info)
            {
                char_info_countdown = new CountDown(60);
            }

            var anti_afk_cd = new CountDown(5 * 60);

            yield return instance.window.ClickButton(TransmitButtons.End);
            yield return instance.window.ClickButton(FishingButtons.EquipPole);
            yield return instance.SetFishingWindow(instance.window);
            yield return FiberOps.WaitFor(1);

            var nms_boxes = new List<Rect>();
            var screen_size = context.screen.GetSize();
            for (; ; )
            {
                if(instance.should_shut_down)
                {
                    break;
                }

                if(DateTime.Now >= fishing_end_time)
                {
                    break;
                }

                if(char_info_countdown != null)
                {
                    if(char_info_countdown.HasCountedDown())
                    {
                        char_info_countdown.Reset();

                        yield return instance.SetDefaultWindow(instance.window);
                        yield return FiberOps.WaitFor(1);
                        yield return instance.text_reader.Request(instance.account);
                        if(instance.account.current_fishing == instance.account.max_fishing)
                        {
                            yield break;
                        } else
                        {
                            yield return instance.window.ClickButton(TransmitButtons.End);
                        }
                        yield return instance.SetFishingWindow(instance.window);
                        yield return FiberOps.WaitFor(1);
                        continue;
                    }
                }

                if(instance.bright_bouble_countdown.HasCountedDown())
                {
                    instance.bright_bouble_countdown.Reset();
                    yield return apply_fishing_buff(instance);
                }

                if(anti_afk_cd.HasCountedDown())
                {
                    anti_afk_cd.Reset();
                    yield return instance.window.ClickButton(BuiltinWoWButtons.Jump);
                    yield return FiberOps.WaitFor(2);
                }

                var area = instance.windowAlloc.area;
                context.screen.BlackenPart(area);
                yield return instance.window.ClickButton(FishingButtons.Fish);
                yield return FiberOps.WaitFor(4);
                //context.screen.ShowImage("fish", area, HighSpeed.GlobalImage.Changes);

                nms_boxes.Clear();
                context.screen.ComputeNMSBoxes(area, nms_boxes);

                var last_white_pixel_count = 0;
                if (nms_boxes.Count > 0)
                {
                    int fish_hooked_counter = 0;
                    int invalid_bobber_counter = 0;

                    log(instance, "====");

                    var nms_box = nms_boxes[0];
                    var extended_nms_box = nms_box;
                    extended_nms_box.Right += 10;
                    extended_nms_box.Right = Mathf.Clamp(extended_nms_box.Right, 0, screen_size.x);
                    extended_nms_box.Top -= 10;
                    extended_nms_box.Top = Mathf.Clamp(extended_nms_box.Top, 0, screen_size.y);
                    var cast_end_time = DateTime.Now.AddSeconds(30);

                    for (; ; )
                    {
                        context.screen.BlackenPart(area);
                        yield return null;

                        var changes = context.screen.CountWhitePixels(extended_nms_box);
                        if (changes != last_white_pixel_count)
                        {
                            last_white_pixel_count = changes;
                            log(instance, changes.ToString());
                            if (changes >= min_pixel_change_inclusive)
                            {
                                ++fish_hooked_counter;
                                if (fish_hooked_counter >= 2)
                                {
                                    log(instance, "caught");
                                    var click_pos = nms_box.TopLeft();
                                    click_pos.x += 3;
                                    click_pos.y += 2;
                                    yield return instance.MouseClickAtAbs(click_pos, MouseButton.Right);
                                    break;
                                }
                            }
                            else
                            {
                                fish_hooked_counter = 0;
                            }


                            if (changes == 0)
                            {
                                ++invalid_bobber_counter;
                                if (invalid_bobber_counter > 5)
                                {
                                    log(instance, "Fish, wrong bobber");
                                    break;
                                }
                            }
                            else
                            {
                                invalid_bobber_counter = 0;
                            }
                        }

                        if (DateTime.Now >= cast_end_time)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    log(instance, "Could not find bobber");
                }

                yield return instance.window.ClickButton(FishingButtons.StopCasting);
                yield return instance.window.ClickButton(FishingButtons.Cleanup);
                yield return instance.window.ClickButton(FishingButtons.OpenClam);
                yield return FiberOps.WaitFor(3);
            }

            yield return instance.SetDefaultWindow(instance.window);
        }

        private static void log(WoWInstance instance, string msg)
        {
            if (Args.Instance.LogFishScores)
            {
                instance.Log("Fish " + msg);
            }
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }
    }
}
