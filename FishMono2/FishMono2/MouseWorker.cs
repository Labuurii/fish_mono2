﻿using Shared;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FishMono2
{
    public abstract class MouseJob
    {
        volatile bool _isCompleted;

        public bool isCompleted => _isCompleted;

        protected abstract void PerformImpl();

        public void Perform()
        {
            try
            {
                PerformImpl();
            } finally
            {
                _isCompleted = true;
            }
        }
    }

    public class MouseClickAtJob : MouseJob
    {
        Window window;
        Vector2I screen_pos;
        MouseButton btn;
        int wait_dur_milliseconds;

        public MouseClickAtJob(Window window, Vector2I screen_pos, MouseButton btn, float wait_dur)
        {
            this.window = window;
            this.screen_pos = screen_pos;
            this.btn = btn;
            this.wait_dur_milliseconds = (int) ( wait_dur * 1000.0f );
        }

        protected override void PerformImpl()
        {
            var handle = window.handle;
            Win32.WM wm_down, wm_up;
            var lparam = Win32.MAKELPARAM(screen_pos.x, screen_pos.y);

            switch (btn)
            {
                case MouseButton.Left:
                    wm_down = Win32.WM.LBUTTONDOWN;
                    wm_up = Win32.WM.LBUTTONUP;
                    break;
                case MouseButton.Right:
                    wm_down = Win32.WM.RBUTTONDOWN;
                    wm_up = Win32.WM.RBUTTONUP;
                    break;
                default:
                    throw new Exception("Unhandled enum value " + btn);
            }

            Win32Safe.SetCursorPos(screen_pos.x, screen_pos.y);

            Thread.Sleep(wait_dur_milliseconds); // TODO: Can I remove this?
            Win32.SendMessage(handle, (int)wm_down, IntPtr.Zero, (IntPtr)lparam);
            Thread.Sleep(wait_dur_milliseconds);
            Win32.SendMessage(handle, (int)wm_up, IntPtr.Zero, (IntPtr)lparam);
            Thread.Sleep(wait_dur_milliseconds); // TODO: Can I remove this?
        }
    }

    public class MouseWorker
    {
        readonly ConcurrentQueue<MouseJob> jobs = new ConcurrentQueue<MouseJob>();
        volatile Exception exception; // This is guaranteed to be threadsafe by the language

        public MouseWorker()
        {
            var thread = new Thread(delegate() { Run(); });
            thread.IsBackground = true;
            thread.Name = "MouseWorker";
            thread.Start();
        }

        public void CheckException()
        {
            if(exception != null)
            {
                throw exception;
            }
        }

        public void AddJob(MouseJob job)
        {
            jobs.Enqueue(job);
        }

        private void Run()
        {
            try
            {
                for (;;)
                {
                    MouseJob job;
                    while (jobs.TryDequeue(out job))
                    {
                        job.Perform();
                    }

                    Thread.Sleep(3);
                }
            } catch(Exception e)
            {
                this.exception = e;
            }
        }
    }
}
