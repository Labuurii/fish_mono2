﻿using Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class Win32Ops
    {
        const int MAX_CLASS_NAME_LENGTH = 1024;
        static StringBuilder reused_class_name = new StringBuilder(MAX_CLASS_NAME_LENGTH);

        public static void GetWindowHandles(Process process, List<IntPtr> sink)
        {
            foreach (ProcessThread thread in process.Threads)
            {
                Win32.EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { sink.Add(hWnd); return true; }, IntPtr.Zero);
            }
        }

        public static string GetWindowTitle(IntPtr window)
        {
            int length = Win32.GetWindowTextLength(window);
            if(length == 0)
            {
                return string.Empty;
            }

            var builder = new StringBuilder(length);
            var actual_len = Win32.GetWindowText(window, builder, length + 1);
            if(actual_len != length)
            {
                return string.Empty;
            }

            return builder.ToString();
        }

        public static string GetWindowClass(IntPtr window)
        {
            var actual_len = Win32.GetClassName(window, reused_class_name, MAX_CLASS_NAME_LENGTH + 1);
            return reused_class_name.ToString(0, actual_len);
        }
    }
}
