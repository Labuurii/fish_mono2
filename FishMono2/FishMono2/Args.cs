﻿using Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class Args
    {
        public static Args Instance;

        public bool PrintFPS;
        public bool Highjack;
        public bool Test;
        public bool LogFishScores;
        public bool DontLaunchNew;
        public bool PrintReadTextJobCount;
        public bool DisableDeadLockCheck;
        public bool CaptchaRequired;

        public MasterFlags GetMasterFlags()
        {
            var flags = MasterFlags.None;
            if(Highjack)
            {
                flags |= MasterFlags.Highjack;
            }
            if(Test)
            {
                flags |= MasterFlags.LaunchTest;
            }
            if(DontLaunchNew)
            {
                flags |= MasterFlags.DontLaunchNew;
            }
            return flags;
        }

        public static void Init(string[] args)
        {
            Instance = global::Args.Configuration.Configure<Args>().CreateAndBind(args);
        }
    }
}
