﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public struct LevelGrind
    {
        public int max_level;
        public Vector2[] to_path, from_path, grinding_path;
    }

    public struct GrindLocation
    {
        public int start_level;
        public Vector2[] to_vendor;
        public Vector2[] from_vendor_to_trainer; // Can be null
        public Vector2[] to_origin;

        public LevelGrind[] grind_locations;

        public CommandList Construct()
        {
            var commands = new List<ICommand>();

            int current_level = start_level;

            foreach(var grind_loc in grind_locations)
            {
                for(;current_level < grind_loc.max_level; ++current_level)
                {
                    // Now standing at origin
                    //add_print(commands, "Origin");
                    commands.Add(new RunAndCombat
                    {
                        path = new WorldPathData
                        {
                            points = grind_loc.to_path,
                            pointCompletedDistance = 3.0f
                        },
                        max_level = current_level + 1
                    }.Construct());

                    // Now at the path start location
                    //add_print(commands, "Start Loc");
                    commands.Add(new RunAndCombat
                    {
                        path = new WorldPathData
                        {
                            points = grind_loc.grinding_path,
                            flags = WorldPathFlags.Circular,
                            pointCompletedDistance = 3.0f
                        },
                        max_level = current_level + 1
                    }.Construct());

                    // Now anywhere on the path
                    //add_print(commands, "Anywhere");
                    commands.Add(new RunAndCombat
                    {
                        path = new WorldPathData
                        {
                            points = grind_loc.grinding_path,
                            flags = WorldPathFlags.AvoidCombat,
                            pointCompletedDistance = 3.0f
                        },
                        max_level = current_level + 2
                    }.Construct());

                    // Now at the path end point
                    //add_print(commands, "End point");
                    commands.Add(new RunAndCombat
                    {
                        path = new WorldPathData
                        {
                            points = grind_loc.from_path,
                            flags = WorldPathFlags.AvoidCombat,
                            pointCompletedDistance = 3.0f
                        },
                        max_level = current_level + 2
                    }.Construct());

                    // Now at the location origin
                    //add_print(commands, "Origin 2");
                    commands.Add(new VendorAllItems
                        {
                            vendor_pos = to_vendor[to_vendor.Length - 1],
                            to_vendor_path = new WorldPathData
                            {
                                points = to_vendor,
                                flags = WorldPathFlags.AvoidCombat,
                                pointCompletedDistance = 2.0f
                            },
                            back_path = new WorldPathData
                            {
                                points = new Vector2[] { to_vendor[to_vendor.Length - 1] },
                                flags = WorldPathFlags.AvoidCombat,
                                pointCompletedDistance = 2.0f
                            },
                            level = current_level + 1
                    }.Construct());

                    // Now standing at vendor
                    //add_print(commands, "Vendor");
                    if (from_vendor_to_trainer != null && from_vendor_to_trainer.Length > 0)
                    {
                        commands.Add(new LearnAllSpells
                        {
                            trainer_pos = from_vendor_to_trainer[from_vendor_to_trainer.Length - 1],
                            to_trainer_path = new WorldPathData
                            {
                                points = from_vendor_to_trainer,
                                flags = WorldPathFlags.AvoidCombat,
                                pointCompletedDistance = 2.0f,
                            },
                            back_path = new WorldPathData
                            {
                                points = new Vector2[] { from_vendor_to_trainer[from_vendor_to_trainer.Length - 1] },
                                flags = WorldPathFlags.AvoidCombat,
                                pointCompletedDistance = 2.0f
                            },
                            level = current_level + 1,
                        }.Construct());
                    }

                    // Now standing at trainer or vendor
                    //add_print(commands, "TrainerOrVendor");
                    commands.Add(new RunAndCombat
                    {
                        path = new WorldPathData
                        {
                            points = to_origin,
                            flags = WorldPathFlags.AvoidCombat,
                            pointCompletedDistance = 2.0f,
                        },
                        max_level = current_level + 2
                    }.Construct());

                    // Not back at origin
                }
            }

            return new CommandList(commands.ToArray());
        }
    }


}
