﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class GrindingPaths
    {
        public static readonly Vector2[] ShadowGlen04Start = new Vector2[]
        {
            new Vector2( 10314.8f, 722.6f ),
            new Vector2( 10325.5f, 696.7f ),
            new Vector2( 10319.8f, 665.5f ),
            new Vector2( 10357.4f, 665.6f ),
            new Vector2( 10392.6f, 647.0f ),
            new Vector2( 10401.4f, 599.8f ),
            new Vector2( 10407.6f, 564.2f ),
            new Vector2( 10427.0f, 541.9f ),
            new Vector2( 10420.0f, 570.0f ),
            new Vector2( 10412.5f, 595.6f ),
            new Vector2( 10426.3f, 630.4f ),
            new Vector2( 10414.8f, 666.2f ),
            new Vector2( 10386.2f, 685.3f ),
            new Vector2( 10338.1f, 678.0f ),
            new Vector2( 10318.7f, 708.4f ),
        };

        public static readonly WorldPathData ToValleyOfTrials04 = new WorldPathData
        {
            points = new Vector2[]
            {
                new Vector2( -615.3f, -4251.4f ),
                new Vector2( -597.7f, -4265.3f ),
                new Vector2( -552.2f, -4273.8f ),
                new Vector2( -529.0f, -4279.8f ),
                new Vector2( -481.4f, -4251.4f ),
            },
            pointCompletedDistance = 5.0f,
        };

        public static readonly Vector2[] ValleyOfTrials04 = new Vector2[]
        {
            new Vector2( -481.4f, -4251.4f ),
            new Vector2( -470.0f, -4207.0f ),
            new Vector2( -418.2f, -4206.5f ),
            new Vector2( -394.6f, -4220.5f ),
            new Vector2( -391.4f, -4256.3f ),
            new Vector2( -383.6f, -4287.7f ),
            new Vector2( -374.8f, -4303.6f ),
            new Vector2( -393.3f, -4307.0f ),
            new Vector2( -394.7f, -4333.5f ),
            new Vector2( -384.3f, -4359.6f ),
            new Vector2( -365.6f, -4377.2f ),
            new Vector2( -403.4f, -4375.2f ),
            new Vector2( -413.7f, -4348.4f ),
            new Vector2( -411.7f, -4322.7f ),
            new Vector2( -442.8f, -4328.1f ),
            new Vector2( -456.5f, -4352.6f ),
            new Vector2( -504.4f, -4348.2f ),
            new Vector2( -519.0f, -4302.0f ),
            new Vector2( -508.1f, -4278.5f ),
            new Vector2( -491.7f, -4258.5f ),
        };

        public static readonly WorldPathData ToValleyOfTrials07 = new WorldPathData
        {
            points = new Vector2[]
            {
                new Vector2( -615.3f, -4250.6f ),
                new Vector2( -597.7f, -4261.4f ),
                new Vector2( -551.0f, -4274.3f ),
                new Vector2( -534.3f, -4278.3f ),
                new Vector2( -495.5f, -4256.3f ),
                new Vector2( -478.0f, -4227.3f ),
                new Vector2( -431.9f, -4206.9f ),
                new Vector2( -409.3f, -4183.1f ),
                new Vector2( -403.0f, -4163.8f ),
            },
            flags = WorldPathFlags.AvoidCombat,
            pointCompletedDistance = 3.0f,
        };

        public static readonly Vector2[] ValleyOfTrials07 = new Vector2[]
        {
            new Vector2( -404.8f, -4162.1f ),
            new Vector2( -461.8f, -4129.9f ),
            new Vector2( -428.6f, -4075.4f ),
            new Vector2( -395.9f, -4081.2f ),
            new Vector2( -382.3f, -4055.8f ),
            new Vector2( -366.5f, -4051.6f ),
            new Vector2( -358.2f, -4084.4f ),
            new Vector2( -328.9f, -4121.8f ),
            new Vector2( -307.4f, -4146.0f ),
            new Vector2( -268.7f, -4182.4f ),
            new Vector2( -281.1f, -4199.1f ),
            new Vector2( -309.9f, -4177.2f ),
            new Vector2( -340.5f, -4160.7f ),
            new Vector2( -387.7f, -4159.1f ),
            new Vector2( -395.7f, -4159.1f ),
        };
    }
}
