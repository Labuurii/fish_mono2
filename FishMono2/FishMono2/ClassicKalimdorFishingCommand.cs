﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    class ClassicKalimdorFishingCommand : ICommand
    {
        static readonly FishingSpotData[] fishing_spots = new FishingSpotData[]
        {
            new FishingSpotData
            {
                fishing_spot = new MoongladeFishingSpot(),
                daily_fish_range = HourRange.Construct(-1, 1),
                exit_on_death = false
            },
            new FishingSpotData
            {
                fishing_spot = new TanarisFishingSpot(),
                daily_fish_range = HourRange.Construct(1, 11),
                exit_on_death = false
            },
            new FishingSpotData
            {
                fishing_spot = new TanarisFishingSpot(),
                daily_fish_range = HourRange.Construct(11, 14),
                exit_on_death = true
            },
        };

        static readonly ICommand to_flight_master = ThunderBluffInnkeeperToFlightMaster.Path;
        static readonly Vector2 home_town_origin = ThunderBluffInnkeeperToFlightMaster.Origin;

        public bool IsInRange(Vector2 pos)
        {
            throw new NotImplementedException();
        }

        IFishingSpot get_current_fishingspot(WoWInstance instance, out DateTime starttime_servertime, out DateTime endtime_servertime, out bool exit_on_death)
        {
            var servertime_now = instance.ServerTime();
            var servertime_today = DateTimeOps.Today(servertime_now);
            var servertime_tomorrow = DateTimeOps.Today(servertime_now).AddDays(1);
            var servertime_hour = servertime_now.Hour;

            foreach(var fishing_spot_data in fishing_spots)
            {
                var range = fishing_spot_data.daily_fish_range;
                starttime_servertime = servertime_today.AddHours(range.start_hour);
                endtime_servertime = servertime_today.AddHours(range.end_hour);
                exit_on_death = fishing_spot_data.exit_on_death;
                if (servertime_now >= starttime_servertime && servertime_now < endtime_servertime)
                {
                    return fishing_spot_data.fishing_spot;
                }
                starttime_servertime = servertime_tomorrow.AddHours(range.start_hour);
                endtime_servertime = servertime_tomorrow.AddHours(range.end_hour);
                if (servertime_now >= starttime_servertime && servertime_now < endtime_servertime)
                {
                    return fishing_spot_data.fishing_spot;
                }
            }

            starttime_servertime = new DateTime();
            endtime_servertime = new DateTime();
            exit_on_death = false;
            return null;
        }

        IFishingSpot get_next_fishingspot(WoWInstance instance, out DateTime sink_starttime_servertime, out DateTime sink_endtime_servertime, out bool sink_exit_on_death)
        {
            sink_starttime_servertime = new DateTime();
            sink_endtime_servertime = new DateTime();
            sink_exit_on_death = false;

            var servertime_now = instance.ServerTime();
            var servertime_today = DateTimeOps.Today(servertime_now);

            var current_start_time_servertime = DateTime.UtcNow.AddYears(1);
            IFishingSpot current_fishing_spot = null;

            foreach(var fishing_spot_data in fishing_spots)
            {
                var range = fishing_spot_data.daily_fish_range;
                var next_starttime_servertime = servertime_today.AddHours(range.start_hour);
                while(next_starttime_servertime < servertime_now)
                {
                    next_starttime_servertime = next_starttime_servertime.AddDays(1);
                }
                if(next_starttime_servertime < current_start_time_servertime)
                {
                    current_start_time_servertime = next_starttime_servertime;
                    current_fishing_spot = fishing_spot_data.fishing_spot;
                    var endtime_servertime = servertime_today.AddHours(range.end_hour);
                    sink_starttime_servertime = next_starttime_servertime;
                    sink_endtime_servertime = endtime_servertime;
                    sink_exit_on_death = fishing_spot_data.exit_on_death;
                }
            }

            return current_fishing_spot;
        }

        void select_fishing_spot(WoWInstance instance, out IFishingSpot fishing_spot, out DateTime sink_starttime_servertime, out DateTime sink_endtime_servertime, out bool exit_on_death)
        {
            fishing_spot = get_current_fishingspot(instance, out sink_starttime_servertime, out sink_endtime_servertime, out exit_on_death);
            if(fishing_spot != null)
            {
                return;
            }

            fishing_spot = get_next_fishingspot(instance, out sink_starttime_servertime, out sink_endtime_servertime, out exit_on_death);
        }

        static IEnumerator wait_for_fishing_spot_schedule(WoWInstance instance, DateTime fishing_spot_starttime_servertime)
        {
            if (instance.ServerTime() < fishing_spot_starttime_servertime)
            {
                yield return BotOps.LongWaitUntilServerTime(instance, fishing_spot_starttime_servertime);
            }
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var get_home_data = new GetHomeData();

            for(;;)
            {
                yield return instance.text_reader.Request(get_home_data);

                // Handle taxi
                if (get_home_data.on_taxi != 0)
                {
                    yield return FiberOps.WaitFor(2);
                    continue;
                }

                // Handle combat
                if (get_home_data.player_in_combat != 0)
                {
                    yield return FiberOps.WaitFor(2);
                    continue;
                }

                // Try to handle death
                if (get_home_data.norm_player_hp == 0)
                {
                    instance.Log("We are dead, tries to resurrect");
                    foreach (var spot in fishing_spots)
                    {
                        var command = spot.fishing_spot.GetWalkToCommand();
                        if (command.IsInRange(get_home_data.player_pos))
                        {
                            yield return command.Start(instance, context);
                            break;
                        }
                    }

                    for (; ; )
                    {
                        yield return instance.text_reader.Request(get_home_data);

                        if (get_home_data.norm_player_hp > 0)
                        {
                            break;
                        }

                        yield return instance.window.ClickButton(TransmitButtons.End);
                        instance.Log("We are dead, awaiting help.");
                        yield return FiberOps.WaitFor(10);
                    }
;
                }

                // Figure out our next fishing trip
                DateTime fishing_spot_endtime_servertime, fishing_spot_starttime_servertime;
                IFishingSpot fishing_spot;
                bool fishing_spot_exit_on_death;
                select_fishing_spot(instance, out fishing_spot, out fishing_spot_starttime_servertime, out fishing_spot_endtime_servertime, out fishing_spot_exit_on_death);
                if(fishing_spot == null)
                {
                    instance.Log("Could not find any fishing spot");
                    yield return BotOps.LongWaitUntil(instance, DateTime.Now.AddHours(1));
                    continue;
                }

                if (instance.ServerTime() < fishing_spot_starttime_servertime)
                {
                    instance.Log(string.Format("Selected fishing spot {0}, now waits for {1} hours for the time to start", fishing_spot.GetType().Name, (fishing_spot_starttime_servertime - instance.ServerTime()).TotalHours));
                } else
                {
                    instance.Log(string.Format("Chooses fishing spot {0}, will be fishing here for {1} hours", fishing_spot.GetType().Name, (fishing_spot_endtime_servertime - instance.ServerTime()).TotalHours));
                }

                // Get back home
                if (!fishing_spot.GetWalkToCommand().IsInRange(get_home_data.player_pos))
                {
                    // Get back home
                    if (Vector2.Distance(get_home_data.player_pos, home_town_origin) > 5)
                    {
                        while (get_home_data.is_hearthstone_on_cd != 0)
                        {
                            yield return BotOps.LongWaitUntil(instance, DateTime.Now.AddMinutes(30));
                            yield return instance.text_reader.Request(get_home_data);
                        }

                        yield return instance.window.ClickButton(BuiltinWoWButtons.Hearthstone);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.Hearthstone);
                        do
                        {
                            yield return FiberOps.WaitFor(2);
                            yield return instance.text_reader.Request(get_home_data);
                        } while (get_home_data.is_casting != 0);

                        yield return FiberOps.WaitFor(4);
                        yield return instance.text_reader.Request(get_home_data);
                        if (get_home_data.norm_player_hp == 0 || get_home_data.player_in_combat != 0)
                        {
                            continue;
                        }

                        if (!to_flight_master.IsInRange(get_home_data.player_pos))
                        {
                            instance.Log("Could not get home");
                            yield return BotOps.LongWaitUntil(instance, DateTime.Now.AddMinutes(5));
                            continue;
                        }
                    }

                    // Cleanup worthless items
                    yield return instance.window.ClickButton(FishingButtons.Cleanup);

                    // Delete overquoted fishes
                    yield return instance.window.ClickButton(BuiltinWoWButtons.DestroyOverquotedFish);

                    // Show Hidden buttons in the Appraiser addon has some toggle values which has to be reset
                    yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                    yield return instance.window.EnterStringInChat("/reload");
                    yield return instance.text_reader.Request(get_home_data);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);

                    var has_fishes_to_put_on_ah = true;
                    /*{ Inventory State read is not reliable
                        var bag_state = new InventoryState();
                        yield return instance.text_reader.Request(bag_state);
                        has_fishes_to_put_on_ah = bag_state.fishes.Max() > 0;
                    }*/

                    // Go to AH
                    yield return instance.window.ClickButton(FishingButtons.EquipPole);
                    var ah_open_data = new AHOpenData();
                    yield return ThunderBluffInnToAH.Perform(instance, ah_open_data);
                    if (ah_open_data.is_open == 0)
                    {
                        throw new Exception("Could not open the auction house");
                    }

                    yield return instance.window.ClickButton(TransmitButtons.End);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                    yield return FiberOps.WaitFor(1);

                    // Scan the AH
                    // I dont do prizing right now, it is done by Appraiser, it is not terrible atleast. But I dont have full control.
                    for (int i = 0; i < (int)Fish.COUNT; ++i)
                    {
                        var name = FishOps.GetName((Fish)i);
                        yield return BotOps.QueryAuctionItems(instance, name);
                    }

                    // Put everything on the auction house
                    if (has_fishes_to_put_on_ah)
                    {
                        yield return PostItemsWithAppraiser.Perform(instance);
                    }

                    yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);

                    // Open MailBox
                    yield return ThunderBluffFromAHToMailBox.Perform(instance);

                    // Generate Mailbox report
                    /* Currently not implemented, not instrumental
                    var mail_report = new MailReport();
                    yield return instance.text_reader.Request(mail_report);*/

                    // Take out raw money, and all the fishes for the next fishing trip
                    yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                    yield return BotOps.TakeOutAllMoney(instance);

                    // Take out fishes to use on the next fishing trip
                    yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                    var fishing_spot_fishes = fishing_spot.AvailableFishes();
                    foreach (var fish in fishing_spot_fishes)
                    {
                        yield return BotOps.TakeOutAllFish(instance, fish);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.DestroyOverquotedFish);
                    }

                    yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);

                    // Go to fishing vendor and get bright baubles
                    {
                        yield return instance.window.ClickButton(FishingButtons.UnequipPole);
                        yield return instance.window.ClickButton(FishingButtons.UnequipPole);
                        var merchant_data = new MerchantData();
                        yield return ThunderBluffMailBoxToFishingVendor.Perform(instance, merchant_data);
                        if (merchant_data.merchant_window_open == 0)
                        {
                            throw new Exception("Could not open fishing vendor merchant data");
                        }

                        yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                        yield return FiberOps.WaitFor(2);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.BuyBrightBaubles);
                        yield return FiberOps.WaitFor(8);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
                    }

                    // Go to Inn
                    yield return ThunderBluffFishingVendorToInn.Perform(instance);

                    yield return wait_for_fishing_spot_schedule(instance, fishing_spot_starttime_servertime);

                    // Go to flight master, fly
                    yield return ThunderBluffInnkeeperToFlightMaster.FromInnkeeperToRiderToDestination(instance, fishing_spot.GetTaxiLocation());
                }
                else
                { // GetBackHome
                    yield return wait_for_fishing_spot_schedule(instance, fishing_spot_starttime_servertime);
                }

                // Perform fishing
                yield return instance.window.ClickButton(FishingButtons.UnequipPole);
                yield return instance.window.ClickButton(BuiltinWoWButtons.DestroyOverquotedFish);
                instance.death_counter.Reset();
                {
                    for (; ; )
                    {
                        if(instance.should_shut_down)
                        {
                            break;
                        }

                        if (instance.ServerTime() >= fishing_spot_endtime_servertime)
                        {
                            break;
                        }

                        yield return fishing_spot.GetWalkToCommand().Start(instance, instance.context);

                        if(instance.death_counter.Value > 0)
                        {
                            instance.Log("We have died and this fishing spot wants to exit on death...");
                            yield return BotOps.LongWaitUntilServerTime(instance, fishing_spot_endtime_servertime);
                            break;
                        }

                        if (instance.should_shut_down)
                        {
                            break;
                        }

                        if (instance.ServerTime() >= fishing_spot_endtime_servertime)
                        {
                            break;
                        }

                        yield return BotOps.CameraUp(instance);
                        yield return fishing_spot.GetFishingCommand().Start(instance, instance.context);

                        // Inventory state read is not reliable
                        /*var bag_state = new InventoryState();
                        yield return instance.text_reader.Request(bag_state);
                        if (bag_state.left_bag_slots <= 1 || FishOps.HasHitAllQuotas(fishing_spot.AvailableFishes(), bag_state))
                        {
                            break;
                        }*/
                    }
                }
            }
        }
    }
}
