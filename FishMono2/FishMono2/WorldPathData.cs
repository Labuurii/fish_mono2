﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class WorldPathData
    {
        public Vector2[] points;
        public WorldPathFlags flags = WorldPathFlags.Default;
        public float stopAndTurnDistance = 6.0f;
        public float pointCompletedDistance = 3.0f;

        public WorldPath Construct()
        {
            var r = new WorldPath(flags, points);
            r.stopAndTurnDistance = stopAndTurnDistance;
            r.pointCompletedDistance = pointCompletedDistance;
            return r;
        }
    }
}
