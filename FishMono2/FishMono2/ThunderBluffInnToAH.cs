﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public class ThunderBluffInnToAH
    {
        public static readonly Vector2 AuctioneerPoint = new Vector2(-1199.0f, 110.6f);

        public static readonly ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                new Vector2( -1298.4f, 39.6f ),
                new Vector2( -1288.6f, 44.0f ),
                new Vector2( -1283.0f, 46.9f ),
                new Vector2( -1276.4f, 50.3f ),
                new Vector2( -1250.9f, 74.2f ),
                new Vector2( -1237.1f, 91.4f ),
                new Vector2( -1215.2f, 96.0f ),
                new Vector2( -1211.5f, 103.1f ),
                new Vector2( -1204.5f, 111.4f ),
                new Vector2( -1201.4f, 111.1f ),
            }
        }.Construct();

        public static readonly ICommand[] AHTalkPositions = new ICommand[]
        {
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1200.3f, 111.0f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1199.1f, 111.3f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1198.0f, 110.5f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1198.5f, 109.7f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1200.4f, 108.9f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1200.4f, 110.0f), }
            }.Construct()
        };

        public static IEnumerator Perform(WoWInstance instance, AHOpenData ah_open_data)
        {
            yield return Path.Start(instance, instance.context);
            foreach(var walk_point in AHTalkPositions)
            {
                yield return walk_point.Start(instance, instance.context);
                yield return new LookAtCommand(AuctioneerPoint).Start(instance, instance.context);
                yield return instance.MouseClickAtAbs(instance.windowAlloc.area.GetCenter(), MouseButton.Right);
                yield return FiberOps.WaitFor(2);
                yield return instance.text_reader.Request(ah_open_data);

                if(ah_open_data.is_open != 0)
                {
                    break;
                }
            }
        }
    }
}
