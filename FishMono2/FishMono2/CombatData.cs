﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class CombatData : ITransmittable
    {
        public float player_hp_norm
            , player_mana_norm;
        public Vector2 player_position;
        public float player_angle_radians;
        public int is_casting;

        public int player_in_combat
            , target_in_combat;

        public int has_target;
        public float target_hp_norm;

        public int last_wow_error;
        public int error_count;

        // Modified by WoWInstance, is not actually transmitted
        public bool is_new_error;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out player_hp_norm)
                .Parse(out player_mana_norm)
                .Parse(out player_position)
                .Parse(out player_angle_radians)
                .Parse(out is_casting)
                .Parse(out player_in_combat)
                .Parse(out target_in_combat)
                .Parse(out has_target)
                .Parse(out target_hp_norm)
                .Parse(out last_wow_error)
                .Parse(out error_count);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.CombatData);
        }
    }
}
