﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ComputerCleanup
    {
        readonly List<RepeatComponent> jobs = new List<RepeatComponent>();

        public ComputerCleanup()
        {
            jobs.Add(new CleanupWoWVoiceProxy());
            jobs.Add(new CleanupWin7Warning());
            jobs.Add(new CleanupFirefox());
            jobs.Add(new CleanupColorSchemeMessage());
        }

        public void Update()
        {
            foreach(var job in jobs)
            {
                job.Update();
            }
        }
    }
}
