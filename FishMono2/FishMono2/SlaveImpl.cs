﻿using Master;
using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class SlaveImpl : ISlave
    {
        public Runtime runtime;

        static BotState get_bot_state(WoWInstance instance)
        {
            if(instance.has_stopped)
            {
                return BotState.Stopped;
            }
            if(instance.is_paused)
            {
                return BotState.Paused;
            }
            return BotState.Running;
        }

        public SlaveImpl(Runtime runtime)
        {
            this.runtime = runtime;
        }

        public void AddAccountToLaunch(WoWAccount account)
        {
            runtime.AddAccount(account);
        }

        public WoWAccount GetAccountByWindow(Window window)
        {
            foreach(var instance in runtime.wow_instances)
            {
                if(instance.window == window)
                {
                    return instance.account;
                }
            }

            return null;
        }

        public Window GetTopLeftWindow()
        {
            var alloc = runtime.bot_context.window_alloc.GetFirstAlloc();
            if(alloc != null)
            {
                return alloc.window;
            }
            return null;
        }

        public Window GetWindowToTheSideOf(Vector2I screen_pos, Vector2I search_direction)
        {
            var alloc = runtime.bot_context.window_alloc.GetWindowToTheSideOf(screen_pos, search_direction);
            if(alloc == null)
            {
                return null;
            }
            return alloc.window;
        }

        public BotState GetBotState(Window window)
        {
            foreach(var instance in runtime.wow_instances)
            {
                if(instance.window == window)
                {
                    return get_bot_state(instance);
                }
            }

            return BotState.Stopped;
        }

        public void RestartAccount(WoWAccount account)
        {
            runtime.RemoveAccount(account.email);
            runtime.AddAccount(account);
        }

        public void RestartWindow(Window selected_window)
        {
            for(int i = 0; i < runtime.wow_instances.Count; ++i)
            {
                var instance = runtime.wow_instances[i];
                if(instance.window == selected_window)
                {
                    runtime.RemoveAccount(i);
                    runtime.AddAccount(instance.account);
                    break;
                }
            }
        }

        public void SetProcessesToHighjack(List<Process> processes)
        {
            foreach(var p in processes)
            {
                var window = new Window(p.MainWindowHandle);
                runtime.AddAccount(p.Id, window);
            }
        }

        public void Shutdown(Window selected_window)
        {
            for(int i = 0; i < runtime.wow_instances.Count; ++i)
            {
                if(runtime.wow_instances[i].window == selected_window)
                {
                    runtime.RemoveAccount(i);
                    break;
                }
            }
        }

        public void ShutdownApp()
        {
            runtime.should_shut_down = true;
        }

        public void ShutdownLevelingAccounts()
        {
            for(int i = runtime.wow_instances.Count - 1; i >= 0; --i)
            {
                var instance = runtime.wow_instances[i];
                if(instance.is_leveling)
                {
                    instance.should_shut_down = true;
                }
            }
        }

        public BotState TogglePause(Window selected_window)
        {
            foreach(var instance in runtime.wow_instances)
            {
                if(instance.window == selected_window)
                {
                    instance.is_paused = !instance.is_paused;
                    return get_bot_state(instance);
                }
            }

            return BotState.Stopped;
        }

        public void TryRelaunchAccount(WoWAccount account)
        {
            foreach(var instance in runtime.wow_instances)
            {
                if(instance.account.email == account.email)
                {
                    if(instance.has_stopped)
                    {
                        instance.has_stopped = false;
                        instance.TerminateProcess();
                        instance.executor.SetEnumerator(BotOps.Start(instance, instance.window));
                    }
                    return;
                }
            }

            runtime.AddAccount(account);
        }

        public void ClickInMiddleOfWindow(Window window)
        {
            var job = new MouseClickAtJob(window, window.GetArea().GetCenter(), MouseButton.Right, 0.1f);
            runtime.bot_context.mouse_worker.AddJob(job);
        }

        public void ToggleFPS()
        {
            Args.Instance.PrintFPS = !Args.Instance.PrintFPS;
        }

        public void PrintEnumeratorStack(Window selected_window)
        {
            foreach (var instance in runtime.wow_instances)
            {
                if (instance.window == selected_window)
                {
                    instance.Log("\n" + instance.executor.GetEnumeratorsStackFrames());
                }
            }
        }
    }
}
