﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class ThunderBluffInnkeeperToFlightMaster
    {
        public static readonly Vector2 Origin = new Vector2(-1297.0f, 40.3f);
        public static readonly Vector2 FlightMasterPos = new Vector2(-1196.8f, 26.5f);

        public static readonly ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                Origin,
                new Vector2( -1296.9f, 39.5f ),
                new Vector2( -1288.6f, 43.6f ),
                new Vector2( -1284.2f, 45.9f ),
                new Vector2( -1279.9f, 48.3f ),
                new Vector2(-1253.9f, 68.2f),
                new Vector2(-1243.1f, 62.1f),
                new Vector2(-1210.0f, 39.0f),
                new Vector2(-1207.8f, 33.6f),
                new Vector2(-1208.1f, 30.7f),
                new Vector2(-1207.0f, 25.5f),
                new Vector2(-1203.3f, 22.2f),
                new Vector2(-1196.2f, 21.3f),
                new Vector2(-1192.7f, 22.6f),
                new Vector2(-1188.8f, 27.3f),
                new Vector2(-1188.7f, 34.9f),
                new Vector2(-1194.3f, 39.7f),
                new Vector2(-1201.4f, 39.3f),
                new Vector2(-1205.4f, 34.8f),
                new Vector2(-1206.3f, 30.5f),
                new Vector2(-1204.7f, 26.2f),
                new Vector2(-1202.7f, 23.8f),
                new Vector2(-1200.7f, 22.7f),
                new Vector2(-1194.4f, 22.5f),
                new Vector2(-1190.4f, 25.0f),
                new Vector2(-1188.2f, 29.1f),
                new Vector2(-1188.3f, 33.4f),
                new Vector2(-1190.2f, 36.9f),
                new Vector2(-1193.4f, 39.4f),
                new Vector2(-1197.8f, 40.2f),
                new Vector2(-1202.5f, 38.4f),
                new Vector2(-1205.5f, 33.9f),
                new Vector2(-1205.5f, 29.2f),
                new Vector2(-1201.1f, 26.5f),
                new Vector2(-1198.4f, 26.2f),
            }
        }.Construct();

        public static readonly ICommand[] RiderPositions = new ICommand[]
        {
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1198.1f, 25.9f), }
            }.Construct(),
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1197.0f, 27.2f), }
            }.Construct(),
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1195.5f, 26.6f), }
            }.Construct(),
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1195.8f, 25.1f), }
            }.Construct(),
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1196.8f, 24.8f), }
            }.Construct(),
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1196.8f, 25.5f), }
            }.Construct(),
        };

        public static IEnumerator FromInnkeeperToRiderToDestination(WoWInstance instance, Vector2I destination)
        {
            var topleft = instance.windowAlloc.area.TopLeft();
            yield return Path.Start(instance,instance.context);
            foreach(var rider_pos in RiderPositions)
            {
                yield return rider_pos.Start(instance, instance.context);
                yield return new LookAtCommand(FlightMasterPos).Start(instance, instance.context);
                yield return instance.MouseClickAtAbs(instance.windowAlloc.area.GetCenter(), MouseButton.Right);
                yield return FiberOps.WaitFor(2);
                yield return BotOps.TaxiTowards(instance, destination);
                yield return FiberOps.WaitFor(2);
                var taxi_data = new TaxiData();
                yield return instance.text_reader.Request(taxi_data);
                if(taxi_data.on_taxi != 0)
                {
                    while(taxi_data.on_taxi != 0)
                    {
                        yield return FiberOps.WaitFor(2);
                        yield return instance.text_reader.Request(taxi_data);
                    }
                    break;
                }
            }
        }
    }
}
