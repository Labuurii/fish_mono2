﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public enum Fish
    {
        OilyBlackmouth,
        FirefinSnapper,
        WinterSquid,
        SummerBass,
        NightfinSnapper,
        BigMouthClam,
        StoneScaleEel,
        GoldenPearl,
        BlackPearl,
        IrisdescentPearl,
        SmallLustrousPearl,
        SuperiorHealingPotion,
        COUNT
    }

    public static class FishOps
    {
        public static int GetQuota(Fish fish)
        {
            switch(fish)
            {
                case Fish.OilyBlackmouth:
                    return 20;
                case Fish.FirefinSnapper:
                    return 25;
                case Fish.WinterSquid:
                    return 18;
                case Fish.SummerBass:
                    return 10;
                case Fish.NightfinSnapper:
                    return 25;
                case Fish.BigMouthClam:
                    return 0;
                case Fish.StoneScaleEel:
                    return 25;
                case Fish.GoldenPearl:
                case Fish.BlackPearl:
                case Fish.IrisdescentPearl:
                case Fish.SmallLustrousPearl:
                    return 0;
                case Fish.SuperiorHealingPotion:
                    return 0;
                default:
                    throw new Exception("Unhandled enum value " + fish);
            }
        }

        public static bool IsEnabled(Fish fish)
        {
            switch(fish)
            {
                case Fish.SummerBass:
                    return true;
                case Fish.WinterSquid:
                    return false; // Was last seem 20 March
                default:
                    return true;
            }
        }

        public static string GetName(Fish fish)
        {
            switch(fish)
            {
                case Fish.OilyBlackmouth:
                    return "Oily Blackmouth";
                case Fish.FirefinSnapper:
                    return "Firefin Snapper";
                case Fish.WinterSquid:
                    return "Winter Squid";
                case Fish.SummerBass:
                    return "Raw Summer Bass";
                case Fish.NightfinSnapper:
                    return "Raw Nightfin Snapper";
                case Fish.BigMouthClam:
                    return "Big-mouth Clam";
                case Fish.StoneScaleEel:
                    return "Stonescale Eel";
                case Fish.GoldenPearl:
                    return "Golden Pearl";
                case Fish.BlackPearl:
                    return "Black Pearl";
                case Fish.IrisdescentPearl:
                    return "Iridescent Pearl";
                case Fish.SmallLustrousPearl:
                    return "Small Lustrous Pearl";
                case Fish.SuperiorHealingPotion:
                    return "Superior Healing Potion";
                default:
                    throw new Exception("Unhandled enum value " + fish);
            }
        }

        public static bool HasHitAllQuotas(Fish[] fishing_spot_fishes, InventoryState bag_state)
        {
            foreach(var fish in fishing_spot_fishes)
            {
                if(IsEnabled(fish) && GetQuota(fish) * 20 > bag_state.fishes[(int)fish])
                {
                    return false;
                }
            }

            return true;
        }
    }


    public class InventoryState : ITransmittable
    {
        // Not in amount of stacks
        public int[] fishes = new int[(int)Fish.COUNT];
        public int left_bag_slots;

        public void Parse(StringArrayParseState parse_state)
        {
            for(var i = 0; i < fishes.Length; ++i)
            {
                parse_state.Parse(out fishes[i]);
            }
            parse_state.Parse(out left_bag_slots);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.BagReport);
        }
    }
}
