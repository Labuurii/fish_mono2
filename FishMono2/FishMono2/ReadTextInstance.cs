﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ReadTextInstance : IDisposable
    {
        enum ReadMode
        {
            Invalid,
            None,
            CharSummary,
            CombatData,
            PathingData,
            LevelChourData,
            MageRestData,
            TaxiData,
            GetHomeData,
            BagReport,
            AHOpenData,
            AHListingData,
            MailReport,
            MerchantData
        }

        readonly WoWInstance wow_instance;
        ReadMode last_read_mode = ReadMode.Invalid;
        int current_read_index = 0;
        string current_read_text = string.Empty;

        string current_text;

        int last_wow_error_count = -1;

        public string currentTextReadIndexStr => current_read_text;
        public ITransmittable lastTransmit;
        public DateTime lastTransmitTime;

        public ReadTextInstance(WoWInstance i)
        {
            wow_instance = i;
        }

        public IEnumerator ResetIdCounter()
        {
            yield return wow_instance.window.ClickButton(TransmitButtons.ResetTransmitId);
            current_read_index = 0;
        }

        IEnumerator update_message_id(ReadMode new_read_mode, ITransmittable trans)
        {
            last_read_mode = new_read_mode;
            if (current_read_index > 3)
            {
                yield return ResetIdCounter();
            }

            yield return trans.RequestTransmit(wow_instance.window);
            ++current_read_index;
            current_read_text = "IT";
            for(int i = 0; i < current_read_index; ++i)
            {
                current_read_text += "1";
            }
            current_read_text += "=";
        }

        IEnumerator start_read_text(ReadMode new_read_mode, ITransmittable trans)
        {
            if(last_read_mode != new_read_mode)
            {
                yield return update_message_id(new_read_mode, trans);
            }

            current_text = null;
            wow_instance.context.read_text_worker.Add(wow_instance);
        }

        void stop_read_text()
        {
            wow_instance.context.read_text_worker.TryRemove(wow_instance);
        }

        IEnumerator read_text(ReadMode mode, ITransmittable trans)
        {
            yield return start_read_text(mode, trans);
            try
            {
                for(int i = 0; i < 30; ++i)
                {
                    var timeout = DateTime.Now;
                    timeout = timeout.AddSeconds(1);

                    for (; ; )
                    {
                        bool did_catch_exception = false;
                        try
                        {

                            if (!string.IsNullOrEmpty(current_text))
                            {
                                var parts = current_text.Split('=');
                                var parser = new StringArrayParseState(parts);
                                trans.Parse(parser);
                                this.lastTransmit = trans;
                                this.lastTransmitTime = DateTime.Now;
                                yield break;
                            }

                            if (DateTime.Now >= timeout)
                            {
                                throw new ReadTransmitException("Single request read text timed out");
                            }

                        }
                        catch(Exception e)
                        {
                            did_catch_exception = true;
                        }

                        if(did_catch_exception)
                        {
                            yield return update_message_id(mode, trans);
                            break;
                        }

                        yield return null;
                    }
                }

                throw new ReadTransmitException("Could not read text");
            } finally
            {
                stop_read_text();
            }
        }

        public void SetCurrentText(string t)
        {
            this.current_text = t;
        }

        public IEnumerator Request(WoWAccount account)
        {
            yield return read_text(ReadMode.CharSummary, account);
        }

        public IEnumerator Request(CombatData d)
        {
            yield return read_text(ReadMode.CombatData, d);
            d.is_new_error = d.error_count != this.last_wow_error_count;
            this.last_wow_error_count = d.error_count;
        }

        public IEnumerator Request(PathingData d)
        {
            yield return read_text(ReadMode.PathingData, d);
        }

        public IEnumerator Request(LevelChourData d)
        {
            yield return read_text(ReadMode.LevelChourData, d);
        }

        public IEnumerator Request(MageRestData d)
        {
            yield return read_text(ReadMode.MageRestData, d);
        }

        public IEnumerator Request(TaxiData d)
        {
            yield return read_text(ReadMode.TaxiData, d);
        }

        public IEnumerator Request(GetHomeData d)
        {
            yield return read_text(ReadMode.GetHomeData, d);
        }

        public IEnumerator Request(InventoryState d)
        {
            yield return read_text(ReadMode.BagReport, d);
        }

        public IEnumerator Request(AHOpenData d)
        {
            yield return read_text(ReadMode.AHOpenData, d);
        }

        public IEnumerator Request(int page, AHListingsData d)
        {
            for(int i = 0; i < 10; ++i)
            {
                d.page = page;
                yield return read_text(ReadMode.AHListingData, d);
                if(d.page == page)
                {
                    yield break;
                }
            }

            throw new ReadTransmitException("Could not get the correct page");
        }

        public IEnumerator Request(MailReport d)
        {
            yield return read_text(ReadMode.MailReport, d);
        }

        public IEnumerator Request(MerchantData d)
        {
            yield return read_text(ReadMode.MerchantData, d);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    wow_instance.context.read_text_worker.TryRemove(wow_instance);
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
