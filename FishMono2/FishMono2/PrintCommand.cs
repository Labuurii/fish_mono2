﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class PrintCommand : ICommand
    {
        string to_print;

        public PrintCommand(string to_print)
        {
            this.to_print = to_print;
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            Console.WriteLine(to_print);
            yield return null;
        }
    }
}
