﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class VendorAllItems
    {
        public WorldPathData to_vendor_path, back_path;
        public Vector2 vendor_pos;
        public int level;

        public VendorAllItemsCommand Construct()
        {
            return new VendorAllItemsCommand(to_vendor_path.Construct(), vendor_pos, back_path.Construct(), level);
        }
    }

    public class VendorAllItemsCommand : ICommand
    {
        readonly WorldPath start_path;
        readonly CommandList list;
        readonly int level;

        public VendorAllItemsCommand(WorldPath to_vendor, Vector2 vendor_pos, WorldPath back_from_vendor, int level)
        {
            this.start_path = to_vendor;
            this.level = level;
            this.list = new CommandList(new ICommand[]
            {
                new RunAndCombatCommand(to_vendor, -1, -1, null, 0),
                new LookAtCommand(vendor_pos),
                new ClickCenterCommand(MouseButton.Right),
                new WaitForCommand(2.0f),
                new ClickButtonCommand(BuiltinWoWButtons.VendorSellAll),
                new WaitForCommand(2.0f),
                new ClickButtonCommand(BuiltinWoWButtons.VendorClose),
                new RunAndCombatCommand(back_from_vendor, -1, -1, null, 0)
            });
        }

        public bool IsInRange(Vector2 pos)
        {
            return start_path.IsInRange(pos);
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var chour_data = new LevelChourData();
            yield return instance.text_reader.Request(chour_data);

            if(chour_data.current_level != level || chour_data.current_level_xp > chour_data.current_level_max_xp / 10)
            {
                yield break;
            }

            var closest_point_index = start_path.GetClosestPointIndex(chour_data.player_pos);
            if(closest_point_index == -1 || Vector2.Distance(chour_data.player_pos, start_path.points[closest_point_index]) > 50.0f)
            {
                yield break;
            }

            yield return list.Run(instance, context);
        }
    }
}
