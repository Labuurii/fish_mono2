﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FishMono2
{
    public class FrameRate
    {
        DateTime last_frame_start_time = DateTime.Now;

        public float frame_length = 1.0f / 60.0f;
        public float delta_time;

        public void Sync()
        {
            var now = DateTime.Now;
            var delta = now - last_frame_start_time;
            if(delta.TotalSeconds < frame_length)
            {
                var sleep_time = frame_length - delta.TotalSeconds;
                Thread.Sleep((int)( sleep_time * 1000.0f));
                now = DateTime.Now;
                delta = now - last_frame_start_time;
            }
            this.delta_time = (float)delta.TotalSeconds;
            last_frame_start_time = now;
        }
    }
}
