﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public unsafe static class HighSpeed
    {
        public enum GlobalImage
        {
            Screen,
            Changes
        }

        [DllImport("HighSpeed.dll")]
        public static extern void window_minimize_size(IntPtr window);

        [DllImport("HighSpeed.dll")]
        public extern static void have_fun42(IntPtr window);

        [DllImport("HighSpeed.dll")]
        public extern static IntPtr create_screen_resource();

        [DllImport("HighSpeed.dll")]
        public extern static void destroy_screen_resource(IntPtr s);

        [DllImport("HighSpeed.dll")]
        public extern static void screen_resource_get_size(IntPtr screen_resource, out Vector2I sink);

        [DllImport("HighSpeed.dll")]
        public extern static void screen_resource_blacken_part(IntPtr screen_resource, Rect rect);

        [DllImport("HighSpeed.dll")]
        public extern static void screen_resource_save_image_part(IntPtr screen_resource, string path, Rect screen_part, GlobalImage image);

        [DllImport("HighSpeed.dll")]
        public extern static void screen_resource_show_image_part(IntPtr screen_resource, string name, Rect screen_part, GlobalImage image);

        [DllImport("HighSpeed.dll")]
        public extern static bool screen_resource_update(IntPtr screen_resource);

        [DllImport("HighSpeed.dll")]
        public extern static int screen_resource_compute_fishing_bobbers(IntPtr screen_resource, Rect screen_part, int* nms_boxes_result, int max_nms_boxes);

        [DllImport("HighSpeed.dll")]
        public extern static int screen_resource_count_white_pixels(IntPtr screen_resource, Rect screen_part);

        [DllImport("HighSpeed.dll")]
        public extern static int screen_resource_read_text(IntPtr screen_resource, Rect screen_part, IntPtr char_sink, int sink_max_length);

        [DllImport("Highspeed.dll")]
        public extern static void screen_resource_read_text_array(IntPtr screen_resource, int screen_part_count, int* screen_parts, int* text_lengths, byte* sinks, int sink_element_length);
    }
}
