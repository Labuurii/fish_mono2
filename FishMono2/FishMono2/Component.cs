﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public abstract class Component
    {
        public WoWInstance instance;

        public Component(WoWInstance instance)
        {
            this.instance = instance;
        }

        public abstract void Update();
    }
}
