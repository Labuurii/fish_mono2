﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public class MoongladeFishingSpot : IFishingSpot
    {
        public static readonly Vector2[] PathPoints = new Vector2[]
        {
            new Vector2( 7469.3f, -2123.0f ),
            new Vector2( 7490.0f, -2126.1f ),
            new Vector2( 7525.8f, -2130.0f ),
            new Vector2( 7580.4f, -2224.6f ),
            new Vector2( 7595.8f, -2287.1f ),
            new Vector2( 7579.7f, -2359.5f ),
            new Vector2( 7593.7f, -2431.0f ),
            new Vector2( 7808.3f, -2756.6f ),
            new Vector2( 7851.0f, -2771.8f ),
        };

        public static readonly ICommand Path = new CommandList(new ICommand[]
        {
            new CameraDownCommand(),
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat | WorldPathFlags.IgnoreCombat,
                    points = PathPoints
                },
                death_handler = new DeathWalkAlongPath
                {
                    points = new Vector2[]
                    {
                        new Vector2( 7426.0f, -2809.0f ),
                        new Vector2( 7436.3f, -2796.0f ),
                        new Vector2( 7466.5f, -2782.7f ),
                        new Vector2( 7471.7f, -2692.2f ),
                        new Vector2( 7487.1f, -2665.0f ),
                        new Vector2( 7511.0f, -2623.4f ),
                        new Vector2( 7522.6f, -2605.9f ),
                        new Vector2( 7518.1f, -2559.2f ),
                        new Vector2( 7526.9f, -2500.5f ),
                        new Vector2( 7537.2f, -2482.5f ),
                        new Vector2( 7556.7f, -2431.6f ),
                        new Vector2( 7585.4f, -2387.6f ),
                        new Vector2( 7592.7f, -2362.6f ),
                        new Vector2( 7607.8f, -2325.1f ),
                        new Vector2( 7625.5f, -2289.9f ),
                        new Vector2( 7631.9f, -2253.7f ),
                        new Vector2( 7646.4f, -2229.5f ),
                        new Vector2( 7644.3f, -2187.9f ),
                        new Vector2( 7624.4f, -2145.3f ),
                        new Vector2( 7589.5f, -2117.2f ),
                        new Vector2( 7532.2f, -2125.7f ),
                        new Vector2( 7509.1f, -2126.0f ),
                        new Vector2( 7480.7f, -2125.0f ),
                    }.Concat(PathPoints).ToArray()
                }.Construct()
            }.Construct()
        });

        public static readonly ICommand StartFishingCommand = new Fishing
        {
            pos = new Vector2( 7855.6f, -2768.3f ),
            look_at_pos = new Vector2(7838.9f, -2776.2f),
            runtime = 30 * 60,
            min_pixel_change = 7
        }.Construct();

        public static readonly Vector2I TaxiLocation = TaxiLocations.Moonglade;

        public static readonly Fish[] Fishes = new Fish[]
        {
            Fish.OilyBlackmouth,
            Fish.NightfinSnapper,
            Fish.SuperiorHealingPotion
        };

        public Vector2I GetTaxiLocation()
        {
            return TaxiLocation;
        }

        public ICommand GetWalkToCommand()
        {
            return Path;
        }

        public ICommand GetFishingCommand()
        {
            return StartFishingCommand;
        }

        public Fish[] AvailableFishes()
        {
            return Fishes;
        }
    }
}
