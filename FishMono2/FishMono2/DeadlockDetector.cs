﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FishMono2
{
    public class DeadlockDetector
    {
        DateTime last_update_time = DateTime.Now;

        public DeadlockDetector()
        {
            var thread = new Thread(delegate() { Run(); });
            thread.Name = "DeadlockDetector";
            thread.IsBackground = true;
            thread.Start();
        }

        void Run()
        {
            for(;;)
            {
                DateTime last_update_time;
                lock(this)
                {
                    last_update_time = this.last_update_time;
                }
                if(last_update_time.AddSeconds(10) < DateTime.Now)
                {
                    Environment.Exit(-1);
                }
                Thread.Sleep(1000);
            }
        }

        public void Update()
        {
            lock(this)
            {
                last_update_time = DateTime.Now;
            }
        }
    }
}
