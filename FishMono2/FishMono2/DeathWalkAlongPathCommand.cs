﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class DeathWalkAlongPath
    {
        public Vector2[] points;

        public DeathWalkAlongPathCommand Construct()
        {
            return new DeathWalkAlongPathCommand(new WorldPathData
            {
                points = points
            }.Construct());
        }
    }

    public class DeathWalkAlongPathCommand : ICommand
    {
        WorldPath world_path;

        public DeathWalkAlongPathCommand(WorldPath world_path)
        {
            this.world_path = world_path;
        }

        public bool IsInRange(Vector2 pos)
        {
            return world_path.IsInRange(pos);
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var has_toggled_walking = false;
            var last_player_point = new Vector2();
            var path = new PathingState(world_path);
            var could_retrieve_corpse = false;
            var use_retrive_corpse_point = false;
            Vector2 could_retrive_corpse_point = new Vector2();
            var pathing_data = new PathingData();

            yield return instance.text_reader.Request(pathing_data);

            if(pathing_data.norm_player_hp != 0)
            {
                yield break;
            }

            instance.bright_bouble_countdown.ForceCountDownComplete();

            if(!path.Advance(pathing_data.player_pos))
            {
                yield break;
            }

            if(!WorldPath.IsInRange(pathing_data.player_pos, path.CurrentPoint()))
            {
                yield break;
            }

            instance.death_counter.Inc();

            var point = path.CurrentPoint();
            yield return BotOps.TurnTowardsPoint(instance, pathing_data.player_pos, pathing_data.player_angle_rad, point);

            for(;;)
            {
                yield return instance.text_reader.Request(pathing_data);

                if(!could_retrieve_corpse)
                {
                    if(pathing_data.can_retrieve_corpse != 0)
                    {
                        could_retrieve_corpse = true;
                        could_retrive_corpse_point = path.CurrentPoint();
                    }
                } else
                {
                    if(pathing_data.can_retrieve_corpse == 0 && !use_retrive_corpse_point)
                    {
                        use_retrive_corpse_point = true;
                    } else if(pathing_data.can_retrieve_corpse != 0 && use_retrive_corpse_point)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.DoManualStuff);
                        break;
                    }
                }

                if(!path.Advance(pathing_data.player_pos))
                {
                    if(pathing_data.can_retrieve_corpse != 0)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.DoManualStuff);
                    }

                    break;
                }

                if ((path.path.flags & WorldPathFlags.NoAntiStuck) != WorldPathFlags.NoAntiStuck && last_player_point == pathing_data.player_pos)
                {
                    yield return instance.window.TurnDegrees(90);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.StartAutoRun);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.Jump);
                }

                if(use_retrive_corpse_point)
                {
                    point = could_retrive_corpse_point;
                } else
                {
                    point = path.CurrentPoint();
                }

                if (pathing_data.current_player_speed == 0)
                {
                    if (!has_toggled_walking)
                    {
                        has_toggled_walking = true;
                        yield return instance.window.ClickButton(BuiltinWoWButtons.StartAutoRun);
                        yield return instance.text_reader.Request(pathing_data);

                        if (!path.Advance(pathing_data.player_pos))
                        {
                            break;
                        }
                    }
                    else
                    {
                        instance.window.ClickButtonAsync(BuiltinWoWButtons.StartAutoRun);
                    }
                }
                else
                {
                    if ((path.path.flags & WorldPathFlags.NoAntiStuck) == WorldPathFlags.NoAntiStuck && last_player_point == pathing_data.player_pos) // There is a bug where sometimes the animation is running but the character isnt
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.MoveBackward);
                    }
                }

                if (path.path.walk)
                {
                    if (pathing_data.current_player_speed != 0 && pathing_data.current_player_speed > 4.71f)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.ToggleWalking);
                    }
                }
                else
                {
                    if (pathing_data.current_player_speed != 0 && pathing_data.current_player_speed < 4.69f) // Swimming is 4.7
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.ToggleWalking);
                    }
                }

                var angles_to_turn = BotOps.AnglesToTurn(pathing_data.player_pos, pathing_data.player_angle_rad, point);
                if (Mathf.Positive(angles_to_turn) > 5)
                {
                    var point_distance = Vector2.Distance(point, pathing_data.player_pos);
                    var should_stop_and_turn = point_distance < path.path.stopAndTurnDistance;

                    if (should_stop_and_turn)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                    }

                    yield return instance.window.TurnDegrees(angles_to_turn);
                }

                last_player_point = pathing_data.player_pos;
            }

            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
        }
    }
}
