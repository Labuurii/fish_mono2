﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class GetHomeData : ITransmittable
    {
        public int is_hearthstone_on_cd;
        public Vector2 player_pos;
        public float norm_player_hp;
        public int is_casting;
        public int player_in_combat;
        public int mailbox_open;
        public int on_taxi;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out is_hearthstone_on_cd)
                .Parse(out player_pos)
                .Parse(out norm_player_hp)
                .Parse(out is_casting)
                .Parse(out player_in_combat)
                .Parse(out mailbox_open)
                .Parse(out on_taxi);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.GetHomeData);
        }
    }
}
