﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class TrollMage20
    {
        public static readonly CommandList command = new CommandList(new ICommand[]
        {
            new RunAndCombat
            {
                path = GrindingPaths.ToValleyOfTrials04,
                max_level = 2,
            }.Construct(),
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = GrindingPaths.ValleyOfTrials04,
                    flags = WorldPathFlags.Circular,
                    pointCompletedDistance = 5.0f
                },
                max_level = 3
            }.Construct(),
            VendorPaths.FromValleyOfTrials04(3),
            new RunAndCombat
            {
                path = GrindingPaths.ToValleyOfTrials04,
                max_level = 4,
            }.Construct(),
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = GrindingPaths.ValleyOfTrials04,
                    flags = WorldPathFlags.Circular,
                    pointCompletedDistance = 5.0f
                },
                max_level = 4
            }.Construct(),
            VendorPaths.FromValleyOfTrials04(4),
            new RunAndCombat
            {
                path = GrindingPaths.ToValleyOfTrials07,
                max_level = 7,
            }.Construct(),
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = GrindingPaths.ValleyOfTrials07,
                    flags = WorldPathFlags.Circular,
                    pointCompletedDistance = 5.0f
                },
                max_level = 7
            }.Construct(),
            VendorPaths.FromValleyOfTrials07(7),

            new RunAndCombat
            {
                path = new WorldPathData
                {
                    pointCompletedDistance = 3.0f,
                    points = new Vector2[]
                    {
                        new Vector2( -603.1f, -4263.7f ),
                        new Vector2( -597.7f, -4414.1f ),
                        new Vector2( -569.1f, -4480.1f ),
                        new Vector2( -589.3f, -4526.1f ),
                        new Vector2( -632.8f, -4537.1f ),
                        new Vector2( -632.3f, -4557.2f ),
                        new Vector2( -598.1f, -4576.2f ),
                        new Vector2( -601.3f, -4588.9f ),
                        new Vector2( -605.1f, -4603.8f ),
                        new Vector2( -604.5f, -4696.5f ),
                        new Vector2( -708.0f, -4784.4f ),
                        new Vector2( -772.1f, -4844.9f ),
                        new Vector2( -814.0f, -4878.0f ),
                        new Vector2( -822.2f, -4895.2f ),
                        new Vector2( -833.4f, -4910.4f ),
                    }
                },
                max_level = 8
            }.Construct(),

            // Senjin Village
            new GrindLocation
            {
                start_level = 7,
                to_vendor = new Vector2[]
                {
                    new Vector2( -833.6f, -4912.6f ),
                    new Vector2( -825.9f, -4925.8f ),
                    new Vector2( -823.3f, -4928.8f ),
                    new Vector2( -820.4f, -4929.8f ),
                    new Vector2( -816.9f, -4931.1f ),
                    new Vector2( -814.7f, -4931.9f ),
                    new Vector2( -811.8f, -4933.0f ),
                    new Vector2( -808.1f, -4934.4f ),
                    new Vector2( -804.8f, -4935.7f ),
                    new Vector2( -801.7f, -4936.9f ),
                    new Vector2( -773.3f, -4947.5f ),
                    new Vector2( -769.2f, -4948.5f ),
                },
                from_vendor_to_trainer = new Vector2[]
                {
                    new Vector2( -769.2f, -4948.5f ),
                    new Vector2( -773.9f, -4946.7f ),
                    new Vector2( -791.6f, -4940.3f ),
                    new Vector2( -802.7f, -4936.5f ),
                    new Vector2( -805.9f, -4935.4f ),
                    new Vector2( -808.0f, -4934.7f ),
                    new Vector2( -811.5f, -4933.3f ),
                    new Vector2( -814.3f, -4932.1f ),
                    new Vector2( -817.0f, -4931.0f ),
                    new Vector2( -819.4f, -4930.1f ),
                    new Vector2( -822.9f, -4928.6f ),
                    new Vector2( -832.8f, -4928.3f ),
                    new Vector2( -836.6f, -4932.2f ),
                    new Vector2( -839.1f, -4936.1f ),
                    new Vector2( -839.1f, -4938.2f ),
                    new Vector2( -839.3f, -4939.9f ),
                },
                to_origin = new Vector2[]
                {
                    new Vector2( -839.3f, -4939.9f ),
                    new Vector2( -839.5f, -4937.2f ),
                    new Vector2( -838.9f, -4935.4f ),
                    new Vector2( -836.9f, -4932.3f ),
                    new Vector2( -834.8f, -4929.2f ),
                    new Vector2( -833.7f, -4926.2f ),
                    new Vector2( -834.3f, -4913.8f ),
                },

                grind_locations = new LevelGrind[]
                {
                    // Shore
                    new LevelGrind
                    {
                        max_level = 9,
                        to_path = new Vector2[]
                        {
                            new Vector2( -832.1f, -4910.0f ),
                            new Vector2( -838.8f, -4922.0f ),
                            new Vector2( -845.0f, -4925.7f ),
                            new Vector2( -849.7f, -4928.5f ),
                            new Vector2( -867.1f, -4936.8f ),
                            new Vector2( -892.6f, -4946.9f ),
                            new Vector2( -926.0f, -4972.4f ),
                        },
                        from_path = new Vector2[]
                        {
                            new Vector2( -853.4f, -5029.1f ),
                            new Vector2( -866.4f, -4985.0f ),
                            new Vector2( -898.5f, -4957.5f ),
                            new Vector2( -869.7f, -4934.5f ),
                            new Vector2( -854.7f, -4932.4f ),
                            new Vector2( -849.1f, -4928.7f ),
                            new Vector2( -842.4f, -4923.4f ),
                            new Vector2( -835.7f, -4910.8f ),
                        },
                        grinding_path = new Vector2[]
                        {
                            new Vector2( -927.5f, -4974.8f ),
                            new Vector2( -919.4f, -5027.0f ),
                            new Vector2( -884.1f, -5043.3f ),
                            new Vector2( -830.3f, -5076.6f ),
                            new Vector2( -782.4f, -5106.2f ),
                            new Vector2( -737.5f, -5116.5f ),
                            new Vector2( -696.1f, -5125.5f ),
                            new Vector2( -671.1f, -5131.5f ),
                            new Vector2( -710.1f, -5125.8f ),
                            new Vector2( -771.2f, -5110.9f ),
                            new Vector2( -802.6f, -5103.3f ),
                            new Vector2( -837.4f, -5071.2f ),
                            new Vector2( -901.5f, -5035.2f ),
                            new Vector2( -943.3f, -5027.4f ),
                            new Vector2( -991.0f, -4973.5f ),
                            new Vector2( -974.9f, -4945.4f ),
                            new Vector2( -992.1f, -4928.2f ),
                            new Vector2( -1009.0f, -4897.2f ),
                            new Vector2( -1048.9f, -4889.1f ),
                            new Vector2( -1096.4f, -4844.4f ),
                            new Vector2( -1095.4f, -4817.1f ),
                            new Vector2( -1117.1f, -4804.4f ),
                            new Vector2( -1123.2f, -4764.5f ),
                            new Vector2( -1120.5f, -4729.8f ),
                            new Vector2( -1130.7f, -4696.7f ),
                            new Vector2( -1122.2f, -4731.4f ),
                            new Vector2( -1124.8f, -4774.0f ),
                            new Vector2( -1118.7f, -4802.4f ),
                            new Vector2( -1096.0f, -4815.2f ),
                            new Vector2( -1099.5f, -4838.3f ),
                            new Vector2( -1069.1f, -4867.0f ),
                            new Vector2( -1033.3f, -4893.6f ),
                            new Vector2( -996.3f, -4910.0f ),
                            new Vector2( -976.5f, -4944.4f ),
                            new Vector2( -985.8f, -4975.1f ),
                            new Vector2( -958.9f, -5015.5f ),
                            new Vector2( -927.4f, -5032.5f ),
                            new Vector2( -897.9f, -5033.8f ),
                            new Vector2( -853.9f, -5031.1f ),

                        }
                    },

                    // Islands
                    new LevelGrind
                    {
                        max_level = 11,
                        to_path = new Vector2[]
                        {
                            new Vector2( -831.6f, -4912.4f ),
                            new Vector2( -840.8f, -4921.6f ),
                            new Vector2( -846.6f, -4926.5f ),
                            new Vector2( -851.2f, -4930.4f ),
                            new Vector2( -882.8f, -4939.3f ),
                            new Vector2( -917.2f, -5002.0f ),
                            new Vector2( -908.2f, -5041.3f ),
                            new Vector2( -879.9f, -5129.0f ),
                            new Vector2( -865.1f, -5174.8f ),
                            new Vector2( -851.4f, -5217.2f ),
                            new Vector2( -834.4f, -5269.7f ),
                            new Vector2( -815.4f, -5328.6f ),
                            new Vector2( -805.8f, -5368.2f ),
                            new Vector2( -804.0f, -5396.6f ),
                            new Vector2( -796.3f, -5445.8f ),
                            new Vector2( -791.4f, -5498.3f ),
                            new Vector2( -792.1f, -5509.5f ),
                        },
                        from_path = new Vector2[]
                        {
                            new Vector2( -785.7f, -5507.8f ),
                            new Vector2( -810.0f, -5347.5f ),
                            new Vector2( -830.6f, -5309.5f ),
                            new Vector2( -868.9f, -5080.4f ),
                            new Vector2( -880.4f, -5040.1f ),
                            new Vector2( -871.5f, -5006.0f ),
                            new Vector2( -893.5f, -4965.7f ),
                            new Vector2( -879.7f, -4942.2f ),
                            new Vector2( -857.3f, -4932.9f ),
                            new Vector2( -847.7f, -4927.2f ),
                            new Vector2( -841.6f, -4922.4f ),
                            new Vector2( -832.6f, -4914.0f ),

                        },
                        grinding_path = new Vector2[]
                        {
                            new Vector2( -788.6f, -5510.8f ),
                            new Vector2( -819.0f, -5567.8f ),
                            new Vector2( -834.0f, -5609.3f ),
                            new Vector2( -834.7f, -5623.6f ),
                            new Vector2( -832.2f, -5662.5f ),
                            new Vector2( -784.4f, -5692.6f ),
                            new Vector2( -760.8f, -5711.4f ),
                            new Vector2( -743.3f, -5701.7f ),
                            new Vector2( -739.6f, -5671.7f ),
                            new Vector2( -767.6f, -5638.9f ),
                            new Vector2( -748.4f, -5654.1f ),
                            new Vector2( -722.5f, -5659.9f ),
                            new Vector2( -678.8f, -5661.2f ),
                            new Vector2( -639.5f, -5670.5f ),
                            new Vector2( -630.3f, -5615.1f ),
                            new Vector2( -639.4f, -5571.3f ),
                            new Vector2( -641.8f, -5539.9f ),
                            new Vector2( -672.9f, -5521.1f ),
                            new Vector2( -712.3f, -5520.2f ),
                            new Vector2( -742.1f, -5489.6f ),
                            new Vector2( -773.9f, -5500.7f ),
                            new Vector2( -788.7f, -5510.7f ),
                        }
                    }
                }
            }.Construct(),


            // Run to the Crossroads
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    pointCompletedDistance = 3.0f,
                    points = new Vector2[]
                    {
                        new Vector2( -833.1f, -4912.0f ),
                        new Vector2( -817.6f, -4885.4f ),
                        new Vector2( -806.3f, -4867.4f ),
                        new Vector2( -757.8f, -4830.2f ),
                        new Vector2( -714.7f, -4791.7f ),
                        new Vector2( -624.9f, -4716.3f ),
                        new Vector2( -598.4f, -4699.6f ),
                        new Vector2( -569.1f, -4726.9f ),
                        new Vector2( -510.4f, -4759.6f ),
                        new Vector2( -444.4f, -4778.1f ),
                        new Vector2( -369.7f, -4812.4f ),
                        new Vector2( -295.8f, -4800.6f ),
                        new Vector2( -265.9f, -4794.4f ),
                        new Vector2( -201.6f, -4775.2f ),
                        new Vector2( -164.2f, -4769.3f ),
                        new Vector2( -95.6f, -4765.2f ),
                        new Vector2( -57.7f, -4758.8f ),
                        new Vector2( 8.5f, -4744.2f ),
                        new Vector2( 52.5f, -4745.4f ),
                        new Vector2( 232.3f, -4738.4f ),
                        new Vector2( 267.9f, -4743.1f ),
                        new Vector2( 292.6f, -4742.8f ),
                        new Vector2( 301.9f, -4731.4f ),
                        new Vector2( 304.6f, -4713.2f ),
                        new Vector2( 298.5f, -4694.9f ),
                        new Vector2( 271.5f, -4659.5f ),
                        new Vector2( 247.2f, -4611.3f ),
                        new Vector2( 232.6f, -4560.6f ),
                        new Vector2( 246.9f, -4498.0f ),
                        new Vector2( 281.6f, -4417.5f ),
                        new Vector2( 327.3f, -4370.8f ),
                        new Vector2( 333.8f, -4355.5f ),
                        new Vector2( 333.0f, -4344.4f ),
                        new Vector2( 328.4f, -4332.4f ),
                        new Vector2( 325.4f, -4317.6f ),
                        new Vector2( 327.6f, -4295.8f ),
                        new Vector2( 328.7f, -4255.6f ),
                        new Vector2( 320.3f, -4223.4f ),
                        new Vector2( 299.9f, -4181.9f ),
                        new Vector2( 284.9f, -4148.4f ),
                        new Vector2( 285.9f, -4109.0f ),
                        new Vector2( 289.1f, -4066.8f ),
                        new Vector2( 292.2f, -4008.8f ),
                        new Vector2( 293.8f, -3979.7f ),
                        new Vector2( 288.7f, -3948.5f ),
                        new Vector2( 291.2f, -3928.3f ),
                        new Vector2( 311.3f, -3827.0f ),
                        new Vector2( 317.5f, -3804.0f ),
                        new Vector2( 317.2f, -3790.0f ),
                        new Vector2( 316.9f, -3778.9f ),
                        new Vector2( 315.8f, -3760.7f ),
                        new Vector2( 315.0f, -3744.0f ),
                        new Vector2( 313.9f, -3728.9f ),
                        new Vector2( 313.0f, -3716.4f ),
                        new Vector2( 314.1f, -3715.6f ),
                        new Vector2( 320.2f, -3615.8f ),
                        new Vector2( 315.9f, -3592.5f ),
                        new Vector2( 291.7f, -3558.1f ),
                        new Vector2( 225.3f, -3441.0f ),
                        new Vector2( 186.4f, -3393.5f ),
                        new Vector2( 179.7f, -3368.3f ),
                        new Vector2( 173.7f, -3297.5f ),
                        new Vector2( 142.3f, -3199.0f ),
                        new Vector2( 122.6f, -3140.9f ),
                        new Vector2( 117.5f, -3121.8f ),
                        new Vector2( 114.3f, -2989.8f ),
                        new Vector2( 95.9f, -2949.9f ),
                        new Vector2( 85.7f, -2881.3f ),
                        new Vector2( 81.0f, -2822.5f ),
                        new Vector2( 54.4f, -2752.9f ),
                        new Vector2( 46.9f, -2734.7f ),
                        new Vector2( -23.3f, -2712.4f ),
                        new Vector2( -77.6f, -2693.9f ),
                        new Vector2( -157.4f, -2687.1f ),
                        new Vector2( -228.9f, -2682.6f ),
                        new Vector2( -303.4f, -2680.4f ),
                        new Vector2( -358.0f, -2680.0f ),
                        new Vector2( -406.6f, -2679.6f ),
                        new Vector2( -441.2f, -2667.8f ),

                    }
                },
                max_level = 12
            }.Construct(),

            // Crossroads
            new GrindLocation
            {
                start_level = 11,
                to_vendor = new Vector2[]
                {
                    new Vector2( -440.8f, -2663.4f ),
                    new Vector2( -450.2f, -2655.6f ),
                    new Vector2( -471.1f, -2652.5f ),
                    new Vector2( -535.6f, -2654.3f ),
                    new Vector2( -543.2f, -2665.7f ),
                    new Vector2( -547.8f, -2670.2f ),
                    new Vector2( -550.8f, -2673.1f ),
                },
                to_origin = new Vector2[]
                {
                    new Vector2( -550.7f, -2672.9f ),
                    new Vector2( -546.1f, -2668.8f ),
                    new Vector2( -538.0f, -2657.0f ),
                    new Vector2( -502.9f, -2653.4f ),
                    new Vector2( -476.0f, -2653.1f ),
                    new Vector2( -457.3f, -2655.1f ),
                    new Vector2( -443.6f, -2658.4f ),
                    new Vector2( -439.7f, -2663.2f ),
                },
                grind_locations = new LevelGrind[]
                {
                    new LevelGrind
                    {
                        max_level = 15,
                        to_path = new Vector2[]
                        {
                            new Vector2( -440.6f, -2664.6f ),
                            new Vector2( -436.7f, -2652.3f ),
                            new Vector2( -441.1f, -2633.2f ),
                            new Vector2( -364.9f, -2544.4f ),
                            new Vector2( -356.2f, -2536.6f ),
                            new Vector2( -351.9f, -2525.1f ),
                            new Vector2( -350.6f, -2515.2f ),
                            new Vector2( -349.4f, -2408.5f ),
                            new Vector2( -341.1f, -2364.7f ),
                        },
                        from_path = new Vector2[]
                        {
                            new Vector2( -342.7f, -2362.9f ),
                            new Vector2( -350.3f, -2403.8f ),
                            new Vector2( -348.8f, -2510.8f ),
                            new Vector2( -350.9f, -2523.4f ),
                            new Vector2( -353.7f, -2533.3f ),
                            new Vector2( -367.4f, -2549.3f ),
                            new Vector2( -435.9f, -2625.3f ),
                            new Vector2( -439.1f, -2636.0f ),
                            new Vector2( -436.7f, -2652.6f ),
                            new Vector2( -439.7f, -2663.0f ),

                        },
                        grinding_path = new Vector2[]
                        {
                            new Vector2( -343.3f, -2363.2f ),
                            new Vector2( -266.2f, -2332.2f ),
                            new Vector2( -203.6f, -2309.6f ),
                            new Vector2( -179.9f, -2256.5f ),
                            new Vector2( -212.4f, -2187.6f ),
                            new Vector2( -251.9f, -2208.7f ),
                            new Vector2( -310.6f, -2261.8f ),
                            new Vector2( -369.6f, -2286.5f ),
                            new Vector2( -403.5f, -2318.0f ),
                            new Vector2( -444.2f, -2360.1f ),
                            new Vector2( -487.1f, -2414.8f ),
                            new Vector2( -543.3f, -2460.9f ),
                            new Vector2( -604.3f, -2478.9f ),
                            new Vector2( -644.8f, -2529.4f ),
                            new Vector2( -612.6f, -2572.3f ),
                            new Vector2( -568.4f, -2578.2f ),
                            new Vector2( -546.9f, -2557.9f ),
                            new Vector2( -491.9f, -2522.0f ),
                            new Vector2( -457.8f, -2491.1f ),
                            new Vector2( -431.6f, -2469.3f ),
                            new Vector2( -378.5f, -2437.0f ),
                            new Vector2( -364.4f, -2390.8f ),
                            new Vector2( -341.9f, -2363.8f ),
                        }
                    }
                }
            }.Construct(),
            
            // Now in Crossroads, time to do some more leveling
            new GrindLocation
            {
                start_level = 15,
                to_vendor = new Vector2[]
                {
                    new Vector2( -440.8f, -2663.4f ),
                    new Vector2( -450.2f, -2655.6f ),
                    new Vector2( -471.1f, -2652.5f ),
                    new Vector2( -535.6f, -2654.3f ),
                    new Vector2( -543.2f, -2665.7f ),
                    new Vector2( -547.8f, -2670.2f ),
                    new Vector2( -550.8f, -2673.1f ),
                },
                to_origin = new Vector2[]
                {
                    new Vector2( -550.7f, -2672.9f ),
                    new Vector2( -546.1f, -2668.8f ),
                    new Vector2( -538.0f, -2657.0f ),
                    new Vector2( -502.9f, -2653.4f ),
                    new Vector2( -476.0f, -2653.1f ),
                    new Vector2( -457.3f, -2655.1f ),
                    new Vector2( -443.6f, -2658.4f ),
                    new Vector2( -439.7f, -2663.2f ),
                },
                grind_locations = new LevelGrind[]
                {
                    new LevelGrind
                    {
                        max_level = 17,
                        from_path = new Vector2[]
                        {
                            new Vector2( -451.2f, -1916.8f ),
                            new Vector2( -322.8f, -2014.9f ),
                            new Vector2( -281.1f, -2079.3f ),
                            new Vector2( -276.0f, -2245.6f ),
                            new Vector2( -344.2f, -2370.9f ),
                            new Vector2( -349.4f, -2513.8f ),
                            new Vector2( -351.5f, -2524.1f ),
                            new Vector2( -354.4f, -2534.1f ),
                            new Vector2( -413.2f, -2601.4f ),
                            new Vector2( -437.8f, -2629.1f ),
                            new Vector2( -438.4f, -2640.5f ),
                            new Vector2( -436.0f, -2652.3f ),
                            new Vector2( -439.4f, -2663.8f ),
                        },
                        to_path = new Vector2[]
                        {
                            new Vector2( -439.4f, -2663.8f ),
                            new Vector2( -437.7f, -2653.0f ),
                            new Vector2( -440.4f, -2639.2f ),
                            new Vector2( -436.5f, -2626.5f ),
                            new Vector2( -356.8f, -2536.7f ),
                            new Vector2( -354.0f, -2532.5f ),
                            new Vector2( -352.0f, -2526.4f ),
                            new Vector2( -350.5f, -2516.7f ),
                            new Vector2( -346.0f, -2384.2f ),
                            new Vector2( -291.7f, -2297.8f ),
                            new Vector2( -283.2f, -2204.8f ),
                            new Vector2( -281.8f, -2083.9f ),
                            new Vector2( -369.9f, -1960.4f ),
                            new Vector2( -410.6f, -1926.0f ),
                            new Vector2( -453.1f, -1918.9f ),
                        },
                        grinding_path = new Vector2[]
                        {
                            new Vector2( -453.1f, -1920.2f ),
                            new Vector2( -507.7f, -1937.6f ),
                            new Vector2( -555.6f, -1961.5f ),
                            new Vector2( -626.9f, -1940.0f ),
                            new Vector2( -695.6f, -1938.0f ),
                            new Vector2( -752.4f, -1927.3f ),
                            new Vector2( -791.1f, -1926.9f ),
                            new Vector2( -790.8f, -1891.3f ),
                            new Vector2( -760.8f, -1886.8f ),
                            new Vector2( -694.3f, -1890.2f ),
                            new Vector2( -672.0f, -1890.7f ),
                            new Vector2( -557.5f, -1915.6f ),
                            new Vector2( -549.9f, -1842.7f ),
                            new Vector2( -543.3f, -1801.1f ),
                            new Vector2( -564.5f, -1710.3f ),
                            new Vector2( -556.0f, -1684.1f ),
                            new Vector2( -524.7f, -1662.3f ),
                            new Vector2( -507.6f, -1647.5f ),
                            new Vector2( -476.4f, -1671.5f ),
                            new Vector2( -443.7f, -1726.1f ),
                            new Vector2( -424.7f, -1767.5f ),
                            new Vector2( -448.1f, -1805.9f ),
                            new Vector2( -431.4f, -1854.5f ),
                            new Vector2( -434.1f, -1904.9f ),
                            new Vector2( -451.2f, -1916.8f ),
                        }
                    }
                }
            }.Construct(),

            // From Crossroads to Sunrock Retreat
            new RunAndCombat
            {
                max_level = 18,
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    points = new Vector2[]
                    {
                        new Vector2( -436.4f, -2662.6f ),
                        new Vector2( -438.0f, -2642.5f ),
                        new Vector2( -439.5f, -2628.7f ),
                        new Vector2( -354.8f, -2533.6f ),
                        new Vector2( -350.6f, -2522.0f ),
                        new Vector2( -336.8f, -2365.2f ),
                        new Vector2( -284.1f, -2254.6f ),
                        new Vector2( -282.8f, -2084.2f ),
                        new Vector2( -405.2f, -1937.4f ),
                        new Vector2( -451.1f, -1803.4f ),
                        new Vector2( -524.1f, -1652.7f ),
                        new Vector2( -526.5f, -1574.6f ),
                        new Vector2( -508.6f, -1534.9f ),
                        new Vector2( -460.1f, -1457.1f ),
                        new Vector2( -394.8f, -1373.5f ),
                        new Vector2( -379.5f, -1323.2f ),
                        new Vector2( -263.8f, -1148.8f ),
                        new Vector2( -243.4f, -997.0f ),
                        new Vector2( -242.2f, -986.8f ),
                        new Vector2( -260.8f, -848.0f ),
                        new Vector2( -225.9f, -767.1f ),
                        new Vector2( -198.7f, -707.7f ),
                        new Vector2( -173.8f, -689.9f ),
                        new Vector2( -121.0f, -686.7f ),
                        new Vector2( -100.7f, -673.2f ),
                        new Vector2( -57.3f, -673.5f ),
                        new Vector2( -12.9f, -638.5f ),
                        new Vector2( -0.5f, -595.4f ),
                        new Vector2( -5.1f, -579.8f ),
                        new Vector2( -27.7f, -556.6f ),
                        new Vector2( -58.3f, -478.4f ),
                        new Vector2( -61.0f, -424.8f ),
                        new Vector2( -28.2f, -328.4f ),
                        new Vector2( -33.5f, -277.2f ),
                        new Vector2( -12.9f, -250.9f ),
                        new Vector2( 8.1f, -228.8f ),
                        new Vector2( 14.8f, -189.8f ),
                        new Vector2( 74.6f, -133.1f ),
                        new Vector2( 119.4f, -33.6f ),
                        new Vector2( 152.6f, 43.6f ),
                        new Vector2( 156.9f, 119.8f ),
                        new Vector2( 187.4f, 153.6f ),
                        new Vector2( 190.0f, 191.3f ),
                        new Vector2( 214.4f, 224.3f ),
                        new Vector2( 226.6f, 251.8f ),
                        new Vector2( 261.4f, 301.3f ),
                        new Vector2( 295.1f, 314.8f ),
                        new Vector2( 387.6f, 314.5f ),
                        new Vector2( 425.9f, 342.3f ),
                        new Vector2( 462.9f, 345.9f ),
                        new Vector2( 544.9f, 317.3f ),
                        new Vector2( 588.6f, 328.7f ),
                        new Vector2( 629.0f, 365.1f ),
                        new Vector2( 703.5f, 386.9f ),
                        new Vector2( 749.5f, 419.2f ),
                        new Vector2( 818.2f, 485.4f ),
                        new Vector2( 823.8f, 522.6f ),
                        new Vector2( 846.0f, 554.0f ),
                        new Vector2( 859.8f, 588.5f ),
                        new Vector2( 912.9f, 659.7f ),
                        new Vector2( 928.9f, 714.5f ),
                        new Vector2( 932.8f, 802.1f ),
                        new Vector2( 933.1f, 871.9f ),
                        new Vector2( 935.2f, 891.9f ),
                        new Vector2( 934.1f, 901.5f ),
                    }
                }
            }.Construct(),

            // At Sunrock Retreat
            new GrindLocation
            {
                start_level = 17,
                to_vendor = new Vector2[]
                {
                    new Vector2( 931.8f, 901.3f ),
                    new Vector2( 947.0f, 925.7f ),
                    new Vector2( 961.9f, 941.8f ),
                    new Vector2( 982.2f, 972.4f ),
                    new Vector2( 988.5f, 981.9f ),
                    new Vector2( 1006.3f, 1005.3f ),
                },
                to_origin = new Vector2[]
                {
                    new Vector2( 1006.3f, 1005.3f ),
                    new Vector2( 990.4f, 983.6f ),
                    new Vector2( 984.1f, 975.0f ),
                    new Vector2( 970.7f, 951.4f ),
                    new Vector2( 940.7f, 916.4f ),
                    new Vector2( 934.0f, 901.5f ),
                },
                grind_locations = new LevelGrind[]
                {
                    new LevelGrind
                    {
                        max_level = 20,
                        to_path = new Vector2[]
                        {
                            new Vector2( 934.8f, 900.1f ),
                            new Vector2( 928.1f, 842.0f ),
                            new Vector2( 933.5f, 756.9f ),
                            new Vector2( 915.9f, 677.5f ),
                        },
                        from_path = new Vector2[]
                        {
                            new Vector2( 914.2f, 672.4f ),
                            new Vector2( 927.3f, 711.3f ),
                            new Vector2( 932.5f, 795.7f ),
                            new Vector2( 928.9f, 850.8f ),
                            new Vector2( 936.2f, 905.2f ),
                        },
                        grinding_path = new Vector2[]
                        {
                            new Vector2( 915.0f, 670.6f ),
                            new Vector2( 928.4f, 649.0f ),
                            new Vector2( 921.4f, 629.7f ),
                            new Vector2( 904.0f, 601.9f ),
                            new Vector2( 869.5f, 559.2f ),
                            new Vector2( 832.3f, 523.8f ),
                            new Vector2( 826.5f, 465.1f ),
                            new Vector2( 884.3f, 444.0f ),
                            new Vector2( 827.9f, 467.1f ),
                            new Vector2( 775.4f, 421.5f ),
                            new Vector2( 746.2f, 414.1f ),
                            new Vector2( 676.1f, 379.8f ),
                            new Vector2( 621.4f, 364.1f ),
                            new Vector2( 583.4f, 327.9f ),
                            new Vector2( 554.0f, 309.0f ),
                            new Vector2( 529.6f, 311.3f ),
                            new Vector2( 520.3f, 310.4f ),
                            new Vector2( 506.5f, 308.2f ),
                            new Vector2( 484.1f, 308.3f ),
                            new Vector2( 451.3f, 304.6f ),
                            new Vector2( 378.5f, 285.9f ),
                            new Vector2( 334.6f, 332.5f ),
                            new Vector2( 370.2f, 340.8f ),
                            new Vector2( 384.8f, 362.8f ),
                            new Vector2( 420.7f, 357.1f ),
                            new Vector2( 477.1f, 351.4f ),
                            new Vector2( 534.4f, 356.6f ),
                            new Vector2( 577.9f, 361.0f ),
                            new Vector2( 600.6f, 383.7f ),
                            new Vector2( 637.6f, 391.6f ),
                            new Vector2( 649.0f, 401.7f ),
                            new Vector2( 691.8f, 412.3f ),
                            new Vector2( 724.9f, 428.7f ),
                            new Vector2( 737.9f, 441.4f ),
                            new Vector2( 782.4f, 481.4f ),
                            new Vector2( 797.1f, 496.7f ),
                            new Vector2( 802.1f, 514.1f ),
                            new Vector2( 826.3f, 565.7f ),
                            new Vector2( 836.5f, 577.2f ),
                            new Vector2( 848.5f, 604.8f ),
                            new Vector2( 868.4f, 626.8f ),
                            new Vector2( 873.8f, 647.9f ),
                            new Vector2( 880.0f, 656.9f ),
                            new Vector2( 900.9f, 674.8f ),
                            new Vector2( 912.2f, 684.2f ),
                        }
                    }
                }
            }.Construct(),

            // At Sunrock Retreat to Camp Taurajo
            new RunAndCombat
            {
                min_level = 20,
                max_level = 21,
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    points = new Vector2[]
                    {
                        new Vector2( 932.6f, 900.6f ),
                        new Vector2( 928.6f, 808.1f ),
                        new Vector2( 930.8f, 706.0f ),
                        new Vector2( 827.5f, 529.7f ),
                        new Vector2( 822.4f, 490.6f ),
                        new Vector2( 721.5f, 394.2f ),
                        new Vector2( 629.2f, 366.1f ),
                        new Vector2( 596.9f, 339.6f ),
                        new Vector2( 601.5f, 308.1f ),
                        new Vector2( 587.9f, 297.7f ),
                        new Vector2( 513.6f, 330.4f ),
                        new Vector2( 440.0f, 345.6f ),
                        new Vector2( 399.1f, 322.1f ),
                        new Vector2( 274.8f, 311.8f ),
                        new Vector2( 224.6f, 254.4f ),
                        new Vector2( 217.2f, 229.0f ),
                        new Vector2( 190.2f, 195.6f ),
                        new Vector2( 184.4f, 158.3f ),
                        new Vector2( 156.0f, 111.9f ),
                        new Vector2( 154.6f, 43.6f ),
                        new Vector2( 61.9f, -146.8f ),
                        new Vector2( 33.2f, -165.7f ),
                        new Vector2( 12.9f, -197.1f ),
                        new Vector2( 11.6f, -229.5f ),
                        new Vector2( -29.9f, -260.5f ),
                        new Vector2( -27.8f, -331.2f ),
                        new Vector2( -59.7f, -416.2f ),
                        new Vector2( -60.4f, -473.2f ),
                        new Vector2( -26.8f, -551.2f ),
                        new Vector2( -6.9f, -574.3f ),
                        new Vector2( -2.1f, -588.1f ),
                        new Vector2( -3.3f, -603.0f ),
                        new Vector2( -15.7f, -639.7f ),
                        new Vector2( -60.8f, -671.6f ),
                        new Vector2( -77.8f, -673.3f ),
                        new Vector2( -98.7f, -674.1f ),
                        new Vector2( -122.9f, -684.6f ),
                        new Vector2( -167.3f, -685.2f ),
                        new Vector2( -203.1f, -713.8f ),
                        new Vector2( -253.1f, -829.8f ),
                        new Vector2( -249.3f, -944.5f ),
                        new Vector2( -240.4f, -991.1f ),
                        new Vector2( -277.8f, -1180.6f ),
                        new Vector2( -374.2f, -1313.2f ),
                        new Vector2( -389.5f, -1364.8f ),
                        new Vector2( -396.9f, -1375.9f ),
                        new Vector2( -406.9f, -1389.0f ),
                        new Vector2( -428.7f, -1403.2f ),
                        new Vector2( -524.0f, -1561.9f ),
                        new Vector2( -526.3f, -1576.8f ),
                        new Vector2( -417.3f, -1893.4f ),
                        new Vector2( -285.7f, -2090.8f ),
                        new Vector2( -279.0f, -2257.7f ),
                        new Vector2( -334.3f, -2362.4f ),
                        new Vector2( -545.7f, -2473.7f ),
                        new Vector2( -618.6f, -2494.9f ),
                        new Vector2( -761.0f, -2586.3f ),
                        new Vector2( -861.5f, -2530.6f ),
                        new Vector2( -888.2f, -2499.1f ),
                        new Vector2( -1113.9f, -2454.0f ),
                        new Vector2( -1267.2f, -2457.4f ),
                        new Vector2( -1459.3f, -2539.3f ),
                        new Vector2( -1569.1f, -2528.2f ),
                        new Vector2( -1628.2f, -2519.5f ),
                        new Vector2( -1652.5f, -2529.6f ),
                        new Vector2( -1667.1f, -2536.7f ),
                        new Vector2( -1685.2f, -2545.4f ),
                        new Vector2( -1704.1f, -2554.6f ),
                        new Vector2( -1782.9f, -2539.0f ),
                        new Vector2( -1865.4f, -2457.1f ),
                        new Vector2( -1911.3f, -2387.9f ),
                        new Vector2( -2028.7f, -2265.7f ),
                        new Vector2( -2181.1f, -2188.3f ),
                        new Vector2( -2260.1f, -2181.9f ),
                        new Vector2( -2331.8f, -1974.7f ),
                        new Vector2( -2342.2f, -1941.2f ),
                        new Vector2( -2353.8f, -1929.6f ),
                        new Vector2( -2369.2f, -1895.7f ),
                        new Vector2( -2383.6f, -1881.1f ),
                    }
                }
            }.Construct(),

            // At Camp Taurajo get flightpath
            new LookAtCommand(new Vector2( -2383.6f, -1881.1f )),
            new ClickCenterCommand(MouseButton.Right),

            // Now lets finish our journey to Thunder Bluff
            new RunAndCombat
            {
                min_level = 20,
                max_level = 21,
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    points = new Vector2[]
                    {
                        new Vector2( -2384.0f, -1880.8f ),
                        new Vector2( -2347.1f, -1866.7f ),
                        new Vector2( -2357.1f, -1452.1f ),
                        new Vector2( -2394.8f, -1355.8f ),
                        new Vector2( -2416.0f, -1281.8f ),
                        new Vector2( -2440.2f, -1201.2f ),
                        new Vector2( -2411.9f, -1016.2f ),
                        new Vector2( -2249.0f, -689.6f ),
                        new Vector2( -2244.8f, -611.6f ),
                        new Vector2( -2158.9f, -496.1f ),
                        new Vector2( -2148.1f, -471.7f ),
                        new Vector2( -2106.9f, -429.3f ),
                        new Vector2( -2097.9f, -417.6f ),
                        new Vector2( -2052.6f, -355.3f ),
                        new Vector2( -2041.5f, -335.0f ),
                        new Vector2( -1996.2f, -259.0f ),
                        new Vector2( -1936.4f, -157.6f ),
                        new Vector2( -1763.9f, -34.7f ),
                        new Vector2( -1602.4f, -2.4f ),
                        new Vector2( -1526.8f, 2.8f ),
                        new Vector2( -1447.9f, 64.5f ),
                        new Vector2( -1435.4f, 75.8f ),
                        new Vector2( -1423.8f, 86.5f ),
                        new Vector2( -1416.4f, 94.3f ),
                        new Vector2( -1394.1f, 128.2f ),
                        new Vector2(-1349.5f, 171.1f),
                        new Vector2(-1336.3f, 177.1f),
                        new Vector2(-1318.0f, 181.4f),
                    }
                }
            }.Construct()
        });
    }
}
