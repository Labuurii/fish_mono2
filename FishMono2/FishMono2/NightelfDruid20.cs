﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class NightelfDruid20
    {
        public static readonly CommandList Command = new CommandList(new ICommand[]
        {
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = new Vector2[]
                    {
                        new Vector2( 10334.6f, 841.1f ),
                        new Vector2( 10319.0f, 868.5f ),
                        new Vector2( 10293.1f, 848.7f ),
                        new Vector2( 10280.8f, 808.7f ),
                        new Vector2( 10289.6f, 769.4f ),
                        new Vector2( 10277.1f, 733.9f ),
                        new Vector2( 10308.6f, 724.8f ),
                    },
                    flags = WorldPathFlags.Default
                },
                max_level = 3
            }.Construct(),

            new RunAndCombat
            {
                path = new WorldPathData {
                    points = GrindingPaths.ShadowGlen04Start,
                    flags = WorldPathFlags.Circular
                },
                max_level = 4
            }.Construct(),

            new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = GrindingPaths.ShadowGlen04Start,
                    flags = WorldPathFlags.AvoidCombat
                },
                max_level = 5
            }.Construct(),

            new VendorAllItems
            {
                to_vendor_path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    points = new Vector2[]
                    {
                        new Vector2( 10351.1f, 724.7f ),
                        new Vector2( 10382.5f, 728.1f ),
                        new Vector2( 10385.0f, 752.0f ),
                        new Vector2( 10401.6f, 766.1f ),
                        new Vector2( 10431.6f, 786.1f ),
                        new Vector2( 10436.0f, 793.7f ),
                        new Vector2( 10436.7f, 794.8f )
                    },
                },
                back_path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    points = new Vector2[]
                    {
                        new Vector2( 10424.5f, 784.2f ),
                        new Vector2( 10402.3f, 769.0f ),
                        new Vector2( 10389.5f, 754.7f ),
                        new Vector2( 10383.1f, 735.1f ),
                        new Vector2( 10370.5f, 722.5f ),
                        new Vector2( 10352.0f, 724.1f ),
                    }
                },
                level = 4,
                vendor_pos = new Vector2( 10436.7f, 794.8f )
            }.Construct(),

            new LearnAllSpells
            {
                to_trainer_path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    stopAndTurnDistance = 50.0f,
                    points = new Vector2[]
                    {
                        new Vector2( 10334.3f, 711.4f ),
                        new Vector2( 10345.6f, 736.9f ),
                        new Vector2( 10349.3f, 753.3f ),
                        new Vector2( 10351.9f, 766.2f ),
                        new Vector2( 10356.2f, 780.2f ),
                        new Vector2( 10379.0f, 838.3f ),
                        new Vector2( 10400.2f, 883.7f ),
                        new Vector2( 10415.1f, 885.1f ),
                        new Vector2( 10429.4f, 877.6f ),
                        new Vector2( 10441.2f, 869.9f ),
                        new Vector2( 10458.7f, 861.2f ),
                        new Vector2( 10472.1f, 857.6f ),
                        new Vector2( 10485.1f, 856.4f ),
                        new Vector2( 10501.8f, 852.0f ),
                        new Vector2( 10514.2f, 840.4f ),
                        new Vector2( 10522.0f, 822.4f ),
                        new Vector2( 10512.1f, 820.7f ),
                        new Vector2( 10505.7f, 832.4f ),
                        new Vector2( 10494.6f, 843.3f ),
                        new Vector2( 10480.7f, 847.3f ),
                        new Vector2( 10467.0f, 848.1f ),
                        new Vector2( 10451.3f, 843.6f ),
                        new Vector2( 10447.2f, 837.4f ),
                        new Vector2( 10449.4f, 835.5f ),
                        new Vector2( 10461.9f, 828.5f ),
                        new Vector2( 10464.0f, 829.5f ),
                    }
                },

                back_path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat,
                    stopAndTurnDistance = 50.0f,
                    points = new Vector2[]
                    {
                        new Vector2( 10461.3f, 828.9f ),
                        new Vector2( 10453.9f, 833.1f ),
                        new Vector2( 10447.7f, 836.8f ),
                        new Vector2( 10452.1f, 844.4f ),
                        new Vector2( 10465.5f, 848.1f ),
                        new Vector2( 10480.0f, 847.4f ),
                        new Vector2( 10491.4f, 844.2f ),
                        new Vector2( 10501.8f, 837.5f ),
                        new Vector2( 10508.0f, 829.3f ),
                        new Vector2( 10511.5f, 820.6f ),
                        new Vector2( 10520.9f, 821.8f ),
                        new Vector2( 10520.8f, 828.3f ),
                        new Vector2( 10515.8f, 839.3f ),
                        new Vector2( 10507.5f, 848.4f ),
                        new Vector2( 10498.1f, 854.2f ),
                        new Vector2( 10490.2f, 855.5f ),
                        new Vector2( 10476.0f, 857.5f ),
                        new Vector2( 10463.6f, 858.9f ),
                        new Vector2( 10453.4f, 863.6f ),
                        new Vector2( 10441.9f, 871.1f ),
                        new Vector2( 10428.6f, 879.3f ),
                        new Vector2( 10417.8f, 886.0f ),
                        new Vector2( 10401.4f, 886.6f ),
                        new Vector2( 10382.5f, 850.6f ),
                        new Vector2( 10370.5f, 812.6f ),
                        new Vector2( 10357.1f, 776.0f ),
                        new Vector2( 10351.4f, 762.3f ),
                        new Vector2( 10347.1f, 747.1f ),
                        new Vector2( 10345.9f, 725.3f ),
                    }
                },

                trainer_pos = new Vector2( 10464.0f, 829.5f ),
                level = 4

            }.Construct(),
    });
    }
}
