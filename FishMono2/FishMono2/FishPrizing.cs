﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class FishPrizing
    {
        // In silver
        public static readonly int[] DefaultPrizes = new int[(int)Fish.COUNT];

        private static void set_default(Fish fish, int prize_silver)
        {
            DefaultPrizes[(int)fish] = prize_silver;
        }

        static FishPrizing()
        {
            set_default(Fish.OilyBlackmouth, 35);
            set_default(Fish.FirefinSnapper, 45);
            set_default(Fish.WinterSquid, 100);
            set_default(Fish.WinterSquid, 100);
            set_default(Fish.WinterSquid, 150);
            set_default(Fish.NightfinSnapper, 100);
            set_default(Fish.BigMouthClam, 0);
            set_default(Fish.StoneScaleEel, 25);
            set_default(Fish.GoldenPearl, 300);
            set_default(Fish.SuperiorHealingPotion, 30);
        }

        public static InventoryState CalculateCurrentMedianPrize(List<int>[] listings)
        {
            if(listings.Length != (int)Fish.COUNT)
            {
                throw new Exception("Listings size does not match the fish count");
            }

            var result = new InventoryState();
            for(var i = 0; i < (int)Fish.COUNT; ++i)
            {
                var fish_listings = listings[i];
                var median = MathOps.Median(fish_listings);
                result.fishes[i] = median;
            }

            return result;
        }
    }
}
