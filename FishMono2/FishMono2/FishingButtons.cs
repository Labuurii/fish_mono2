﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class FishingButtons
    {
        public const int Fish = '1';
        public const int Cleanup = '2';
        public const int StopCasting = '3';
        public const int BrightBaubles = '4';
        public const int UnequipPole = '5';
        public const int EquipPole = '6';
        public const int OpenClam = '7';
    }
}
