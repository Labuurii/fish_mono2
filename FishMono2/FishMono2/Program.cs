﻿using Master;
using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FishMono2
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Args.Init(args);
            Environment.SetEnvironmentVariable("TESSDATA_PREFIX", Path.Combine(Directory.GetCurrentDirectory(), "tessdata"));

            if(!Args.Instance.Highjack)
            {
                var processes = WoWProcessOps.GetWoWProcesses();
                foreach (var process in processes) process.Kill();
            }

            var cleanup_jobs = new ComputerCleanup();
            using(var runtime = new Runtime())
            {
                var slave = new SlaveImpl(runtime);

                var master = new Master.Master(Args.Instance.GetMasterFlags(), slave);
                runtime.bot_context.master = master;
                var frame_rate = new FrameRate();
                frame_rate.frame_length = 1.0f / 5.0f;
                DeadlockDetector deadlock_detector = null;
                if(!Args.Instance.DisableDeadLockCheck)
                {
                    deadlock_detector = new DeadlockDetector();
                }

                master.PreLoop();
                for (; ; )
                {
                    if (runtime.should_shut_down)
                    {
                        break;
                    }

                    if(deadlock_detector != null)
                    {
                        deadlock_detector.Update();
                    }
                    runtime.Update();
                    master.Update();
                    cleanup_jobs.Update();
                    frame_rate.Sync();
                    if (Args.Instance.PrintFPS)
                    {
                        Console.WriteLine("FPS: " + (1.0f / frame_rate.delta_time).ToString());
                    }
                }
            }
        }
    }
}
