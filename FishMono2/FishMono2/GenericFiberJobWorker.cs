﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class GenericFiberJobWorker
    {
        readonly FiberExecutor exe = new FiberExecutor();
        readonly Queue<IEnumerator> queue = new Queue<IEnumerator>();

        public GenericFiberJobWorker()
        {
            exe.SetEnumerator(Run());
        }

        private IEnumerator Run()
        {
            for(; ; )
            {
                while(queue.Count > 0)
                {
                    var job = queue.Dequeue();
                    yield return job;
                }
                yield return null;
            }
        }

        public void Update()
        {
            if(!exe.Step())
            {
                throw new Exception("GenericFiberJobWorker has stopped");
            }
        }

        public void Add(IEnumerator job)
        {
            queue.Enqueue(job);
        }
    }
}
