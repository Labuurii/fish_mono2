﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ListedPrizeCount : ITransmittable
    {
        public int prize_silver;
        public int count;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out prize_silver)
                .Parse(out count);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            throw new NotImplementedException();
        }
    }

    public class AHListingsData : ITransmittable
    {
        public int page;
        public ListedPrizeCount[] listed_prize_counts;

        public void Parse(StringArrayParseState parse_state)
        {
            int list_count;
            parse_state
                .Parse(out page)
                .Parse(out list_count);

            listed_prize_counts = new ListedPrizeCount[list_count];
            for(int i = 0; i < list_count; ++i)
            {
                var prize_count = listed_prize_counts[i] = new ListedPrizeCount();
                prize_count.Parse(parse_state);
            }
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.EnterStringInChat("/run t_cahl(" + page.ToString() + ")");
        }
    }
}
