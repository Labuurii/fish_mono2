﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class BotOps
    {
        private const float TakeOutMailboxWaitTime = 40;

        public static IEnumerator StartTest(WoWInstance instance, Window window)
        {
            yield return instance.SetDefaultWindow(window);
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);

            yield return instance.window.ClickButton(FishingButtons.UnequipPole);
            yield return instance.window.ClickButton(FishingButtons.UnequipPole);
            var merchant_data = new MerchantData();
            yield return ThunderBluffMailBoxToFishingVendor.Perform(instance, merchant_data);
            if (merchant_data.merchant_window_open == 0)
            {
                throw new Exception("Could not open fishing vendor merchant data");
            }

            yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
            yield return FiberOps.WaitFor(2);
            yield return instance.window.ClickButton(BuiltinWoWButtons.BuyBrightBaubles);
            yield return FiberOps.WaitFor(8);
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
        }

        public static IEnumerator RunCombat(WoWInstance instance, RefValue<bool> engaged_in_combat)
        {
            var c = instance.account.@class;
            if(c == "MAGE")
            {
                yield return MageOps.RunCombat(instance, engaged_in_combat);
            } else
            {
                throw new Exception("Does not know how to run combat with class " + c);
            }
        }

        public static IEnumerator RunLeveling(WoWInstance instance)
        {
            instance.is_leveling = true;
            instance.should_terminate_on_stop = false;
            yield return instance.window.ClickButton(BuiltinWoWButtons.PlaceSpells);
            
            var @class = instance.account.@class;
            if(@class == "MAGE")
            {
                yield return MageOps.CreateCombatResourcesIfNecessary(instance);
                yield return TrollMage20.command.Run(instance, instance.context);
            } else
            {
                throw new Exception("Does not know how to level with class " + @class);
            }
        }

        public static IEnumerator RunDumbFishing(WoWInstance instance)
        {
            instance.is_leveling = true;
            yield return new FishingCommand(true, 10 * 60 * 60 /* 10 hours */).Start(instance, instance.context);
        }

        public static IEnumerator RunClassicLevelingKalimdor(WoWInstance instance, Window window)
        {
            if (instance.account.current_level < 20 || instance.account.max_fishing == 0)
            {
                yield return RunLeveling(instance);
            } else if(instance.account.max_fishing > 0 && instance.account.max_fishing < 225)
            {
                yield return RunDumbFishing(instance);
                while (true) { yield return null; }
            }
        }

        public static IEnumerator RunBot(WoWInstance instance, Window window)
        {
            switch(instance.account.job)
            {
                case WoWJob.None:
                    break;
                default:
                    throw new Exception("Unhandled enum value " + instance.account.job);
            }

            switch (instance.account.primary_wow_job)
            {
                case PrimaryWoWJob.ClassicLevelingKalimdor:
                    yield return RunClassicLevelingKalimdor(instance, window);
                    break;
                case PrimaryWoWJob.ClassicFishingKalimdor:
                    yield return new ClassicKalimdorFishingCommand().Start(instance, instance.context);
                    break;
                default:
                    throw new Exception("Unhandled enum value " + instance.account.primary_wow_job);
            }
        }

        public static IEnumerator StartWoWAndLogin(WoWInstance instance, RefValue<Window> window)
        {
            var should_start_wow = window.value == null;

            if (should_start_wow)
            {
                var ref_window = new RefValue<Window>();
                yield return StartWoW(instance, ref_window);
                window.value = ref_window.value;

                yield return instance.SetDefaultWindow(window.value);
                yield return FiberOps.WaitFor(5);
                yield return Login(instance, window.value);

                if (Args.Instance.CaptchaRequired)
                {
                    instance.Log("Captcha now required. Waits for the user to accept the captcha");
                    window.value.SetArea(new Rect
                    {
                        Right = 1200,
                        Bottom = 800
                    });
                    instance.is_paused = true;
                    yield return null;
                }

            } else
            {
                yield return instance.SetDefaultWindow(window.value);
            }

            // Enter character
            yield return FiberOps.WaitFor(20);
            yield return instance.window.ClickButton(Win32.VK.Return);
            yield return FiberOps.WaitFor(5);

            yield return instance.text_reader.Request(new TaxiData()); // Only because the data is small and wont interfere with the UI
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
            yield return instance.text_reader.Request(instance.account);

            if(!should_start_wow)
            {
                yield return window.value.ClickButton(BuiltinWoWButtons.ShowUI);
                yield return window.value.EnterStringInChat("/reload");
                yield return FiberOps.WaitFor(5);
                yield return instance.text_reader.Request(new TaxiData());
                yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
            }

            yield return window.value.ClickButton(TransmitButtons.ResetTransmitId);
            yield return window.value.ClickButton(BuiltinWoWButtons.HideScriptErrors);
        }

        public static IEnumerator Start(WoWInstance instance, Window window)
        {
            var window_ref = new RefValue<Window>();
            window_ref.value = window;
            yield return StartWoWAndLogin(instance, window_ref);
            yield return RunBot(instance, window_ref.value);
        }

        public static IEnumerator StartWoW(WoWInstance instance, RefValue<Window> window)
        {
            var future = instance.context.start_wow_worker.Add(instance);
            while(future.window == null)
            {
                yield return null;
            }
            window.value = future.window;
            var area = window.value.GetArea();
            area.PlaceAt(instance.context.screen.GetSize());
            window.value.SetArea(area);
            while (window.value.IsNotResponding())
            {
                yield return null;
            }         
        }

        public static IEnumerator Login(WoWInstance instance, Window window)
        {
            yield return window.EnterString(instance.account.email);
            yield return window.ClickButton(Win32.VK.Tab);
            yield return window.EnterString("Bergvalls1");
            yield return window.ClickButton(Win32.VK.Return);
        }

        public static float AnglesToTurn(Vector2 player_pos, float player_angle_rad, Vector2 point)
        {
            var angle = Mathf.WoWAngleBetween(player_pos, point);
            var current_angle = player_angle_rad * Mathf.Rad2Deg;
            var delta_angle = Mathf.Deg360To180(current_angle - angle);
            return delta_angle;
        }

        public static IEnumerator TurnTowardsPoint(WoWInstance instance, Vector2 player_pos, float player_angle_rad, Vector2 point)
        {
            var delta_angle = AnglesToTurn(player_pos, player_angle_rad, point);

            //Console.WriteLine("angle: " + angle + ", current:" + current_angle);
            if (Mathf.Positive(delta_angle) > 5.0f)
            {
                //Console.WriteLine("delta angle:" + delta_angle);
                yield return instance.window.TurnDegrees(delta_angle);
            }
        }

        public static IEnumerator Loot(WoWInstance instance)
        {
            int[] y_offsets = new int[]
            {
                67
            };

            foreach(var y_offset in y_offsets)
            {
                var area = instance.windowAlloc.area;
                var pos = new Vector2I
                {
                    y = area.Top + y_offset
                };

                var size = area.GetSize();
                var click_x_delta = size.x / 4;
                for (int i = 1; i < 4; ++i)
                {
                    pos.x = area.Left + click_x_delta * i;
                    yield return instance.MouseClickAtAbs(pos, MouseButton.Right);
                }
            }            
        }

        public static IEnumerator TaxiTowards(WoWInstance instance, Vector2I loc)
        {
            yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
            yield return FiberOps.WaitFor(2);
            yield return instance.window.ClickButton(BuiltinWoWButtons.DoManualStuff); // SelectGossipOption(1)
            yield return instance.window.ClickButton(BuiltinWoWButtons.DoManualStuff); // SelectGossipOption(1)
            yield return FiberOps.WaitFor(2);
            var topleft = instance.windowAlloc.area.TopLeft();
            yield return instance.MouseClickAtAbs(topleft + loc, MouseButton.Left);
            yield return instance.MouseClickAtAbs(topleft + loc, MouseButton.Left);
            yield return FiberOps.WaitFor(2);
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
        }

        public static IEnumerator GetAHListings(WoWInstance instance, List<int> silver_prizes_sink)
        {
            var prize_listings = new AHListingsData();
            for(int page_index = 1; page_index < 10; ++page_index)
            {
                yield return instance.text_reader.Request(page_index, prize_listings);
                if (prize_listings.listed_prize_counts.Length == 0)
                    break;

                foreach(var prize_count_pair in prize_listings.listed_prize_counts)
                {
                    for(var i = 0; i < prize_count_pair.count; ++i)
                    {
                        silver_prizes_sink.Add(prize_count_pair.prize_silver);
                    }
                }
            }
        }

        public static IEnumerator QueryAuctionItems(WoWInstance instance, string item_name)
        {
            yield return instance.window.EnterStringInChat("/run QueryAuctionItems(\"" + item_name + "\")");
        }

        public static IEnumerator GetAHListings(WoWInstance instance, string item_name, List<int> silver_prizes_sink)
        {
            yield return QueryAuctionItems(instance, item_name);
            yield return FiberOps.WaitFor(4);
            yield return GetAHListings(instance, silver_prizes_sink);
        }

        public static IEnumerator TakeOutAllMoney(WoWInstance instance)
        {
            yield return instance.window.EnterStringInChat("/run take_out_all_money()");
            yield return FiberOps.WaitFor(TakeOutMailboxWaitTime);
        }

        public static IEnumerator TakeOutAllFish(WoWInstance instance, Fish fish)
        {
            yield return instance.window.EnterStringInChat("/run take_out_all(\"" + FishOps.GetName(fish) + "\")");
            yield return FiberOps.WaitFor(TakeOutMailboxWaitTime);
        }

        public static IEnumerator ShowUIAndClickButton(WoWInstance instance, ButtonClick button, float wait_after_click)
        {
            yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
            yield return FiberOps.WaitFor(1);
            yield return instance.window.ClickButton(button);
            yield return FiberOps.WaitFor(wait_after_click);
            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
        }

        public static IEnumerator CameraDown(WoWInstance instance)
        {
            yield return ShowUIAndClickButton(instance, BuiltinWoWButtons.CameraDown, 3);
        }

        public static IEnumerator CameraUp(WoWInstance instance)
        {
            yield return ShowUIAndClickButton(instance, BuiltinWoWButtons.CameraUp, 3);
        }

        private static IEnumerator long_wait_until_impl(WoWInstance instance, DateTime end_time, Func<WoWInstance, DateTime> get_current_time_fn)
        {
            if(Args.Instance.CaptchaRequired)
            {

                instance.Log("Does a long wait without logging out");
                do
                {
                    yield return FiberOps.WaitFor(5 * 60);
                    yield return instance.window.ClickButton(BuiltinWoWButtons.Jump);
                } while (get_current_time_fn(instance) < end_time);

            } else
            {

                instance.Log("Does a long wait with terminate process method");
                instance.TerminateProcess();
                do
                {
                    yield return null;
                } while (get_current_time_fn(instance) < end_time);
                yield return StartWoWAndLogin(instance, new RefValue<Window>());
            
            }
        }

        public static IEnumerator LongWaitUntil(WoWInstance instance, DateTime end_time)
        {
            yield return long_wait_until_impl(instance, end_time, i => DateTime.Now);
        }

        public static IEnumerator LongWaitUntilServerTime(WoWInstance instance, DateTime end_time_servertime)
        {
            yield return long_wait_until_impl(instance, end_time_servertime, i => instance.ServerTime());
        }
    }
}
