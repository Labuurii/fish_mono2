﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    class SmallCBCommand : ICommand
    {
        Action<WoWInstance> cb;

        public SmallCBCommand(Action<WoWInstance> cb)
        {
            this.cb = cb;
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            cb(instance);
            yield return null;
        }
    }
}
