﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class MageRestData : ITransmittable
    {
        public int water_count, food_count;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out water_count)
                .Parse(out food_count);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.MageRestData);
        }
    }
}
