﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ReadTextWorker : WoWInstanceWorker
    {
        readonly ScreenResource screen;

        readonly List<Rect> reused_rects = new List<Rect>();
        readonly List<string> reused_strings = new List<string>();

        public ReadTextWorker(ScreenResource screen)
        {
            this.screen = screen;
        }

        public override void Update()
        {
            reused_rects.Clear();
            reused_strings.Clear();
            if(Args.Instance.PrintReadTextJobCount)
            {
                Console.WriteLine("Read Text Job Count: " + instances.Count);
            }
            foreach(var instance in instances)
            {
                var area = instance.textReadArea;
                reused_rects.Add(area);
            }

            screen.ReadText(reused_rects, reused_strings);

            for(var i = 0; i < instances.Count; ++i)
            {
                var instance = instances[i];
                var text = reused_strings[i];
                handle_text(instance, text);
            }

            /*
            foreach(var instance in instances)
            {
                var area = instance.textReadArea;
                var text = instance.context.screen.ReadText(area).Replace("\n", "");
                handle_text(instance, text);
            }*/
        }

        private static void handle_text(WoWInstance instance, string text)
        {
            text = text.Replace("\n", "");
            var expected_start = instance.text_reader.currentTextReadIndexStr;
            var end_index = text.IndexOf("QQ");
            if (text.StartsWith(expected_start) && end_index != -1)
            {
                text = text.Substring(expected_start.Length, end_index - expected_start.Length);
                instance.text_reader.SetCurrentText(text);
            }
            else
            {

                int x = 42;
            }
        }
    }
}
