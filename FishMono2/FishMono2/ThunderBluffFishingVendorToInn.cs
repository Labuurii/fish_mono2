﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public static class ThunderBluffFishingVendorToInn
    {
        public static ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                new Vector2( -1179.1f, -67.6f ),
                new Vector2( -1190.2f, -61.1f ),
                new Vector2(-1195.0f, -22.9f),
                new Vector2(-1210.5f, 9.1f),
                new Vector2(-1213.9f, 14.0f),
                new Vector2(-1226.0f, 32.6f),
                new Vector2(-1222.3f, 37.1f),
                new Vector2(-1219.5f, 48.0f),
                new Vector2(-1236.3f, 61.4f),
                new Vector2(-1252.0f, 61.1f),
                new Vector2(-1262.5f, 61.7f),
                new Vector2(-1278.0f, 49.8f),
                new Vector2(-1282.5f, 47.1f),
                new Vector2(-1287.1f, 44.5f),
                new Vector2(-1298.4f, 39.1f),
            },
            additional_flags = WorldPathFlags.IgnoreResting
        }.Construct();

        public static IEnumerator Perform(WoWInstance instance)
        {
            yield return Path.Start(instance, instance.context);
        }
    }
}
