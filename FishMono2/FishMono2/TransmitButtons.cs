﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class TransmitButtons
    {
        public const int End = (int)Win32.VK.F1;
        public const int ResetTransmitId = (int)Win32.VK.F2;
        public const int CharSummary = (int)Win32.VK.F3;
        public const int CombatData = (int)Win32.VK.F4;
        public const int PathingData = (int)Win32.VK.F5;
        public const int LevelChourData = (int)Win32.VK.F6;
        public const int MageRestData = (int)Win32.VK.F7;

        public static readonly ButtonClick TaxiData = new ButtonClick(Win32.VK.F1, KeyMod.Ctrl);
        public static readonly ButtonClick GetHomeData = new ButtonClick(Win32.VK.F2, KeyMod.Ctrl);
        public static readonly ButtonClick BagReport = new ButtonClick(Win32.VK.F4, KeyMod.Ctrl);
        public static readonly ButtonClick AHOpenData = new ButtonClick(Win32.VK.F5, KeyMod.Ctrl);
        public static readonly ButtonClick MailReport = new ButtonClick(Win32.VK.F7, KeyMod.Ctrl);
        public static readonly ButtonClick MerchantData = new ButtonClick(Win32.VK.F8, KeyMod.Ctrl);
    }
}
