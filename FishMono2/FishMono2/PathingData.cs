﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class PathingData : ITransmittable
    {
        public Vector2 player_pos;
        public float player_angle_rad;
        public float norm_player_hp;
        public float norm_player_mana;
        public int player_in_combat;
        public int player_is_casting;
        public int has_target;
        public int player_level;
        public float current_player_speed;
        public int can_retrieve_corpse;
        public int player_swimming;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out player_pos)
                .Parse(out player_angle_rad)
                .Parse(out norm_player_hp)
                .Parse(out norm_player_mana)
                .Parse(out player_in_combat)
                .Parse(out player_is_casting)
                .Parse(out has_target)
                .Parse(out player_level)
                .Parse(out current_player_speed)
                .Parse(out can_retrieve_corpse)
                .Parse(out player_swimming);
        }

        public PathingData Copy()
        {
            return new PathingData
            {
                player_pos = player_pos,
                player_angle_rad = player_angle_rad,
                norm_player_hp = norm_player_hp,
                norm_player_mana = norm_player_mana,
                player_in_combat = player_in_combat,
                player_is_casting = player_is_casting,
                has_target = has_target,
                player_level = player_level,
                current_player_speed = current_player_speed,
                can_retrieve_corpse = can_retrieve_corpse,
                player_swimming = player_swimming
            };
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.PathingData);
        }
    }
}
