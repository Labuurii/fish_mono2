﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class DateTimeOps
    {
        public static DateTime Today(DateTime time)
        {
            return time.AddHours(-time.Hour)
                .AddMinutes(-time.Minute)
                .AddSeconds(-time.Second);
        }
    }
}
