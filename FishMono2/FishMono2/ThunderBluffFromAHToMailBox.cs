﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public static class ThunderBluffFromAHToMailBox
    {
        public static readonly Vector2 MailBoxPos = new Vector2(-1263.3f, 44.6f);

        public static readonly ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                new Vector2( -1212.9f, 96.2f ),
                new Vector2( -1220.2f, 92.8f ),
                new Vector2( -1229.9f, 83.2f ),
                new Vector2( -1242.2f, 71.8f ),
                new Vector2( -1256.8f, 60.9f ),
                new Vector2( -1260.1f, 52.5f ),
                new Vector2( -1262.9f, 45.6f ),
            }
        }.Construct();

        public static IEnumerator Perform(WoWInstance instance)
        {
            yield return Path.Start(instance, instance.context);
            yield return new LookAtCommand(MailBoxPos).Start(instance, instance.context);
            yield return instance.MouseClickAtAbs(instance.windowAlloc.area.GetCenter(), MouseButton.Right);
        }
    }
}
