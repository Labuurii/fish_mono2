﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public abstract class FiberComponent : Component
    {
        protected readonly FiberExecutor exe = new FiberExecutor();

        public FiberComponent(WoWInstance instance) : base(instance)
        {
            exe.SetEnumerator(Run());
        }

        protected abstract IEnumerator Run();

        public override void Update()
        {
            exe.Step();
        }
    }
}
