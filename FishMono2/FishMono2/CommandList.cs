﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class CommandList : ICommand
    {
        public readonly ICommand[] commands;

        public CommandList(ICommand[] commands)
        {
            this.commands = commands;
        }

        public bool IsInRange(Vector2 pos)
        {
            if(commands == null)
            {
                return false;
            }

            foreach(var c in commands)
            {
                if(c.IsInRange(pos))
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerator Run(WoWInstance instance, BotContext context)
        {
            var fibers = new IEnumerator[commands.Length];
            for(int i = 0; i < commands.Length; ++i)
            {
                fibers[i] = commands[i].Start(instance, context);
            }

            foreach(var fiber in fibers)
            {
                yield return fiber;
            }
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            yield return Run(instance, context);
        }
    }
}
