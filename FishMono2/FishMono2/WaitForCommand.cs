﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class WaitForCommand : ICommand
    {
        float time;

        public WaitForCommand(float time)
        {
            this.time = time;
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            yield return FiberOps.WaitFor(time);
        }
    }
}
