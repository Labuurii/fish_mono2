﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class VendorPaths
    {
        public static CommandList FromValleyOfTrials04(int level)
        {
            return new CommandList(new ICommand[]
            {
                new RunAndCombat
                {
                    path = new WorldPathData
                    {
                        points = GrindingPaths.ValleyOfTrials04,
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 5.0f
                    },
                    max_level = level + 1
                }.Construct(),

                new VendorAllItems
                {
                    to_vendor_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -493.8f, -4257.1f ),
                            new Vector2( -519.7f, -4277.5f ),
                            new Vector2( -551.3f, -4276.1f ),
                            new Vector2( -576.7f, -4268.9f ),
                            new Vector2( -586.3f, -4247.7f ),
                            new Vector2( -577.8f, -4227.0f ),
                            new Vector2( -575.0f, -4218.2f ),
                            new Vector2( -573.2f, -4214.2f ),
                            new Vector2( -565.3f, -4214.2f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    vendor_pos = new Vector2( -565.3f, -4214.2f ),
                    back_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -569.4f, -4213.2f ),
                            new Vector2( -578.1f, -4207.2f ),
                            new Vector2( -590.1f, -4205.6f ),
                            new Vector2( -601.9f, -4211.8f ),
                            new Vector2( -612.1f, -4208.5f ),
                            new Vector2( -618.9f, -4212.0f ),
                            new Vector2( -625.4f, -4210.3f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    level = level
                }.Construct(),
                new LearnAllSpells
                {
                    to_trainer_path = new WorldPathData() // Learn train path walks to this location
                    {
                        points = new Vector2[] { new Vector2( -625.2f, -4210.2f ), },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    trainer_pos = new Vector2( -625.2f, -4210.2f ),
                    back_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -625.4f, -4210.3f ),
                            new Vector2( -620.1f, -4211.9f ),
                            new Vector2( -618.4f, -4222.1f ),
                            new Vector2( -614.4f, -4251.1f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    level = level
                }.Construct()
            });
        }

        public static CommandList FromValleyOfTrials07(int level)
        {
            return new CommandList(new ICommand[]
            {
                new RunAndCombat
                {
                    path = new WorldPathData
                    {
                        points = GrindingPaths.ValleyOfTrials07,
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 5.0f
                    },
                    max_level = level + 1
                }.Construct(),

                new VendorAllItems
                {
                    to_vendor_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -403.2f, -4164.3f ),
                            new Vector2( -410.4f, -4201.0f ),
                            new Vector2( -430.4f, -4234.1f ),
                            new Vector2( -459.3f, -4254.4f ),
                            new Vector2( -531.5f, -4277.5f ),
                            new Vector2( -569.5f, -4270.8f ),
                            new Vector2( -593.0f, -4229.6f ),
                            new Vector2( -599.4f, -4207.8f ),
                            new Vector2( -582.9f, -4205.4f ),
                            new Vector2( -574.3f, -4210.4f ),
                            new Vector2( -567.4f, -4214.7f ),
                            new Vector2( -565.4f, -4214.2f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    vendor_pos = new Vector2( -565.4f, -4214.2f ),
                    back_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -569.4f, -4213.2f ),
                            new Vector2( -578.1f, -4207.2f ),
                            new Vector2( -590.1f, -4205.6f ),
                            new Vector2( -601.9f, -4211.8f ),
                            new Vector2( -612.1f, -4208.5f ),
                            new Vector2( -618.9f, -4212.0f ),
                            new Vector2( -625.4f, -4210.3f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    level = level
                }.Construct(),
                new LearnAllSpells
                {
                    to_trainer_path = new WorldPathData() // Learn train path walks to this location
                    {
                        points = new Vector2[] { new Vector2( -625.2f, -4210.2f ), },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    trainer_pos = new Vector2( -625.2f, -4210.2f ),
                    back_path = new WorldPathData
                    {
                        points = new Vector2[]
                        {
                            new Vector2( -625.4f, -4210.3f ),
                            new Vector2( -620.1f, -4211.9f ),
                            new Vector2( -618.4f, -4222.1f ),
                            new Vector2( -614.4f, -4251.1f ),
                        },
                        flags = WorldPathFlags.AvoidCombat,
                        pointCompletedDistance = 2.0f
                    },
                    level = level
                }.Construct()
            });
        }
    }
}
