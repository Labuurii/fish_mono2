﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class WoWErrors
    {
        public const int OutOfRange = 363;
        public const int TargetNotInFront = 50;
        public const int OutOfMana = 300;
    }
}
