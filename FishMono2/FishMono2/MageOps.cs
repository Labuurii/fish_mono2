﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class MageOps
    {
        public static IEnumerator CreateCombatResourcesIfNecessary(WoWInstance instance)
        {
            var pathing_data = new PathingData();
            var rest_data = new MageRestData();
            yield return instance.text_reader.Request(rest_data);

            if (rest_data.water_count == 0)
            {
                yield return instance.window.ClickButton(BuiltinWoWButtons.MageCleanupBagForMoreWater);
                yield return instance.window.ClickButton(MageSpells.ConjureWater);
                for (; ; )
                {
                    yield return instance.text_reader.Request(pathing_data);
                    if (pathing_data.player_is_casting == 0)
                    {
                        break;
                    }
                    yield return FiberOps.WaitFor(1);
                }
            }

            if (rest_data.food_count == 0)
            {
                yield return instance.window.ClickButton(BuiltinWoWButtons.MageCleanupBagForMoreFood);
                yield return instance.window.ClickButton(MageSpells.ConjureFood);
                for (; ; )
                {
                    yield return instance.text_reader.Request(pathing_data);
                    if (pathing_data.player_is_casting == 0)
                    {
                        break;
                    }
                    yield return FiberOps.WaitFor(1);
                }
            }
        }

        public static IEnumerator RunCombat(WoWInstance instance, RefValue<bool> engaged_in_combat)
        {
            var last_cast_time = DateTime.Now.AddDays(-1);

            var combat_data = new CombatData();
            for (; ; )
            {
                yield return instance.text_reader.Request(combat_data);

                if (combat_data.has_target == 0 && combat_data.player_in_combat == 0 && combat_data.is_casting == 0)
                {
                    yield break;
                }

                if(combat_data.player_hp_norm <= 0.01f)
                {
                    yield break;
                }

                if(combat_data.player_in_combat != 0 || combat_data.target_in_combat != 0)
                {
                    engaged_in_combat.value = true;
                }

                if (combat_data.is_new_error && combat_data.is_casting == 0)
                {
                    switch (combat_data.last_wow_error)
                    {
                        case WoWErrors.TargetNotInFront:
                            if(combat_data.has_target != 0)
                            {
                                yield return instance.window.TurnDegrees(90);
                            }
                            break;
                        case WoWErrors.OutOfRange:
                            if (combat_data.player_in_combat != 0 && combat_data.is_casting == 0)
                            {
                                yield return instance.window.ClickButton(BuiltinWoWButtons.WalkForward, 1.0f);
                            }
                            else
                            {
                                yield return instance.window.ClickButton(BuiltinWoWButtons.ClearTarget);
                                yield break;
                            }
                            break;
                        case WoWErrors.OutOfMana:
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StartAttack);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (combat_data.target_in_combat == 0 && combat_data.player_in_combat != 0 && DateTime.Now > last_cast_time.AddSeconds(2.5))
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.ClearTarget);
                        yield return FiberOps.WaitFor(1.0f);
                    }
                    else if (combat_data.is_casting == 0)
                    {
                        last_cast_time = DateTime.Now;
                        if(combat_data.player_hp_norm < 0.3f && instance.account.race == "TROLL")
                        {
                            yield return instance.window.ClickButton(MageSpells.Berserking);
                        }
                        if(combat_data.player_in_combat != 0)
                        {
                            //yield return instance.window.ClickButton(MageSpells.FireBlast); Not trained on the current leveling rotation
                        }
                        yield return instance.window.ClickButton(MageSpells.Frostbolt);
                        yield return instance.window.ClickButton(MageSpells.Fireball);
                    } else if(combat_data.is_casting != 0 && DateTime.Now.AddSeconds(1) > last_cast_time)
                    {
                        yield return FiberOps.WaitUntil(last_cast_time.AddSeconds(1));
                    } else
                    {
                        yield return null;
                        yield return null;
                    }
                }
            }
        }
    }
}
