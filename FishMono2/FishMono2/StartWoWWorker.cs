﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public struct StartWoWJob
    {
        public WoWInstance instance;
        public StartWoWFuture future;
    }

    public class StartWoWFuture
    {
        // Pid is written directly to the WoW instance so it knows about it, the window can be assigned directly to the instance because the size is not known here.
        public Window window;
    }

    public class StartWoWWorker
    {
        public const string SetPortalStringStart = "SET portal \"";
        public const string SetWindowResolutionStart = "SET gxWindowedResolution \"";
        public const string SetTextLocaleStart = "SET textLocale \"";
        public const string WindowResolutionValue = "320x235";

        public const string WTF_OPTION_END = "\"";

        public const string ClassicWoWConfigFilePath = "C:\\Program Files (x86)\\World of Warcraft\\_classic_\\WTF\\Config.wtf";

        readonly Queue<StartWoWJob> jobs = new Queue<StartWoWJob>();
        readonly FiberExecutor exe = new FiberExecutor();


        public StartWoWWorker()
        {
            exe.SetEnumerator(Run());
        }

        private IEnumerator Run()
        {
            for(;;)
            {
                if(jobs.Count > 0)
                {
                    var job = jobs.Dequeue();
                    yield return perform_job(job);
                } else
                {
                    yield return null;
                }
            }
        }

        static string replace_content_between(string subject, string start, string end, string new_in_between)
        {
            var start_index = subject.IndexOf(start);
            if (start_index == -1)
            {
                throw new Exception("Could not find start");
            }
            start_index += start.Length;
            var end_index = subject.IndexOf(end, start_index);
            if (end_index == -1)
            {
                throw new Exception("Could not find end");
            }
            return subject.Substring(0, start_index) + new_in_between + subject.Substring(end_index);
        }

        private IEnumerator perform_job(StartWoWJob job)
        {
            var instance = job.instance;

            {
                var config_file_content = File.ReadAllText(ClassicWoWConfigFilePath);
                config_file_content = replace_content_between(config_file_content, SetPortalStringStart, WTF_OPTION_END, instance.account.region.ToString());
                config_file_content = replace_content_between(config_file_content, SetWindowResolutionStart, WTF_OPTION_END, WindowResolutionValue);
                config_file_content = replace_content_between(config_file_content, SetTextLocaleStart, WTF_OPTION_END, instance.account.language.ToString());
                File.WriteAllText(ClassicWoWConfigFilePath, config_file_content);
            }

            var start_wow_task = Task.Run(delegate ()
            {
                return Process.Start(new ProcessStartInfo
                {
                    FileName = WoWPaths.CLASSIC_WOW_PATH,
                });
            });
            yield return FiberOps.WaitFor(start_wow_task);
            var process = start_wow_task.Result;
            instance.pid = process.Id;

            for (; ; )
            {
                var main_window_task = Task.Run(delegate ()
                {
                    var window_handles = new List<IntPtr>();
                    Win32Ops.GetWindowHandles(process, window_handles);
                    foreach(var h in window_handles)
                    {
                        if(Win32Ops.GetWindowTitle(h) == "World of Warcraft")
                        {
                            if(Win32Ops.GetWindowClass(h) == "GxWindowClass")
                            {
                                return h;
                            }
                        }
                    }

                    return IntPtr.Zero;
                });
                yield return FiberOps.WaitFor(main_window_task);
                var main_window = main_window_task.Result;
                if (main_window != IntPtr.Zero)
                {
                    if(instance.has_stopped)
                    {
                        process.Kill();
                        break; // Future is not referenced by anyone
                    }
                    job.future.window = new Window(main_window);
                    break;
                }
                yield return null;
            }

            yield return FiberOps.WaitFor(5);
        }

        public void Update()
        {
            if(!exe.Step())
            {
                throw new Exception("StartWoWWorker has stopped");
            }
        }

        public StartWoWFuture Add(WoWInstance instance)
        {
            var future = new StartWoWFuture();
            jobs.Enqueue(new StartWoWJob
            {
                instance = instance,
                future = future
            });
            return future;
        }
    }
}
