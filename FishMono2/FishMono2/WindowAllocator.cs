﻿using Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class WindowAllocation
    {
        public readonly Window window;
        public readonly Rect allocation_area;
        public readonly Rect area;

        public WindowAllocation(Window window, Rect allocation_area, Rect area)
        {
            this.window = window;
            this.allocation_area = allocation_area;
            this.area = area;
        }
    }

    public class WindowAllocator
    {
        readonly int allocation_width, allocation_height;
        readonly WindowAllocation[] allocations;
        public readonly Vector2I screen_size;

        public WindowAllocator(Vector2I screen_size)
        {
            this.screen_size = screen_size;
            Debug.Assert(screen_size.x % 10 == 0, "Width is not a power of 10");
            Debug.Assert(screen_size.y % 10 == 0, "Height is not a power of 10");
            allocation_width = screen_size.x / 10;
            allocation_height = screen_size.y / 10;
            allocations = new WindowAllocation[allocation_width * allocation_height];
        }

        public WindowAllocation GetWindowToTheSideOf(Vector2I screen_pos, Vector2I search_direction)
        {
            Debug.Assert(screen_size.x % 10 == 0, "Width is not a power of 10");
            Debug.Assert(screen_size.y % 10 == 0, "Height is not a power of 10");            
            
            var alloc_pos = new Vector2I(screen_pos.x / 10, screen_pos.y / 10);
            var filter = GetAt(alloc_pos);

            for(;;)
            {
                alloc_pos += search_direction;

                if (alloc_pos.x < 0 || alloc_pos.y < 0)
                {
                    break;
                }
                if(alloc_pos.x >= allocation_width)
                {
                    break;
                }
                if(alloc_pos.y >= allocation_height)
                {
                    break;
                }

                var alloc = GetAt(alloc_pos);
                if(filter != null && filter == alloc)
                {
                    continue;
                }

                if(alloc != null)
                {
                    return alloc;
                }
            }

            return null;
        }

        // pos is in allocation slot positions
        int GetIndex(Vector2I allocation_pos)
        {
            return allocation_pos.y * allocation_width + allocation_pos.x;
        }

        // pos is in allocation slot positions
        private WindowAllocation GetAt(Vector2I pos)
        {
            var index = GetIndex(pos);
            return allocations[index];
        }

        // Rect positions in allocation slots
        private bool IsFree(Rect rect)
        {
            for(int y = rect.Top; y < rect.Bottom; ++y)
            {
                for(int x = rect.Left; x < rect.Right; ++x)
                {
                    if(GetAt(new Vector2I(x, y)) != null)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void SetAt(Rect allocation_area, WindowAllocation alloc)
        {
            for (int y = allocation_area.Top; y < allocation_area.Bottom; ++y)
            {
                for (int x = allocation_area.Left; x < allocation_area.Right; ++x)
                {
                    var index = GetIndex(new Vector2I(x, y));
                    allocations[index] = alloc;
                }
            }
        }

        public WindowAllocation TryAllocate(Window window, Vector2I size)
        {
            // Size is now in allocation slots
            size.x /= 10;
            size.y /= 10;

            for (int y = 0; y < allocation_height - size.y + 1; ++y)
            {
                for (int x = 0; x < allocation_width - size.x + 1; ++x)
                {
                    var rect = new Rect
                    {
                        Top = y,
                        Left = x,
                        Right = x + size.x,
                        Bottom = y + size.y
                    };
                    if (IsFree(rect))
                    {
                        var screen_area = new Rect
                        {
                            Top = rect.Top * 10,
                            Left = rect.Left * 10,
                            Right = rect.Right * 10,
                            Bottom = rect.Bottom * 10
                        };
                        var alloc = new WindowAllocation(window, rect, screen_area);
                        SetAt(rect, alloc);
                        return alloc;
                    }
                }
            }

            return null;
        }

        public WindowAllocation Allocate(Window window, Vector2I size)
        {
            var alloc = TryAllocate(window, size);
            if(alloc != null)
            {
                return alloc;
            }

            throw new Exception("Could not find a free window slot");
        }

        public void Free(WindowAllocation allocation)
        {
            SetAt(allocation.allocation_area, null);
        }

        public WindowAllocation GetFirstAlloc()
        {
            foreach(var alloc in allocations)
            {
                if(alloc != null)
                {
                    return alloc;
                }
            }
            return null;
        }
    }
}
