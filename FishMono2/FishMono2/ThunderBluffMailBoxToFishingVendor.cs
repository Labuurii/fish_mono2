﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public static class ThunderBluffMailBoxToFishingVendor
    {
        public static readonly Vector2 VendorPos = new Vector2(-1177.1f, -65.8f);

        public static readonly ICommand[] WalkCloseCommands = new ICommand[]
        {
            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1177.4f, -67.1f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1177.8f, -64.3f), }
            }.Construct(),

            new WalkPath
            {
                points = new Vector2[] { new Vector2(-1176.1f, -64.8f), }
            }.Construct(),
        };

        public static readonly ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                new Vector2( -1262.5f, 48.0f ),
                new Vector2(-1259.0f, 55.7f),
                new Vector2(-1243.7f, 61.6f),
                new Vector2(-1227.3f, 53.6f),
                new Vector2(-1208.1f, 38.0f),
                new Vector2(-1206.9f, 33.9f),
                new Vector2(-1206.5f, 26.6f),
                new Vector2(-1202.7f, 22.1f),
                new Vector2(-1196.9f, 21.1f),
                new Vector2(-1191.2f, 24.3f),
                new Vector2(-1189.1f, 28.3f),
                new Vector2(-1189.6f, 34.5f),
                new Vector2(-1193.8f, 38.7f),
                new Vector2(-1200.9f, 38.7f),
                new Vector2(-1204.5f, 35.5f),
                new Vector2(-1205.7f, 30.4f),
                new Vector2(-1204.6f, 26.0f),
                new Vector2(-1200.6f, 23.2f),
                new Vector2(-1198.3f, 22.4f),
                new Vector2(-1197.5f, 16.9f),
                new Vector2(-1202.1f, -9.7f),
                new Vector2(-1194.4f, -34.4f),
                new Vector2(-1188.2f, -61.2f),
                new Vector2(-1179.5f, -66.9f),
            }
        }.Construct();

        public static IEnumerator Perform(WoWInstance instance, MerchantData merchant_data)
        {
            yield return Path.Start(instance, instance.context);
            foreach(var close_command in WalkCloseCommands)
            {
                yield return close_command.Start(instance, instance.context);
                yield return new LookAtCommand(VendorPos).Start(instance, instance.context);
                yield return instance.MouseClickAtAbs(instance.windowAlloc.area.GetCenter(), MouseButton.Right);
                yield return FiberOps.WaitFor(2);
                yield return instance.text_reader.Request(merchant_data);
                if(merchant_data.merchant_window_open != 0)
                {
                    break;
                }
            }
        }
    }
}
