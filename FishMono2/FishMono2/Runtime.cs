﻿using Shared;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class Runtime : IDisposable
    {
        readonly List<int> wow_instances_to_terminate = new List<int>(); // Indicies into wow_instances

        public readonly List<WoWInstance> wow_instances = new List<WoWInstance>();
        public readonly BotContext bot_context;
        public bool should_shut_down;

        public Runtime()
        {
            var screen = new ScreenResource();
            var size = screen.GetSize();
            var alloc = new WindowAllocator(size);
            bot_context = new BotContext(screen, alloc);
        }

        // Threadsafe
        public void AddAccount(WoWAccount account, int pid = 0, Window window = null)
        {
            var instance = new WoWInstance(bot_context, account);
            if (window != null)
            {
                instance.pid = pid;
            }
            IEnumerator bot_enum;
            if(Args.Instance.Test)
            {
                bot_enum = BotOps.StartTest(instance, window);
            } else
            {
                bot_enum = BotOps.Start(instance, window);
            }
            instance.SetBot(bot_enum);
            wow_instances.Add(instance);
        }

        public void AddAccount(int pid, Window window)
        {
            var account = new WoWAccount(string.Empty);
            AddAccount(account, pid, window);
        }

        public void RemoveAccount(int index)
        {
            var instance = wow_instances[index];
            wow_instances.RemoveAt(index);
            instance.Dispose();
        }

        public void RemoveAccount(string email)
        {
            for(int i = 0; i < wow_instances.Count;)
            {
                var instance = wow_instances[i];
                if(instance.account.email == email)
                {
                    RemoveAccount(i);
                } else
                {
                    ++i;
                }
            }
        }

        public void Update()
        {
            bot_context.screen.Update();
            bot_context.read_text_worker.Update();
            bot_context.mouse_worker.CheckException();
            bot_context.start_wow_worker.Update();

            wow_instances_to_terminate.Clear();
            for (int i = 0; i < wow_instances.Count; ++i)
            {
                var instance = wow_instances[i];
                if (!instance.has_stopped && !instance.is_paused)
                {
                    try
                    {
                        if (!instance.executor.Step())
                        {
                            instance.has_stopped = true;
                            Console.WriteLine(string.Format("{0} has stopped", instance.account.email));

                            if (instance.should_terminate_on_stop || instance.should_shut_down)
                            {
                                wow_instances_to_terminate.Add(i);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        instance.Log("Crashed: " + e.Message + "\nStacktrace:\n" + e.StackTrace + "\n\nFiber Trace:\n" + instance.executor.GetEnumeratorsStackFrames());
                        instance.has_stopped = true;
                        wow_instances_to_terminate.Add(i);
                    }
                }
            }

            wow_instances_to_terminate.Reverse(); // Lower indicies will be invalidated if we start with them
            terminate_accounts();
        }

        private void terminate_accounts()
        {
            foreach (var instance_index in wow_instances_to_terminate)
            {
                RemoveAccount(instance_index);
            }
        }

        public void Dispose()
        {
            wow_instances_to_terminate.Clear();
            for(var i = 0; i < wow_instances.Count; ++i)
            {
                wow_instances_to_terminate.Add(i);
            }
            wow_instances_to_terminate.Reverse();
            terminate_accounts();
        }
    }
}
