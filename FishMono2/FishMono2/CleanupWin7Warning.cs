﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    class CleanupWin7Warning : RepeatComponent
    {
        public CleanupWin7Warning() : base(10.0f)
        {
        }

        protected override void PerformJob()
        {
            Task.Run(delegate ()
            {
                var window_handle = Win32.FindWindow(null, "Optional update delivery is not working");
                if (window_handle != IntPtr.Zero)
                {
                    Win32.ShowWindow(window_handle, Win32.ShowWindowCommands.SW_MINIMIZE);
                }

            });
        }
    }
}
