﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public static class ThunderBluffInnToMailBox
    {
        public static readonly Vector2 MailPoint = new Vector2();

        public static readonly ICommand Path = new WalkPath
        {
            points = new Vector2[]
            {
                new Vector2( -1298.1f, 39.3f ),
                new Vector2( -1287.0f, 45.1f ),
                new Vector2( -1283.5f, 46.7f ),
                new Vector2( -1279.0f, 48.7f ),
                new Vector2( -1273.2f, 51.2f ),
                new Vector2( -1265.6f, 45.3f ),
                new Vector2( -1264.7f, 44.8f ),
            }
        }.Construct();

        // data.mailbox_open will tell you if this function was successful
        public static IEnumerator OpenMailBox(WoWInstance instance, GetHomeData data)
        {
            yield return Path.Start(instance, instance.context);
            yield return new LookAtCommand(MailPoint).Start(instance, instance.context);
            yield return instance.MouseClickAtAbs(instance.windowAlloc.area.GetCenter(), MouseButton.Right);
            yield return FiberOps.WaitFor(2);
            yield return instance.text_reader.Request(data);
        }
    }
}
