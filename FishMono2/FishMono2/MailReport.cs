﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class MailReport : ITransmittable
    {
        public readonly InventoryState normal_mails = new InventoryState();

        public void Parse(StringArrayParseState parse_state)
        {
            normal_mails.Parse(parse_state);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.MailReport);
        }
    }
}
