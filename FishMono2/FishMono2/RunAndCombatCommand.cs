﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class RunAndCombat
    {
        public WorldPathData path;
        public int max_level = -1;
        public int min_level = -1;
        public ICommand death_handler;
        public float max_runtime;

        public RunAndCombatCommand Construct()
        {
            return new RunAndCombatCommand(path.Construct(), max_level, min_level, death_handler, max_runtime);
        }
    }

    public class WalkPath
    {
        public Vector2[] points;
        public WorldPathFlags additional_flags = WorldPathFlags.Default;
        public float max_runtime = 10 * 60;

        public RunAndCombatCommand Construct()
        {
            return new RunAndCombat
            {
                path = new WorldPathData
                {
                    points = points,
                    pointCompletedDistance = 1.0f,
                    flags = WorldPathFlags.IgnoreCombat | WorldPathFlags.AvoidCombat | WorldPathFlags.Walk | WorldPathFlags.NoAntiStuck | additional_flags,
                },
                max_runtime = max_runtime
            }.Construct();
        }
    }

    public class RunAndCombatCommand : ICommand
    {
        readonly RefValue<bool> engaged_in_combat = new RefValue<bool>();

        public readonly WorldPath path;
        public readonly int max_level;
        public readonly int min_level;
        public readonly ICommand death_handler;
        public readonly float max_runtime;

        public RunAndCombatCommand(WorldPath path, int max_level, int min_level, ICommand death_handler, float max_runtime)
        {
            this.path = path;
            this.max_level = max_level;
            this.min_level = min_level;
            this.death_handler = death_handler;
            this.max_runtime = max_runtime;
        }

        IEnumerator run_combat(WoWInstance instance, PathingData pathing_data, PathingState pathing_state)
        {
            engaged_in_combat.value = false;
            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
            yield return BotOps.RunCombat(instance, engaged_in_combat);
            yield return instance.text_reader.Request(pathing_data);
            if(pathing_data.norm_player_hp <= 0.01f)
            {
                yield break; // Dead
            }
            if(should_rest(pathing_data))
            {
                yield return rest(instance, pathing_data, pathing_state);
            }
            if (engaged_in_combat.value)
            {
                yield return instance.window.ClickButton(BuiltinWoWButtons.MoveBackward); // Why is this here?
                yield return BotOps.Loot(instance);
            }
        }

        bool should_rest(PathingData pathing_data)
        {
            if((path.flags & WorldPathFlags.IgnoreResting) == WorldPathFlags.IgnoreResting)
            {
                return false;
            }

            if (pathing_data.norm_player_mana < 0.9f || pathing_data.norm_player_hp < 0.9f)
            {
                return true;
            }
            return false;
        }

        IEnumerator rest(WoWInstance instance, PathingData pathing_data, PathingState pathing_state)
        {
            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);

            if (instance.account.@class == "MAGE")
            {
                yield return MageOps.CreateCombatResourcesIfNecessary(instance);

                if (pathing_data.norm_player_mana < 0.9f)
                {
                    yield return instance.window.ClickButton(BuiltinWoWButtons.MageDrink);
                }
                if (pathing_data.norm_player_hp < 0.9f)
                {
                    yield return instance.window.ClickButton(BuiltinWoWButtons.MageEat);
                }
            }

            while (pathing_data.norm_player_mana < 0.9f || pathing_data.norm_player_hp < 0.9f)
            {
                yield return null;
                yield return instance.text_reader.Request(pathing_data);

                if (should_run_combat(pathing_state, pathing_data))
                {
                    yield return run_combat(instance, pathing_data, pathing_state);
                    break;
                }
                else if (pathing_data.norm_player_hp <= 0.01f)
                {
                    yield break; // Dead
                }
                yield return FiberOps.WaitFor(1);
            }
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            if(instance.should_shut_down)
            {
                yield break;
            }

            var has_toggled_walking = false;
            DateTime? timeout = new DateTime?();
            var last_player_point = new Vector2();
            var path = new PathingState(this.path);

            var pathing_data = new PathingData();

            if(max_runtime != 0)
            {
                timeout = DateTime.Now.AddSeconds(max_runtime);
            }

            {
                var last_transmit = instance.text_reader.lastTransmit as PathingData;
                if(last_transmit != null && instance.text_reader.lastTransmitTime.AddSeconds(2) < DateTime.Now)
                {
                    pathing_data = last_transmit.Copy();
                }
            }
            yield return instance.text_reader.Request(pathing_data);

            if(should_run_combat(path, pathing_data))
            {
                run_combat(instance, pathing_data, path);
            }

            if(max_level != -1 && pathing_data.player_level >= max_level)
            {
                yield break;
            }
            if(min_level != -1 && pathing_data.player_level < min_level)
            {
                yield break;
            }

            if (!path.Advance(pathing_data.player_pos) || !WorldPath.IsInRange(pathing_data.player_pos, path.CurrentPoint()))
            {
                if (death_handler == null || death_handler != null && !death_handler.IsInRange(pathing_data.player_pos))
                {
                    yield break;
                }
            }

            yield return instance.window.ClickButton(BuiltinWoWButtons.ClearTarget);
            yield return instance.window.ClickButton(BuiltinWoWButtons.TargetNearestEnemy);

            Vector2 point;
            if(path.HasPoint())
            {
                point = path.CurrentPoint();
                yield return BotOps.TurnTowardsPoint(instance, pathing_data.player_pos, pathing_data.player_angle_rad, point);
            }

            for (; ; )
            {
                for (int walk_index = 0; walk_index < 3; ++walk_index)
                {
                    if(instance.should_shut_down)
                    {
                        yield break;
                    }

                    if(timeout.HasValue)
                    {
                        if(DateTime.Now >= timeout.Value)
                        {
                            throw new Exception("The path has timed out");
                        }
                    }
                    
                    yield return instance.text_reader.Request(pathing_data);

                    // Wait for death to be resolved
                    if (pathing_data.norm_player_hp <= 0.01f)
                    {
                        if (death_handler == null)
                        {
                            instance.Log("Is currently dead, awaiting manual assistance");
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                            yield return instance.window.ClickButton(BuiltinWoWButtons.ShowUI);
                            for (; ; )
                            {
                                yield return instance.text_reader.ResetIdCounter();
                                yield return instance.text_reader.Request(pathing_data);
                                if (pathing_data.norm_player_hp >= 0.01f)
                                {
                                    break;
                                }
                                yield return instance.window.ClickButton(TransmitButtons.End);
                                yield return FiberOps.WaitFor(5.0f);
                            }
                            instance.Log("Now alive, starts botting again");
                            yield return instance.window.ClickButton(BuiltinWoWButtons.HideUI);
                            instance.buffComponent.Reset();
                        }
                        else
                        {
                            instance.Log("Seems like we are dead, runs death handler");
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                            yield return death_handler.Start(instance, context);
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                            yield return instance.text_reader.Request(pathing_data);
                            instance.Log("We should not be dead anymore...");
                        }
                        has_toggled_walking = false;
                        path.ReevaluateClosestPoint(pathing_data.player_pos);
                        continue;
                    }

                    // Done killing mobs
                    if (max_level != -1 && pathing_data.player_level >= max_level)
                    {
                        goto end;
                    }

                    // Get the next point in the path if required
                    if (!path.Advance(pathing_data.player_pos))
                    {
                        goto end;
                    }

                    // Prepare for combat
                    if(should_rest(pathing_data))
                    {
                        yield return rest(instance, pathing_data, path);
                    }

                    // Antistuck
                    if ((path.path.flags & WorldPathFlags.NoAntiStuck) != WorldPathFlags.NoAntiStuck && last_player_point == pathing_data.player_pos && pathing_data.current_player_speed != 0)
                    {
                        yield return instance.window.TurnDegrees(90);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.StartAutoRun);
                        yield return instance.window.ClickButton(BuiltinWoWButtons.Jump);
                    }

                    // Is in combat or has target if appropriate
                    if (should_run_combat(path, pathing_data))
                    {
                        yield return run_combat(instance, pathing_data, path);
                        continue;
                    } else if(instance.buffComponent.ShouldBuff(pathing_data))
                    {
                        // Apply buffs if necessary
                        yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                        yield return instance.buffComponent.Perform();
                        continue;
                    }

                    // Make sure we are walking in the approprioate speed
                    if (pathing_data.current_player_speed == 0)
                    {
                        if(!has_toggled_walking)
                        {
                            has_toggled_walking = true;
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StartAutoRun);
                            yield return instance.text_reader.Request(pathing_data);

                            if (!path.Advance(pathing_data.player_pos))
                            {
                                goto end;
                            }
                        } else
                        {
                            instance.window.ClickButtonAsync(BuiltinWoWButtons.StartAutoRun);
                        }
                    } else
                    {
                        if((path.path.flags & WorldPathFlags.NoAntiStuck) == WorldPathFlags.NoAntiStuck && last_player_point == pathing_data.player_pos) // There is a bug where sometimes the animation is running but the character isnt
                        {
                            // If antistucking is enabled that one will take care of this bug on it's own
                            yield return instance.window.ClickButton(BuiltinWoWButtons.MoveBackward);
                        }
                    }

                    if (path.path.walk)
                    {
                        if (pathing_data.current_player_speed != 0 && pathing_data.current_player_speed > 4.71f)
                        {
                            yield return instance.window.ClickButton(BuiltinWoWButtons.ToggleWalking);
                        }
                    }
                    else
                    {
                        if (pathing_data.current_player_speed != 0 && pathing_data.current_player_speed < 4.69f) // Swimming is 4.7
                        {
                            yield return instance.window.ClickButton(BuiltinWoWButtons.ToggleWalking);
                        }
                    }

                    // Get the current point and do the turning
                    point = path.CurrentPoint();
                    var angles_to_turn = BotOps.AnglesToTurn(pathing_data.player_pos, pathing_data.player_angle_rad, point);
                    if(Mathf.Positive(angles_to_turn) > 5)
                    {
                        var point_distance = Vector2.Distance(point, pathing_data.player_pos);
                        var should_stop_and_turn = point_distance < path.path.stopAndTurnDistance;

                        if (should_stop_and_turn)
                        {
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
                        }

                        yield return instance.window.TurnDegrees(angles_to_turn);
                    }

                    last_player_point = pathing_data.player_pos;
                }

                // Try to engage in combat
                if (!path.path.avoidCombat)
                {
                    yield return instance.window.ClickButton(BuiltinWoWButtons.TargetNearestEnemy);
                }
            }

            end:
            yield return instance.window.ClickButton(BuiltinWoWButtons.StopAutoRun);
        }

        private static bool should_run_combat(PathingState path, PathingData pathing_data)
        {
            if(path.path.ignoreCombat)
            {
                return false;
            }

            return pathing_data.player_in_combat != 0 || !path.path.avoidCombat && pathing_data.has_target != 0;
        }

        public bool IsInRange(Vector2 pos)
        {
            return path.IsInRange(pos) || death_handler != null && death_handler.IsInRange(pos);
        }

        public ICommand GetDeathHandler()
        {
            return death_handler;
        }
    }
}
