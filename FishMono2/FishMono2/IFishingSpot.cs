﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public interface IFishingSpot
    {
        // The taxi button location in default window configuration
        Vector2I GetTaxiLocation();
        ICommand GetWalkToCommand();
        ICommand GetFishingCommand();

        Fish[] AvailableFishes();
    }
}
