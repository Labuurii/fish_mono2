﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class ClickCenterCommand : ICommand
    {
        readonly MouseButton mouse_btn;

        public ClickCenterCommand(MouseButton mouse_btn)
        {
            this.mouse_btn = mouse_btn;
        }

        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var area = instance.windowAlloc.area;
            var center = area.GetCenter();
            yield return instance.MouseClickAtAbs(center, mouse_btn);
        }
    }
}
