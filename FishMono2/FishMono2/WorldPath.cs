﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    [Flags]
    public enum WorldPathFlags
    {
        Default = 0,
        Circular = 1,
        AvoidCombat = 2,
        IgnoreCombat = 4,
        Walk = 8,
        IgnoreResting = 16,
        NoAntiStuck = 32
    }

    public class WorldPath
    {
        public readonly List<Vector2> points = new List<Vector2>();
        public readonly WorldPathFlags flags;
        public float stopAndTurnDistance = 4.0f;
        public float pointCompletedDistance = 2.0f;

        public bool circular => (flags & WorldPathFlags.Circular) == WorldPathFlags.Circular;
        public bool avoidCombat => (flags & WorldPathFlags.AvoidCombat) == WorldPathFlags.AvoidCombat;

        public bool ignoreCombat => (flags & WorldPathFlags.IgnoreCombat) == WorldPathFlags.IgnoreCombat;

        public bool walk => (flags & WorldPathFlags.Walk) == WorldPathFlags.Walk;

        private static List<Vector2> construct_points(Vector2[] points)
        {
            if(points.Length == 0)
            {
                throw new Exception("There are no points!");
            }

            var result = new List<Vector2>();
            result.Add(points[0]);

            for(var i = 1; i < points.Length; ++i)
            {
                var previous = points[i - 1];
                var next = points[i];
                var middle = previous;
                for(;;)
                {
                    middle = Vector2.MoveTowards(middle, next, 20.0f);
                    result.Add(middle);
                    if(middle == next)
                    {
                        break;
                    }
                }
            }

            return result;
        }

        public WorldPath(WorldPathFlags flags, List<Vector2> ps)
        {
            this.points = construct_points(ps.ToArray());
            this.flags = flags;
        }

        public WorldPath(WorldPathFlags flags, Vector2[] ps)
        {
            this.points = construct_points(ps);
            this.flags = flags;
        }

        public int TryGetClosestPointIndex(Vector2 target)
        {
            var closest_distance = float.MaxValue;
            var closest_index = -1;

            for(var i = 0; i < points.Count; ++i) {
                var point = points[i];
                var d = Vector2.Distance(point, target);
                if (d < closest_distance)
                {
                    closest_distance = d;
                    closest_index = i;
                }
            }

            return closest_index;
        }

        public int GetClosestPointIndex(Vector2 target)
        {
            var i = TryGetClosestPointIndex(target);
            if(i == -1)
            {
                throw new Exception("Could not find the point index");
            }
            return i;
        }

        public static bool IsInRange(Vector2 player_pos, Vector2 point)
        {
            var distance = Vector2.Distance(player_pos, point);
            if(distance > 50)
            {
                return false;
            }

            return true;
        }

        public bool IsInRange(Vector2 pos)
        {
            var index = TryGetClosestPointIndex(pos);
            if (index == -1)
            {
                return false;
            }
            var closest_point = points[index];
            return IsInRange(pos, closest_point);
        }
    }
}
