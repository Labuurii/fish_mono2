﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class PathingState
    {
        public readonly WorldPath path;
        public int currentIndex = -1;
        public float pointCompletedDistance = 2.0f;

        public PathingState(WorldPath path)
        {
            this.path = path;
            this.pointCompletedDistance = path.pointCompletedDistance;
        }

        public Vector2 CurrentPoint()
        {
            return path.points[currentIndex];
        }

        public bool ReevaluateClosestPoint(Vector2 player_pos)
        {
            currentIndex = -1;
            return Advance(player_pos);
        }

        public bool Advance(Vector2 player_pos)
        {
            if(currentIndex == -2)
            {
                return false;
            }

            if(currentIndex == -1)
            {
                currentIndex = path.GetClosestPointIndex(player_pos);
            }

            if(Vector2.Distance(player_pos, path.points[currentIndex]) < pointCompletedDistance)
            {
                ++currentIndex;
                if(currentIndex >= path.points.Count)
                {
                    if(path.circular)
                    {
                        currentIndex = 0;
                    } else
                    {
                        currentIndex = -2; // Path completed
                        return false;
                    }
                }
            }

            return true;
        }

        public bool HasPoint()
        {
            return currentIndex >= 0 && currentIndex < path.points.Count;
        }
    }
}
