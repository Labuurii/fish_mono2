﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace FishMono2
{
    public class TanarisFishingSpot : IFishingSpot
    {
        public static readonly Vector2[] PathPoints = new Vector2[]
        {
            new Vector2( -7047.1f, -3780.0f ),
            new Vector2( -7060.7f, -3780.9f ),
            new Vector2( -7099.4f, -3859.0f ),
            new Vector2( -7149.3f, -3931.2f ),
            new Vector2( -7158.5f, -4025.4f ),
            new Vector2( -7190.7f, -4070.3f ),
            new Vector2( -7215.8f, -4094.6f ),
            new Vector2( -7220.9f, -4133.2f ),
            new Vector2( -7214.4f, -4226.2f ),
            new Vector2( -7208.0f, -4266.6f ),
            new Vector2( -7175.4f, -4311.1f ),
            new Vector2( -7128.9f, -4342.1f ),
            new Vector2( -7096.6f, -4364.2f ),
            new Vector2( -7076.1f, -4403.8f ),
            new Vector2( -7086.4f, -4478.9f ),
            new Vector2( -7079.0f, -4526.1f ),
            new Vector2( -7058.3f, -4572.5f ),
            new Vector2( -7037.7f, -4618.9f ),
            new Vector2( -6970.5f, -4674.2f ),
            new Vector2( -6960.8f, -4744.3f ),
            new Vector2( -6955.9f, -4788.0f ),
            new Vector2( -6938.2f, -4859.3f ),
            new Vector2( -6936.0f, -4871.0f ),
            new Vector2( -6935.7f, -4872.8f ),
            new Vector2( -6927.0f, -4873.9f ),
            new Vector2( -6921.8f, -4874.6f ),
            new Vector2( -6915.5f, -4875.4f ),
            new Vector2( -6888.9f, -4894.6f ),
            new Vector2( -6881.4f, -4895.9f ),
            new Vector2( -6813.4f, -4850.9f ),
            new Vector2( -6721.6f, -4807.1f ),
            new Vector2( -6450.1f, -4908.6f ),
            new Vector2( -6436.3f, -4877.6f ),
            new Vector2( -6436.2f, -4867.6f ),
            new Vector2( -6441.6f, -4865.1f ),
        };

        public static readonly ICommand Path = new CommandList(new ICommand[]
        {
            new CameraDownCommand(),
            new RunAndCombat
            {
                path = new WorldPathData
                {
                    flags = WorldPathFlags.AvoidCombat | WorldPathFlags.IgnoreCombat,
                    points = PathPoints
                },
                death_handler = new DeathWalkAlongPath
                {
                    points = new Vector2[]
                    {
                        new Vector2( -7191.1f, -3944.7f ),
                        new Vector2( -7177.6f, -3952.7f ),
                        new Vector2( -7127.4f, -3907.3f ),
                        new Vector2( -7057.2f, -3781.2f ),
                        new Vector2( -7045.1f, -3780.2f ),
                    }.Concat(PathPoints).ToArray()
                }.Construct()
            }.Construct()
        });

        public static readonly ICommand StartFishingCommand = new Fishing
        {
            pos = new Vector2( -6443.6f, -4872.0f ),
            look_at_pos = new Vector2(-6447.3f, -4882.7f),
            runtime = 30 * 60,
            min_pixel_change = 7
        }.Construct();

        public static readonly Vector2I TaxiLocation = TaxiLocations.Gadgetzan;

        public static readonly Fish[] Fishes = new Fish[]
        {
            Fish.FirefinSnapper,
            Fish.GoldenPearl,
            Fish.StoneScaleEel,
            Fish.WinterSquid,
            Fish.SummerBass
        };

        public ICommand GetFishingCommand()
        {
            return StartFishingCommand;
        }

        public Vector2I GetTaxiLocation()
        {
            return TaxiLocation;
        }

        public ICommand GetWalkToCommand()
        {
            return Path;
        }

        public Fish[] AvailableFishes()
        {
            return Fishes;
        }
    }
}
