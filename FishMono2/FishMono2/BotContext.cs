﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class BotContext
    {
        public readonly ScreenResource screen;
        public readonly WindowAllocator window_alloc;
        public readonly ReadTextWorker read_text_worker;
        public readonly MouseWorker mouse_worker = new MouseWorker();
        public readonly StartWoWWorker start_wow_worker = new StartWoWWorker();
        public readonly GenericFiberJobWorker generic_job_worker = new GenericFiberJobWorker();
        public Master.Master master;

        public BotContext(ScreenResource screen, WindowAllocator alloc)
        {
            this.screen = screen;
            this.window_alloc = alloc;
            this.read_text_worker = new ReadTextWorker(screen);
        }
    }
}
