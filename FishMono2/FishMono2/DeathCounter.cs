﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class DeathCounter
    {
        int _counter;

        public int Value { get => _counter; }
        public void Reset()
        {
            _counter = 0;
        }

        public void Inc()
        {
            ++_counter;
        }
    }
}
