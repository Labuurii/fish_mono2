﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class CleanupWoWVoiceProxy : RepeatComponent
    {
        public CleanupWoWVoiceProxy() : base(10.0f)
        {
        }

        protected override void PerformJob()
        {
            Task.Run(delegate ()
            {
                var processes = Process.GetProcessesByName("WowVoiceProxy");
                foreach (var process in processes)
                {
                    process.Kill();
                }
            });
        }
    }
}
