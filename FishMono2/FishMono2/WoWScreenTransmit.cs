﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class WoWScreenTransmit
    {
        public static string[] Tokenize(string str)
        {
            var end_index = str.IndexOf('#');
            if(end_index == -1)
            {
                throw new Exception("Could not find end index");
            }

            return str.Split(':');
        }
    }
}
