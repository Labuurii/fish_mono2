﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public abstract class RepeatComponent
    {
        readonly float delta_time_seconds;
        DateTime last_run = DateTime.Now;

        public RepeatComponent(float delta_time_seconds)
        {
            this.delta_time_seconds = delta_time_seconds;
        }

        protected abstract void PerformJob();

        public void Update()
        {
            if(last_run.AddSeconds(delta_time_seconds) < DateTime.Now)
            {
                last_run = DateTime.Now;
                PerformJob();
            }
        }
    }
}
