﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    // This class is also used for figuring out when a load screen has ended. Therefore more data can not be added here. It has to be as small as possible.
    public class TaxiData : ITransmittable
    {
        public int on_taxi;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state.Parse(out on_taxi);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.TaxiData);
        }
    }
}
