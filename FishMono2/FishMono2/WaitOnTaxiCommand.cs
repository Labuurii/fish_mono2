﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class WaitOnTaxiCommand : ICommand
    {
        public bool IsInRange(Vector2 pos)
        {
            return false;
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var taxi_data = new TaxiData();
            for(;;)
            {
                yield return instance.text_reader.Request(taxi_data);
                if(taxi_data.on_taxi == 0)
                {
                    break;
                }
                yield return null;
            }
        }
    }
}
