﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class PostItemsWithAppraiser
    {
        public static readonly Vector2I OpenAppraiser = new Vector2I(58, 79);
        public static readonly Vector2I ShowHidden = new Vector2I(15, 23);
        public static readonly Vector2I Post = new Vector2I(113, 75);
        public static readonly Vector2I AcceptLastNotFullStack = new Vector2I(201, 69);
        public const float PostWaitTime = 20;
        public static readonly Vector2I[] SelectItemPositions = new Vector2I[]
        {
            new Vector2I(8, 28),
            new Vector2I(8, 32),
            new Vector2I(8, 36),
            new Vector2I(8, 40),
            new Vector2I(8, 44),
            new Vector2I(8, 48),
            new Vector2I(8, 52),
            new Vector2I(8, 56),
            new Vector2I(8, 60),
        }.Reverse().ToArray();

        public static IEnumerator Perform(WoWInstance instance)
        {
            yield return FiberOps.WaitFor(2);
            var topleft = instance.windowAlloc.area.TopLeft();
            yield return instance.MouseClickAtAbs(topleft + OpenAppraiser, MouseButton.Left);
            yield return FiberOps.WaitFor(2);
            yield return instance.MouseClickAtAbs(topleft + ShowHidden, MouseButton.Left);
            yield return FiberOps.WaitFor(2);

            foreach (var item_pos in SelectItemPositions)
            {
                yield return instance.MouseClickAtAbs(topleft + item_pos, MouseButton.Left);
                yield return FiberOps.WaitFor(2);
                yield return instance.MouseClickAtAbs(topleft + Post, MouseButton.Left);
                yield return FiberOps.WaitFor(PostWaitTime);
                yield return instance.MouseClickAtAbs(topleft + AcceptLastNotFullStack, MouseButton.Left);
                yield return instance.MouseClickAtAbs(topleft + AcceptLastNotFullStack, MouseButton.Left);
                yield return FiberOps.WaitFor(2);
            }
        }
    }
}
