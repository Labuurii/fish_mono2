﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class BuffComponent
    {
        readonly WoWInstance instance;
        DateTime last_buff_time = DateTime.Now.AddDays(-1);

        public BuffComponent(WoWInstance instance)
        {
            this.instance = instance;
        }

        public void Reset()
        {
            last_buff_time = DateTime.Now.AddDays(-1);
        }

        public bool ShouldBuff(PathingData data)
        {
            return data != null && data.player_in_combat == 0 && data.norm_player_mana > 0.8f && data.player_is_casting == 0 && DateTime.Now >= last_buff_time.AddMinutes(10);
        }


        public IEnumerator Perform()
        {
            last_buff_time = DateTime.Now;
            if (instance.account.@class == "MAGE")
            {
                yield return instance.window.ClickButton(MageSpells.FrostArmor);
                yield return FiberOps.WaitFor(1.5f);
                yield return instance.window.ClickButton(MageSpells.ArcaneIntellect);
            }
            else
            {
                yield return null;
            }
        }
    }
}
