﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class MathOps
    {
        public static int Median(List<int> values)
        {
            if(values.Count == 0)
            {
                return 0;
            } else if(values.Count == 1)
            {
                return values[0];
            }

            values.Sort();
            return values[values.Count / 2];
        }
    }
}
