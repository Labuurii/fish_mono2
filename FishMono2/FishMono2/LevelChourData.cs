﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class LevelChourData : ITransmittable
    {
        public Vector2 player_pos;
        public int current_level
            , current_level_xp
            , current_level_max_xp;

        public void Parse(StringArrayParseState parse_state)
        {
            parse_state
                .Parse(out player_pos)
                .Parse(out current_level)
                .Parse(out current_level_xp)
                .Parse(out current_level_max_xp);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(TransmitButtons.LevelChourData);
        }
    }
}
