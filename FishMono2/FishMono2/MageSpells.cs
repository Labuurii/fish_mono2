﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class MageSpells
    {
        public const int Frostbolt = '1';
        public const int Fireball = '2';
        public const int FireBlast = '3';
        public const int Berserking = '4';
        public const int FrostArmor = '6';
        public const int ArcaneIntellect = '7';

        public const int ConjureWater = '8';
        public const int ConjureFood = '9';
    }
}
