﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public static class DruidOps
    {

        public static IEnumerator RunCombat(WoWInstance instance)
        {
            var combat_data = new CombatData();
            for(;;)
            {
                yield return instance.text_reader.Request(combat_data);

                if(combat_data.has_target == 0 && combat_data.player_in_combat == 0)
                {
                    yield break;
                }

                if(combat_data.is_new_error)
                {
                    switch(combat_data.last_wow_error)
                    {
                        case WoWErrors.TargetNotInFront:
                            yield return instance.window.TurnDegrees(90);
                            break;
                        case WoWErrors.OutOfRange:
                            if(combat_data.player_in_combat > 0)
                            {
                                yield return instance.window.ClickButton(BuiltinWoWButtons.WalkForward, 1.0f);
                            } else
                            {
                                yield return instance.window.ClickButton(BuiltinWoWButtons.ClearTarget);
                                yield break;
                            }
                            break;
                        case WoWErrors.OutOfMana:
                            yield return instance.window.ClickButton(BuiltinWoWButtons.StartAttack);
                            break;
                        default:
                            break;
                    }
                } else
                {
                    if(combat_data.target_in_combat == 0 && combat_data.player_in_combat != 0)
                    {
                        yield return instance.window.ClickButton(BuiltinWoWButtons.ClearTarget);
                        yield return FiberOps.WaitFor(2.0f);
                    } else if(combat_data.is_casting == 0)
                    {
                        if(combat_data.player_hp_norm < 0.4f)
                        {
                            yield return instance.window.ClickButton(DruidSpells.HealingTouch);
                        } else
                        {
                            yield return instance.window.ClickButton(DruidSpells.Wrath);
                        }
                    }
                }
            }
        }
    }
}
