﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public struct HourRange
    {
        public int start_hour, end_hour;

        public static HourRange Construct(int start_hour, int end_hour)
        {
            return new HourRange
            {
                start_hour = start_hour,
                end_hour = end_hour
            };
        }
    }
}
