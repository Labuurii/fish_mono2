﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class LookAtCommand : ICommand
    {
        readonly Vector2 point;

        public LookAtCommand(Vector2 p)
        {
            this.point = p;
        }

        public bool IsInRange(Vector2 pos)
        {
            return WorldPath.IsInRange(pos, point);
        }

        public IEnumerator Start(WoWInstance instance, BotContext context)
        {
            var pathing_data = new PathingData();

            for(var i = 0; i < 10; ++i)
            {
                yield return instance.text_reader.Request(pathing_data);
                yield return BotOps.TurnTowardsPoint(instance, pathing_data.player_pos, pathing_data.player_angle_rad, point);
            }
        }
    }
}
