﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public struct DateTimeRange
    {
        public DateTime from;
        public DateTime to;

        public static DateTimeRange Construct(DateTime @base, TimeSpan from, TimeSpan to)
        {
            var r = new DateTimeRange();
            r.from = @base + from;
            r.to = @base + to;
            return r;
        }

        public DateTimeRange Offset(TimeSpan span)
        {
            return new DateTimeRange
            {
                from = from + span,
                to = to + span
            };
        }

        public bool IsBetween(DateTime time)
        {
            return time >= from && time <= to;
        }

        public bool IsPast(DateTime time)
        {
            return time >= to;
        }
    }
}
