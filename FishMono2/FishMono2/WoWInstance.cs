﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishMono2
{
    public class WoWInstance : IDisposable
    {
        Window _window;
        WindowAllocation _window_alloc;

        Rect _text_read_area;

        public readonly BotContext context;

        public WoWAccount account;
        public readonly FiberExecutor executor = new FiberExecutor();
        public readonly ReadTextInstance text_reader;
        public readonly CountDown bright_bouble_countdown = new CountDown(10 * 60);
        public readonly DeathCounter death_counter = new DeathCounter();

        public int pid;
        public bool has_stopped;
        public bool should_terminate_on_stop;
        public bool is_paused;
        public bool is_leveling;
        public bool should_shut_down;

        public Window window => _window;
        public WindowAllocation windowAlloc => _window_alloc;
        public Rect textReadArea => _text_read_area;

        public readonly BuffComponent buffComponent;

        public WoWInstance(BotContext context, WoWAccount account)
        {
            this.context = context;
            this.account = account;
            text_reader = new ReadTextInstance(this);
            buffComponent = new BuffComponent(this);
            bright_bouble_countdown.ForceCountDownComplete();
        }

        public void TerminateProcess()
        {
            if (_window_alloc != null)
            {
                context.window_alloc.Free(_window_alloc);
                _window_alloc = null;
                _window = null;
            }

            if (pid != 0)
            {
                var handle = Win32.OpenProcess(Win32.ProcessAccessFlags.Terminate, false, pid);
                if (handle != IntPtr.Zero)
                {
                    Win32.TerminateProcess(handle, (IntPtr)0);
                    Win32.CloseHandle(handle);
                }
                pid = 0;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    text_reader.Dispose();
                    TerminateProcess();
                }

                disposedValue = true;
            }
        }

        ~WoWInstance()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public void Log(string msg)
        {
            Console.WriteLine(string.Format("{0}: {1}", account.email, msg));
        }

        public DateTime ServerTime()
        {
            return DateTime.UtcNow.Add(WoWPortalOps.GetTimeOffset(account.region));
        }

        private IEnumerator set_window(Window window, Vector2I window_size, Vector2I read_text_offset, Vector2I read_text_size)
        {
            if(_window_alloc != null)
            {
                context.window_alloc.Free(_window_alloc);
                _window_alloc = null;
            }

            for(;;)
            {
                _window = window;
                _window_alloc = context.window_alloc.TryAllocate(window, window_size);
                if(_window_alloc == null)
                {
                    yield return FiberOps.WaitFor(2);
                    continue;
                }

                var area = _window_alloc.area;
                var setup_window_task = Task.Run(delegate ()
                {
                    _window.MinimizeSize();
                    _window.SetArea(area);
                });
                FiberOps.WaitFor(setup_window_task);

                var read_text_area = area;
                read_text_area.Left += read_text_offset.x;
                read_text_area.Top += read_text_offset.y;
                read_text_area.Right = read_text_area.Left + read_text_size.x;
                read_text_area.Bottom = read_text_area.Top + read_text_size.y;
                this._text_read_area = read_text_area;
                break;
            }
        }

        public IEnumerator SetDefaultWindow(Window window)
        {
            yield return set_window(window, new Vector2I(420, 110), new Vector2I(1, 1), new Vector2I(415, 108));
        }

        public IEnumerator SetFishingWindow(Window window)
        {
            yield return set_window(window, new Vector2I(110, 110), new Vector2I(1, 1), new Vector2I(108, 108));
        }

        public void SetBot(IEnumerator e)
        {
            executor.SetEnumerator(e);
        }

        public void SaveWindowImage(string path, HighSpeed.GlobalImage image)
        {
            context.screen.SaveAsImage(path, _window_alloc.area, image);
        }

        public IEnumerator MouseClickAtAbs(Vector2I pos, MouseButton btn, float wait_dur = 0.1f)
        {
            var job = new MouseClickAtJob(window, pos, btn, wait_dur);
            context.mouse_worker.AddJob(job);
            for(;;)
            {
                if(job.isCompleted)
                {
                    break;
                }
                yield return null;
            }
        }
    }
}
