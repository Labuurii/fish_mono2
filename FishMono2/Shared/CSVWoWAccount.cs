﻿using CsvHelper;
using Shared;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
   
    public class CSVWoWAccount
    {
        public const string FILE_PATH = "accounts.csv";

        public string email { get; set; }
        public string bn_toon_id { get; set; }
        public WoWPortal portal { get; set; }

        public WoWLanguage language { get; set; }
        public PrimaryWoWJob job { get; set; }

        public WoWAccount ToWoWAccount()
        {
            var r = new WoWAccount(email);
            r.primary_wow_job = job;
            r.region = portal;
            r.language = language;
            return r;
        }

        public static List<CSVWoWAccount> ReadAccounts()
        {
            var r = new List<CSVWoWAccount>();
            using (var reader = new StreamReader(FILE_PATH))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.TrimOptions = CsvHelper.Configuration.TrimOptions.Trim;
                r.AddRange(csv.GetRecords<CSVWoWAccount>());
            }
            for (int i = r.Count - 1; i >= 0; --i)
            {
                var account = r[i];
                if (account.email.StartsWith("#"))
                {
                    r.RemoveAt(i);
                }
            }
            return r;
        }
    }
}
