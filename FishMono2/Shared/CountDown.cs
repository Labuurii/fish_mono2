﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class CountDown
    {
        DateTime end_time;
        float count_down_length;

        public CountDown(float count_down_length)
        {
            this.count_down_length = count_down_length;
            Reset();
        }

        public void ForceCountDownComplete()
        {
            end_time = DateTime.Now.AddSeconds(-1);
        }

        public void Reset()
        {
            end_time = DateTime.Now.AddSeconds(count_down_length);
        }

        public bool HasCountedDown()
        {
            return DateTime.Now >= end_time;
        }
    }
}
