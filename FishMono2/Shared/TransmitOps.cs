﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class TransmitOps
    {
        private static string ReplaceNumberMisconceptions(this string numeric)
        {
            return numeric.Replace("O", "0").Replace(".", ",").Replace("N", "-");
        }

        public static StringArrayParseState Parse(this StringArrayParseState p, out string sink)
        {
            sink = p.GetNext();
            return p;
        }

        public static StringArrayParseState Parse(this StringArrayParseState p, out int sink)
        {
            sink = int.Parse(p.GetNext().ReplaceNumberMisconceptions());
            return p;
        }

        public static StringArrayParseState Parse(this StringArrayParseState p, out float sink)
        {
            sink = float.Parse(p.GetNext().ReplaceNumberMisconceptions());
            return p;
        }

        public static StringArrayParseState Parse(this StringArrayParseState p, out Vector2 sink)
        {
            sink = new Vector2();
            p.Parse(out sink.x).Parse(out sink.y);
            return p;
        }
    }
}
