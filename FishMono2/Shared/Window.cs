﻿using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    [Flags]
    public enum KeyMod
    {
        None = 0,
        Shift = 1,
        Ctrl = 2,
        Alt = 4
    }

    public struct ButtonClick
    {
        public int button;
        public KeyMod mods;

        public ButtonClick(int button)
        {
            this.button = button;
            this.mods = KeyMod.None;
        }

        public ButtonClick(int button, KeyMod mods)
        {
            this.button = button;
            this.mods = mods;
        }

        public ButtonClick(Win32.VK vk, KeyMod mods)
        {
            this.button = (int)vk;
            this.mods = mods;
        }
    }

    public class Window
    {
        IntPtr _handle;

        public IntPtr handle { get => _handle;}

        public Window(IntPtr handle)
        {
            this._handle = handle;
        }

        public bool Exists()
        {
            return Win32.IsWindow(_handle);
        }

        public void MinimizeSize()
        {
            var style_intptr = Win32.GetWindowLongPtr(_handle, (int)Win32.GWL.GWL_STYLE);
            var style = (uint)style_intptr;
            style &= (uint) ( ~(Win32.WindowStyles.WS_CAPTION | Win32.WindowStyles.WS_THICKFRAME) );
            Win32.SetWindowLongPtr(_handle, (int)Win32.GWL.GWL_STYLE, (IntPtr)style);
            Win32.SetWindowPos(_handle, IntPtr.Zero, 0, 0, 0, 0, Win32.SWP_FRAMECHANGED | Win32.SWP_NOMOVE | Win32.SWP_NOSIZE | Win32.SWP_NOZORDER | Win32.SWP_NOOWNERZORDER);
        }

        public void SetArea(Rect area)
        {
            var size = area.GetSize();
            Win32Safe.SetWindowPos(_handle, Win32.HWND_TOP, area.Left, area.Top, size.x, size.y, 0);
        }

        public void Focus()
        {
            Win32Safe.ForceForegroundWindow(_handle);
        }

        public bool IsNotResponding()
        {
            return Win32.IsHungAppWindow(_handle);
        }

        public IEnumerator EnterString(string str)
        {
            foreach(var ch in str)
            {
                var b = (byte)ch;
                Win32Safe.PostMessage(_handle, (uint)Win32.WM.CHAR, (IntPtr)b, IntPtr.Zero);
                yield return FiberOps.WaitFor(0.05f);
            }
        }

        public IEnumerator EnterStringInChat(string str)
        {
            yield return ClickButton(Win32.VK.Return);
            yield return FiberOps.WaitFor(0.3f);
            yield return EnterString(str);
            yield return FiberOps.WaitFor(0.2f);
            yield return ClickButton(Win32.VK.Return);
        }

        private void press_button(int button)
        {
            Win32.SendMessage(_handle, (int)Win32.WM.KEY_DOWN, (IntPtr)button, IntPtr.Zero);
            //Win32Safe.PostMessage(handle, (uint)Win32.WM.KEY_DOWN, (IntPtr)button, IntPtr.Zero);
        }

        private void release_button(int button)
        {
            Win32.SendMessage(_handle, (int)Win32.WM.KEY_UP, (IntPtr)button, IntPtr.Zero);
            //Win32Safe.PostMessage(handle, (uint)Win32.WM.KEY_UP, (IntPtr)button, IntPtr.Zero);
        }

        private Task click_button_impl(int button, float duration = 0.1f)
        {
            return Task.Run(async delegate () {
                press_button(button);
                await Task.Delay((int)(duration * 1000.0f));
                release_button(button);
            });
        }

        public void ClickButtonAsync(int button, float duration = 0.1f)
        {
            click_button_impl(button, duration);
        }

        public IEnumerator ClickButton(int button, float duration = 0.1f)
        {
            var task = click_button_impl(button, duration);
            yield return FiberOps.WaitFor(task);
        }

        public Rect GetArea()
        {
            Rect rect;
            if (!Win32.GetWindowRect(_handle, out rect))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), "Could not get window rect");
            }
            return rect;
        }

        public IEnumerator ClickButton(Win32.VK vk, float duration = 0.1f)
        {
            yield return ClickButton((int)vk, duration);
        }

        public IEnumerator ClickButton(ButtonClick btn, float duration = 0.1f)
        {
            if((btn.mods & KeyMod.Shift) == KeyMod.Shift)
            {
                press_button((int)Win32.VK.Shift);
            }
            if((btn.mods & KeyMod.Ctrl) == KeyMod.Ctrl)
            {
                press_button((int)Win32.VK.Control);
            }
            if((btn.mods & KeyMod.Alt) == KeyMod.Alt)
            {
                press_button((int)Win32.VK.Menu);
            }

            yield return FiberOps.WaitFor(0.05f);
            yield return ClickButton(btn.button, duration);


            if ((btn.mods & KeyMod.Shift) == KeyMod.Shift)
            {
                release_button((int)Win32.VK.Shift);
            }
            if ((btn.mods & KeyMod.Ctrl) == KeyMod.Ctrl)
            {
                release_button((int)Win32.VK.Control);
            }
            if ((btn.mods & KeyMod.Alt) == KeyMod.Alt)
            {
                release_button((int)Win32.VK.Menu);
            }

            yield return FiberOps.WaitFor(0.05f);
        }

        public IEnumerator TurnDegrees(float degrees)
        {
            const float TURN_SPEED = 2.0f / 360.0f;

            var turn_dur = degrees * TURN_SPEED;
            if(degrees < 0)
            {
                yield return ClickButton(BuiltinWoWButtons.TurnLeft, -turn_dur);
            } else if(degrees > 0)
            {
                yield return ClickButton(BuiltinWoWButtons.TurnRight, turn_dur);
            }
        }
    }
}
