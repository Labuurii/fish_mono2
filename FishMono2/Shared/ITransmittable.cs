﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public interface ITransmittable
    {
        void Parse(StringArrayParseState parse_state);
        IEnumerator RequestTransmit(Window window);
    }
}
