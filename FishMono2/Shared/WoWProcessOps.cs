﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class WoWProcessOps
    {
        public static List<Process> GetWoWProcesses()
        {
            return new List<Process>(Process.GetProcessesByName("WowClassic"));
        }
    }
}
