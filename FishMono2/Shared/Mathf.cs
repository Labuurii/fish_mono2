﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class Mathf
    {
        public static float Min(float a, float b)
        {
            if (a < b) return b;
            return a;
        }

        public static float Max(float a, float b)
        {
            if (a < b) return a;
            return b;
        }

        public static int Clamp(int v, int min, int max)
        {
            if (v < min) v = min;
            if (v > max) v = max;
            return v;
        }

        public static float Clamp(float v, float min, float max)
        {
            if (v < min) v = min;
            if (v > max) v = max;
            return v;
        }

        public static float Clamp01(float v)
        {
            return Clamp(v, 0, 1);
        }

        public static float Sqrt(float v)
        {
            return (float)Math.Sqrt(v);
        }

        public static float Acos(float v)
        {
            return (float)Math.Acos(v);
        }

        public static float ToAntiClockDegree(float acute_degree)
        {
            if(acute_degree < 0)
            {
                return 180.0f - (-acute_degree);
            } else if(acute_degree > 0)
            {
                return 180.0f + acute_degree;
            }

            return acute_degree;
        }

        public static float WoWAngleBetween(Vector2 from, Vector2 to)
        {
            var delta = from - to;
            var angle = Mathf.ToAntiClockDegree((float)Math.Atan2(delta.y, delta.x) * Mathf.Rad2Deg);
            return angle;
        }

        public static float Positive(float v)
        {
            if(v < 0)
            {
                return -v;
            }
            return v;
        }

        public static float Deg360To180(float v)
        {
            if(v < -180.0f)
            {
                return 360.0f + v;
            } else if(v > 180.0f)
            {
                return -360.0f + v;
            }

            return v;
        }

        public const float Deg2Rad = (float)Math.PI * 2F / 360F;

        // Radians-to-degrees conversion constant (RO).
        public const float Rad2Deg = 1F / Deg2Rad;
    }
}
