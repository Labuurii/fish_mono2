﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public int Left, Top, Right, Bottom;

        public Vector2I GetSize()
        {
            return new Vector2I
            {
                x = Right - Left,
                y = Bottom - Top
            };
        }

        public Vector2I GetCenter()
        {
            var size = GetSize();
            return new Vector2I
            {
                x = Left + (size.x / 2),
                y = Top + (size.y / 2)
            };
        }

        public Vector2I TopLeft()
        {
            return new Vector2I(Left, Top);
        }

        public void PlaceAt(Vector2I top_left)
        {
            var size = GetSize();
            Left = top_left.x;
            Top = top_left.y;
            Right = Left + size.x;
            Bottom = Top + size.y;
        }
    }
}
