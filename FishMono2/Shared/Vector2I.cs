﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector2I
    {
        public int x, y;

        public Vector2I(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2I operator +(Vector2I a, Vector2I b)
        {
            return new Vector2I(a.x + b.x, a.y + b.y);
        }
    }
}
