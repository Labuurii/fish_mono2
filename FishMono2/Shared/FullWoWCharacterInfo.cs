﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class FullWoWCharacterInfo
    {
        public const string FILE_PATH = "known_wow_accounts.csv";

        public string bn_toon_id { get; set; }
        public string char_name { get; set; }
        public string region { get; set; }
        public string realm { get; set; }
        public string race { get; set; }
        public string @class { get; set; }
        public int level { get; set; }
        public int current_fishing { get; set; }
        public int max_fishing { get; set; }
        public int gold { get; set; }

        public void ToPrettyString(StringBuilder b)
        {
            b.AppendLine(bn_toon_id);
            b.AppendLine(char_name);
            b.AppendLine(region);
            b.AppendLine(realm);
            b.AppendLine(race);
            b.AppendLine(@class);
            b.AppendLine(level.ToString());
            b.AppendLine(current_fishing.ToString());
            b.AppendLine(max_fishing.ToString());
            b.AppendLine(gold.ToString());
        }

        public string ToPrettyString()
        {
            var b = new StringBuilder();
            ToPrettyString(b);
            return b.ToString();
        }

        public static List<FullWoWCharacterInfo> Load()
        {
            using (var file = new StreamReader(FILE_PATH))
            using (var reader = new CsvReader(file, CultureInfo.InvariantCulture))
            {
                reader.Configuration.TrimOptions = CsvHelper.Configuration.TrimOptions.Trim;
                var records = reader.GetRecords<FullWoWCharacterInfo>().ToList();
                return records;
            }
        }
    }
}
