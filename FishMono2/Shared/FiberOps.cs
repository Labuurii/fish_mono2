﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class FiberOps
    {
        public static IEnumerator RunConcurrently(params IEnumerator[] fibers)
        {
            var executors = new FiberExecutor[fibers.Length];
            for(int i = 0; i < fibers.Length; ++i)
            {
                var f = new FiberExecutor();
                f.SetEnumerator(fibers[i]);
                executors[i] = f;
            }

            for(;;)
            {
                for(var i = 0; i < executors.Length; ++i)
                {
                    var f = fibers[i];
                    if(f == null)
                    {
                        continue;
                    }

                    if(!f.MoveNext())
                    {
                        fibers[i] = null;
                    }
                }

                yield return null;
            }
        }

        public static IEnumerator WaitFor(float seconds)
        {
            var start_time = DateTime.Now;
            var end_time = start_time.AddSeconds(seconds);
            yield return WaitUntil(end_time);
        }

        public static IEnumerator WaitUntil(DateTime end_time)
        {
            for (; ; )
            {
                var now = DateTime.Now;
                if (now >= end_time)
                {
                    break;
                }
                yield return null;
            }
        }

        public static IEnumerator WaitFor(Task task)
        {
            for(;;)
            {
                if(task.IsCompleted)
                {
                    break;
                }
                if(task.IsFaulted)
                {
                    throw task.Exception;
                }
                if(task.IsCanceled)
                {
                    break;
                }
                yield return null;
            }
        }
    }
}
