﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class StringArrayParseState
    {
        string[] strings;
        int index;

        public StringArrayParseState(string[] strings)
        {
            this.strings = strings;
        }

        public bool TryGetNext(out string sink)
        {
            if(index == strings.Length)
            {
                sink = string.Empty;
                return false;
            }

            sink = strings[index];
            ++index;
            return true;
        }

        public string GetNext()
        {
            string r;
            if(!TryGetNext(out r))
            {
                throw new Exception("Tried to get too many string entries");
            }
            return r;
        }
    }
}
