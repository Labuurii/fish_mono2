﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class FiberExecutor
    {
        readonly List<IEnumerator> enumerators = new List<IEnumerator>();

        public void SetEnumerator(IEnumerator e)
        {
            enumerators.Clear();
            enumerators.Add(e);
        }

        public string GetEnumeratorsStackFrames()
        {
            var enums = new List<IEnumerator>(enumerators);
            enums.Reverse();
            var sb = new StringBuilder();
            foreach(var enum_ in enums)
            {
                sb.AppendLine(enum_.ToString());
            }
            return sb.ToString();
        }

        public bool Step()
        {
            if(enumerators.Count == 0)
            {
                return false;
            }

            var enumerator = enumerators[enumerators.Count - 1];

            var r = enumerator.MoveNext();
            if(!r)
            {
                enumerators.RemoveAt(enumerators.Count - 1);
                return Step();
            }

            var current = enumerator.Current;
            if(current != null)
            {
                var new_enum = current as IEnumerator;
                if(new_enum != null)
                {
                    enumerators.Add(new_enum);
                    Step();
                    return true;
                }
            }

            return true;
        }
    }
}
