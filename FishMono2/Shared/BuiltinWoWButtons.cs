﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class BuiltinWoWButtons
    {
        public static int MoveForward = 'W';
        public static int MoveBackward = 'S';
        public static int DoManualStuff = 'R';
        public static int TargetNearestEnemy = (int)Win32.VK.Tab;
        public static int ClearTarget = (int)Win32.VK.F12;
        public static int WalkForward = (int)Win32.VK.W;
        public static int WalkBackward = (int)Win32.VK.S;
        public static int TurnLeft = (int)Win32.VK.A;
        public static int TurnRight = (int)Win32.VK.D;
        public static int StartAttack = (int)Win32.VK.F11;
        public static int StopAttack = (int)Win32.VK.F10;
        public static int StartAutoRun = (int)Win32.VK.F9;
        public static int StopAutoRun = (int)Win32.VK.F8;
        public static int Jump = (int)Win32.VK.Space;
        public static ButtonClick ToggleWalking = new ButtonClick(Win32.VK.F12, KeyMod.Ctrl);
        public static ButtonClick Hearthstone = new ButtonClick(Win32.VK.F3, KeyMod.Ctrl);
        public static ButtonClick DestroyOverquotedFish = new ButtonClick(Win32.VK.F6, KeyMod.Ctrl);
        public static ButtonClick BuyBrightBaubles = new ButtonClick(Win32.VK.F9, KeyMod.Ctrl);
        public static ButtonClick HideScriptErrors = new ButtonClick(Win32.VK.F10, KeyMod.Ctrl);

        public static ButtonClick VendorSellAll = new ButtonClick(Win32.VK.F1, KeyMod.Shift);
        public static ButtonClick VendorClose = new ButtonClick(Win32.VK.F2, KeyMod.Shift);
        public static ButtonClick TrainerClose = new ButtonClick(Win32.VK.F3, KeyMod.Shift);
        public static ButtonClick TrainAll = new ButtonClick(Win32.VK.F4, KeyMod.Shift);
        public static ButtonClick PlaceSpells = new ButtonClick(Win32.VK.F5, KeyMod.Shift);
        public static ButtonClick HideUI = new ButtonClick(Win32.VK.F6, KeyMod.Shift);
        public static ButtonClick ShowUI = new ButtonClick(Win32.VK.F7, KeyMod.Shift);

        public static ButtonClick MageDrink = new ButtonClick(Win32.VK.F8, KeyMod.Shift);
        public static ButtonClick MageEat = new ButtonClick(Win32.VK.F9, KeyMod.Shift);
        public static ButtonClick MageCleanupBagForMoreWater = new ButtonClick(Win32.VK.F10, KeyMod.Shift);
        public static ButtonClick MageCleanupBagForMoreFood = new ButtonClick(Win32.VK.F11, KeyMod.Shift);

        public static ButtonClick CameraDown = new ButtonClick('1', KeyMod.Alt);
        public static ButtonClick CameraUp = new ButtonClick('2', KeyMod.Alt);
    }
}
