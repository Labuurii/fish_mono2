﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public enum PrimaryWoWJob
    {
        ClassicLevelingKalimdor,
        ClassicFishingKalimdor
    }

    public enum WoWJob
    {
        None
    }

    public enum WoWPortal
    {
        EU,
        US,
        KR
    }

    public enum WoWLanguage
    {
        enUS,
        ruRU
    }

    public static class WoWPortalOps
    {
        public static TimeSpan GetTimeOffset(WoWPortal portal)
        {
            switch(portal)
            {
                case WoWPortal.EU:
                    return TimeSpan.FromHours(2);
                case WoWPortal.KR:
                case WoWPortal.US:
                    throw new NotImplementedException();
                default:
                    throw new Exception("Unhandled enum value " + portal);
            }
        }
    }

    public class WoWAccount : ITransmittable
    {
        private readonly string _email;
        private WoWPortal _region;
        private string _race;
        private string _class;
        private int _current_level = -1;
        private int _current_fishing = -1;
        private int _max_fishing = -1;
        private int _gold_count;
        private PrimaryWoWJob _primary_wow_job = PrimaryWoWJob.ClassicFishingKalimdor;

        public int Id { get; set; }
        public string email => _email;

        public WoWPortal region { get => _region; set => _region = value; }
        public string race { get => _race; set => _race = value; }
        public string @class { get => _class; set => _class = value; }
        public int current_level { get => _current_level; set => _current_level = value; }
        public int current_fishing { get => _current_fishing; set => _current_fishing = value; }
        public int max_fishing { get => _max_fishing; set => _max_fishing = value; }

        public int gold_count { get => _gold_count; set => _gold_count = value; }

        public DateTime last_update_time { get; set; }

        public PrimaryWoWJob primary_wow_job { get => _primary_wow_job; set => _primary_wow_job = value; }

        public WoWJob job = WoWJob.None;

        public WoWLanguage language;

        public WoWAccount(string email)
        {
            if(email == null) throw new ArgumentNullException("email");
            this._email = email;
        }

        public bool HasAllData()
        {
            return 
                !string.IsNullOrEmpty(race)
                && !string.IsNullOrEmpty(@class)
                && current_level != -1
                && current_fishing != -1
                && max_fishing != -1;
        }

        public void Parse(StringArrayParseState p)
        {
            p
                .Parse(out _race)
                .Parse(out _class)
                .Parse(out _current_level)
                .Parse(out _current_fishing)
                .Parse(out _max_fishing)
                .Parse(out _gold_count);
        }

        public IEnumerator RequestTransmit(Window window)
        {
            yield return window.ClickButton(Win32.VK.F3);
        }

        public string PrettyString()
        {
            var b = new StringBuilder();
            b.Append("Email: ");
            b.AppendLine(_email);
            b.Append("Region: ");
            b.AppendLine(_region.ToString());
            b.Append("Race: ");
            b.AppendLine(_race);
            b.Append("Class: ");
            b.AppendLine(_class);
            b.Append("Level: ");
            b.AppendLine(_current_level.ToString());
            b.Append("Fishing Skill: ");
            b.AppendLine(_current_fishing.ToString());
            b.Append("Max Fishing Skill: ");
            b.AppendLine(_max_fishing.ToString());
            b.Append("Gold: ");
            b.AppendLine(_gold_count.ToString());

            return b.ToString();
        }

        public bool IsFishingReady()
        {
            return primary_wow_job == PrimaryWoWJob.ClassicFishingKalimdor;
        }

        public void Set(FullWoWCharacterInfo full_info)
        {
            _current_fishing = full_info.current_fishing;
            _max_fishing = full_info.max_fishing;
            _current_level = full_info.level;
            _gold_count = full_info.gold;
            _race = full_info.race;
        }
    }
}
