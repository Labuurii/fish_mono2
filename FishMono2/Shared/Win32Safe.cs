﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class Win32Safe
    {
        public static void PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam)
        {
            var r = Win32.PostMessage(hWnd, Msg, wParam, lParam);
            if(!r)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static void SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags)
        {
            var r = Win32.SetWindowPos(hWnd, hWndInsertAfter, X, Y, cx, cy, uFlags);
            if(!r) {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static void SetCursorPos(int x, int y)
        {
            var r = Win32.SetCursorPos(x, y);
            if(!r)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        public static void ForceForegroundWindow(IntPtr hWnd)
        {
            uint foreThread = Win32.GetWindowThreadProcessId(Win32.GetForegroundWindow(), IntPtr.Zero);
            uint appThread = Win32.GetCurrentThreadId();

            if (foreThread != appThread)

            {
                Win32.AttachThreadInput(foreThread, appThread, true);
                Win32.BringWindowToTop(hWnd);
                Win32.ShowWindow(hWnd, Win32.ShowWindowCommands.SW_SHOW);
                Win32.AttachThreadInput(foreThread, appThread, false);

            } else
            {
                Win32.BringWindowToTop(hWnd);
                Win32.ShowWindow(hWnd, Win32.ShowWindowCommands.SW_SHOW);
            }
        }
    }
}
